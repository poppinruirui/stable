/*
 * 一个PhotonView就是一个跟服务器端的长连接。如果每个球球身上都带一个photonView，这势必无谓地占用大量带宽资源，造成网络拥堵。
 * 
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	public GameObject _shell; 
	public GameObject _thorn;

	public CircleCollider2D _Trigger;
	public CircleCollider2D _Collider;

	public GameObject _Mouth;
	public SpriteRenderer _srMouth;
	public GameObject _main; // 暂不废弃ƒ
	public Rigidbody2D _rigid;


	bool m_bIsEjecting = false;

	public Vector3 vecTempSize = new Vector3();
    public Vector3 vecTempPos = new Vector3 ();
    public Vector3 vecTempPos2 = new Vector3(); 
    public Vector3 vecTempScale = new Vector3 ();
	public Vector2 vec2Temp = new Vector2 ();
	public Vector2 vec2Temp1 = new Vector2 ();
    public Color cTempColor = new Color();

	Vector2 _direction = new Vector2();
	Vector2 _speed = new Vector2 ();
	Vector2 _vecSpitDir = new Vector2();

	public TextMesh _text;
	public TextMesh _txtPlayerName;
	public TextMesh _txtPlayerNameOutLine;

    public BayonetTunnel _bayoTunnel = null;

	public static float s_fTriggerRadius_InitValue = 0f;
	public static Vector3 s_vecShellInitScale = new Vector3 ();

    //// MiniMap 相关
    MiniMapBall m_MyMiniMapDouble = null;
    public MiniMapBall GetMyMiniMapDouble()
    {
        return m_MyMiniMapDouble;
    }

    public void SetMiniMapDouble(MiniMapBall obj )
    {
        m_MyMiniMapDouble = obj; 
    }

    float m_fDirX = 0.0f;
	float m_fDirY = 0.0f;
	public float _dirx = 0.0f;
	/*
	{
		set {  
			if (_ba == null) {
				_direction.x = value;
			} else {
				_ba._direction.x = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.x;
			} else {
				return _ba._direction.x; 
			}
		}
	}
	*/

	public float _diry = 0.0f;
	/*
	{
		set {  
			if (_ba == null) {
				_direction.y = value;
			} else {
				_ba._direction.y = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.y;
			} else {
				return _ba._direction.y; 
			}
		}
	}
	*/
   
	public ObsceneMaskShader _obsceneCircle;
	float m_fSpeed = 0.0f;
    float m_fTargetSize = 1.0f;

	public float _fCapturedSpeed = 0f;
	public float _fCapturedDirX = 0f;
	public float _fCapturedDirY = 0f;

	public void SetDirX( float val )
	{
		//photonView.RPC ( "RPC_SetDirX", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirX( float val )
	{
		_dirx = val;
	}

	public void SetDirY( float val )
	{
		//photonView.RPC ( "RPC_SetDirY", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirY( float val )
	{
		_diry = val;
	}


	public Player _Player = null; // 本Ball所属的Player
	public GameObject _player = null; // 这个 ball所属的player
				

	PhotonTransformView m_PhoTransView;

	eBallType m_eBallType = eBallType.ball_type_ball;
	public eBallType _balltype
	{
		set {  m_eBallType = value; }
		get {  return m_eBallType; }
	}

	public enum eBallType
	{
		ball_type_ball,          // 常规的球
		ball_type_thorn,         // 刺
		ball_type_bean,          // 豆子
		ball_type_spore,         // 孢子 
	};

	public BallAction _ba; // 小熊那边做的BallAction模块


	/*
	[PunRPC]
	public void RPC_DestroyBall()
	{
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}
	*/

	protected bool m_bDead = false;
	public void Die()
	{
        /*
		m_bDead = true;
		
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		gameObject.SetActive ( false );

		if (_Trigger) {
			_Trigger.enabled = false;
		}

		if (_Collider) {
			_Collider.enabled = false;
		}
        */
	}

	public bool IsDead()
	{
		return m_bDead;
	}

	// [poppin youhua]
	public static void DestroyTarget( SpitBallTarget target )
	{
		//GameObject.Destroy ( target.gameObject );
		if (target) {
			target.SetVisible ( false );
		}
	}

	// [poppin youhua] 场景豆子的重生流程可以优化一下。
	public static void DestroyBean( Bean bean )
	{
		if (bean._beanType == Bean.eBeanType.scene) // 只有场景静态豆子才走这个重生流程，其余动态产生的豆子有自己的重生流程
		{
			Bean.ApplyGenerateOneNewBean();
		}

		bean.gameObject.SetActive ( false );
		//GameObject.Destroy ( bean.gameObject );
		ResourceManager.RecycleBean( bean );
		/*
		if (bean._Trigger) {
			bean._Trigger.enabled = false;
		}
		*/
	}

	SpriteRenderer m_sr;
	void Awake()
	{
		
        
        Init ();

		m_sr = this.gameObject.GetComponent<SpriteRenderer> () ;

	}

    void Start()
    {
		Main.s_Instance.s_nBallTotalNum++;
    }

	public bool IsMine()
	{
		return _Player.photonView.isMine;
	}

	public void SetPlayer( Player player )
	{
		_Player = player;
		_srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId ( _Player.photonView.ownerId );
	}

	Vector3 m_vecObsCircleInitScale = new Vector3();		
    public virtual void Init()
	{
		GameObject go;

		if (_shell) {
			s_vecShellInitScale = _shell.transform.localScale;
		}
		SetShellEnable ( false );

		/*
        if (_player)
        {
            _Player = _player.GetComponent<Player>();
			if (!photonView.isMine) {
				_Player.AddOneBall (this);

			}

            if (photonView.isMine)
            {
                DarkForest.s_Instance.AddBall(this);
            }

            MiniMap.s_Instance.AddBall(this, photonView.isMine);

        }
	*/
/*
		Transform transThorn = this.gameObject.transform.Find ("Thorn"); 
		if (transThorn) {
			_thorn = transThorn.gameObject;
			SpriteRenderer sr = _thorn.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                sr.color = _Player._color_poison;
            }
			transThorn.localScale = Vector3.zero;
		} else {
			Debug.LogError ( "transThorn empty" );
			return;
		}
*/

/*
		Transform transMouth = this.gameObject.transform.Find ("Mouth"); 
		if (transMouth) {
			_Mouth = transMouth.gameObject;
			_srMouth = _Mouth.GetComponent<SpriteRenderer> ();
			if (photonView) {
				_srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (photonView.ownerId);
			} else {
				_srMouth.sprite = Main.s_Instance.GetSpriteByPhotonOwnerId (3);
			}
            if (_Player)
            {
               // _srMouth.color = _Player._color_inner;  // 用贴图了，颜色机制暂时废弃
            }
		} else {
			Debug.LogError ( "transMouth empty" );
			return;
		}
*/

		/*
		GameObject goObsceneMask = this.gameObject.transform.Find ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();
		_obsceneCircle.gameObject.SetActive ( false );
		m_vecObsCircleInitScale = _obsceneCircle.gameObject.transform.localScale;
        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game)
        {
       
        }
        else
        {
            goObsceneMask.SetActive( false );
        }
		_ba = this.gameObject.GetComponent<BallAction> ();

		go = this.gameObject.transform.Find ( "Text" ).gameObject;
		_text = go.GetComponent<TextMesh> ();

		*/

	}

	public void SetSprite( Sprite sprite )
	{
		_srMouth.sprite = sprite;
	}

	public void SetShellEnable( bool val )
	{
		if (_shell) {
			_shell.gameObject.SetActive (val);
		}
	}

	// 上面的Init()是初生Init，由于我们是内存池机制，球球删除时并不真正销毁，待会儿会拿出来复用，所以再专门设置一个重生的Init()
	public void RebornInit()
	{
		SetGroup (null);
		m_dicCollideList.Clear ();
		m_fShellShrinkCurTime = -1f;
		SetShellEnable ( false );
		Local_EndShell ();
		SetDead ( false );
		SetEaten ( false );
		m_nMovingToCenterStatus = 0;
		vecTempScale.x = 1f;
		vecTempScale.y = 1f;
		vecTempScale.z = 1f;
		_Mouth.transform.localScale = vecTempScale;
		m_nUnfoldStatus = 0;
		_obsceneCircle.gameObject.SetActive ( false );
		Local_SetDir ( Vector2.zero );	
		_direction = Vector2.zero;
		_speed = Vector2.zero;
		_vecSpitDir = Vector2.zero;
		m_fMoveSpeedX = 0.0f;
		m_fMoveSpeedY = 0.0f;
		m_bIsEjecting = false;
		m_vecdirection = Vector2.zero;
		BallSizeChange ();

		SetPlayerName ( _Player.GetPlayerName() );
	}

	// Update is called once per frame
	public virtual void Update () 
    {
		PreUnfolding ();
		Unfolding ();
		Shell ();
		//Move ();
		//NeedSyncSizeLoop ();
		//Ejecting ();
		Ejecting_New();
		ProcessColliderTemp ();
		Staying ();
		UpdateSpitDir ();
		UpdateSplitTarget ();

		ShowSize();

        // poppin test
        AdjustMoveProtectLoop();
    }

	public void SetPlayerName( string szPlayerName )
	{
		if (Main.s_Instance.m_nTestStatus == 1) {
			return;
		}

		_txtPlayerName.text = szPlayerName;
		_txtPlayerNameOutLine.text = szPlayerName;
	}

	public virtual void  FixedUpdate()
    {
		Move ();
    }

	bool m_bNeedSyncSize = false;
	float m_fLastSyncSizeTime = 0f;
	public void SetSize( float val )
	{
		Local_SetSize ( val );
	}

	[PunRPC]
	public void RPC_SetSize( float val )
	{
		Local_SetSize (val);
	}

	public void Local_SetSize( float val )
	{
		vecTempSize.x = val;
		vecTempSize.y = val;
		vecTempSize.z = 1.0f;
		this.transform.localScale = vecTempSize;

		// 遮挡是由z坐标值决定。
		vecTempPos = GetPos();
		vecTempPos.z = -val;
		SetPos ( vecTempPos );

		BallSizeChange ();
	}

	
	float m_fSize2CiFang = 0.0f;
	float m_fSize3CiFang = 0.0f;
	float m_fSize2KaiGenHao = 0.0f;
	float m_fSize3KaiGenHao = 0.0f;
	void BallSizeChange()
	{
		m_fSize2CiFang = GetSize () * GetSize ();
		m_fSize3CiFang = m_fSize2CiFang * GetSize ();
		m_fSize2KaiGenHao = Mathf.Sqrt ( GetSize () );
		m_fSize3KaiGenHao = CyberTreeMath.AreaToSize ( GetSize (), 3 );
	//	Local_SetThornSize ( GetThornSize() );
		CalculateMassBySize ( GetSize() );
		CalculateMianJiByRadius ();
        vecTempPos = GetPos();
        vecTempPos.z = -GetSize();
        Local_SetPos( vecTempPos.x, vecTempPos.y, vecTempPos.z );
	}

	void SetNeedSyncSize()
	{
//		if (!photonView.isMine) {
//			return;
//		}

		if (!m_bNeedSyncSize) {
			m_fLastSyncSizeTime = (float)PhotonNetwork.time;
		}
		m_bNeedSyncSize = true;
	}

	void NeedSyncSizeLoop()
	{
//		if (photonView == null) {
//			return;
//		}

//		if (!photonView.isMine) {
//			return;
//		}

		if (!m_bNeedSyncSize) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		float fDelta = (float)PhotonNetwork.time - m_fLastSyncSizeTime;
		if ( fDelta > 1.0f)
		{

			SetSize( GetSize() );
			m_bNeedSyncSize = false;
		}

	}

	public float GetArea( int nShit )
	{
		if (nShit == 2) {
			return GetSize2CiFang ();
		} else if (nShit == 3) {
			return GetSize3CiFang ();
		}
		return 0.0f;
	}

	public float GetSize2CiFang()
	{
		return m_fSize2CiFang;
	}

	public float GetSize3CiFang()
	{
		return m_fSize3CiFang;
	}

	public float GetSizeKaiGenHao()
	{
		if (Main.s_Instance.m_nShit == 2) {
			return GetSize2KaiGenHao ();
		} else if (Main.s_Instance.m_nShit == 3) {
			return GetSize3KaiGenHao ();
		} else {
			Debug.LogError ( "GetSizeKaiGenHao" );
			return 1;
		}
	}

	public float GetSize2KaiGenHao ()
	{
		return m_fSize2KaiGenHao;
	}

	public float GetSize3KaiGenHao ()
	{
		return m_fSize3KaiGenHao;
	}


	float m_fMianJi = 0.0f; // 这个是真正数学意义上的面积，不是咱们游戏自定义的一些数据结构
	float m_fRadius = 0.0f;
	void CalculateMianJiByRadius ()
	{
		if (_Trigger == null) {
			return;
		}

		m_fRadius = _Trigger.bounds.size.x / 2.0f;
		m_fMianJi =  Mathf.PI *  m_fRadius * m_fRadius;
	}

	public float GetRadius()
	{
		return m_fRadius;
	}

	public float GetMianJi()
	{
		return m_fMianJi;
	}

	void CalculateMassBySize( float fSize )
	{
		if (_rigid == null) {
			return;
		}
		_rigid.mass = GetSize3CiFang();
	}

	public float GetSize()
	{
		return this.transform.localScale.x;
	}

	float m_fColliderTempTime = 0f;
	void ProcessColliderTemp ()
	{
		/*
		if (IsDead ()) {
			return;
		}

		if ( _ColliderTemp == null || _ColliderTemp.enabled == false )
		{
			return;
		}
		m_fColliderTempTime -= Time.deltaTime;
		if (m_fColliderTempTime <= 0f) {
			_ColliderTemp.enabled = false;
		}
		*/
	}

	public bool IsEjecting()
	{
		return m_bIsEjecting;
	}

	float m_fEjectInitialSpeed = 0.0f;
	float m_fEjectSpeed = 0.0f;
	float m_fEjectAccelerate = 0.0f;
	float m_fStartPosX = 0.0f;
	float m_fStartPosY = 0.0f;
	float m_fTimeClapse = 0.0f;
	float m_fEjectingTotalTime = 0f;
	public void BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f  )
	{
	//photonView.RPC ( "RPC_BeginEject", PhotonTargets.All, fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp,fStayTime );
	}

	[PunRPC]
	public void RPC_BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f   )
	{
		Local_BeginEject ( fInitialSpeed, fAccelerate, fEjectingTotalTime, bRealTimeFollowMouse, bUseColliderTemp, fStayTime );
	}

	bool m_bRealTimeFollowMouse = false;
	bool m_bUseColliderTemp = false;
	float m_fStayTimeWhenEjectEnd = 0f;
	public void Local_BeginEject( float fInitialSpeed, float fAccelerate, float fEjectingTotalTime, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f  )
	{
		m_fStayTimeWhenEjectEnd = fStayTime;
		m_bUseColliderTemp = bUseColliderTemp;
		m_fEjectingTotalTime = fEjectingTotalTime;
		m_bRealTimeFollowMouse = bRealTimeFollowMouse;
		m_bIsEjecting = true;
		m_fEjectInitialSpeed = fInitialSpeed;
		m_fEjectSpeed = fInitialSpeed;
		m_fEjectAccelerate = fAccelerate;
		m_fStartPosX = this.gameObject.transform.position.x;
		m_fStartPosY = this.gameObject.transform.position.y;
		m_fTimeClapse = 0.0f;
	}

	float m_fTotalEjectDisX = 0f;
	float m_fTotalEjectDisY = 0f;
	float m_fCurEjectDisX = 0f;
	float m_fCurEjectDisY = 0f;
	public void Local_BeginEject_New( float fSpeed, float fDis, bool bRealTimeFollowMouse = false, bool bUseColliderTemp = false, float fStayTime = 0f  )
	{
		m_fStayTimeWhenEjectEnd = fStayTime;
		m_bUseColliderTemp = bUseColliderTemp;
		m_bRealTimeFollowMouse = bRealTimeFollowMouse;
		m_bIsEjecting = true;
		m_fEjectSpeed = fSpeed;
		m_fStartPosX = this.gameObject.transform.position.x;
		m_fStartPosY = this.gameObject.transform.position.y;
		m_fTimeClapse = 0.0f;

		m_fTotalEjectDisX = fDis * _dirx;
		m_fTotalEjectDisY = fDis * _diry;
		m_fCurEjectDisX = 0f;
		m_fCurEjectDisY = 0f;


		// poppin test
		if (m_fEjectSpeed > Main.s_Instance.m_fMaxMoveDistancePerFrame) {
			m_fEjectSpeed = Main.s_Instance.m_fMaxMoveDistancePerFrame;
		}

	}

	public bool CheckNeedCollider()
	{
		if (!m_bShell) {
			return false;
		}

		if (Main.s_Instance.m_fShellShrinkTotalTime == 0f) {
			return false;
		}

		return true;
	}

	public void EndEject()
	{
        //SyncBaseInfo();

		m_bIsEjecting = false;


		if (m_bUseColliderTemp) {
			//_ColliderTemp.enabled = true;
			m_fColliderTempTime = 0.5f;
		}

		if (m_bShell) {

		}

		if (m_fStayTimeWhenEjectEnd > 0f) {
			BeginStay ();
		} 

		Local_BeginShell ();
	}

	bool m_bStaying = false;
	public void BeginStay ()
	{
		m_bStaying = true;
	}

	void Staying ()
	{
		if (IsDead ()) {
			return;
		}

		if (m_bStaying == false) {
			return;
		}

		m_fStayTimeWhenEjectEnd -= Time.deltaTime;
		if (m_fStayTimeWhenEjectEnd <= 0f) {
			EndStay ();
		}
	}

	public bool IsStaying()
	{
		return m_bStaying;
	}

	public void EndStay ()
	{
		m_bStaying = false;
	}



	public void Ejecting_New()
	{
		if (IsDead ()) {
			return;
		}

		if (!m_bIsEjecting) {
			return;
		}

		float fDeltaX = Time.deltaTime * m_fEjectSpeed * _dirx;
		float fDeltaY = Time.deltaTime * m_fEjectSpeed * _diry;

		/*
		float fAbsDeltaX = Mathf.Abs( fDeltaX );
		if ( fAbsDeltaX > Main.s_Instance.m_fMaxMoveDistancePerFrame) {
			int nTimes = (int)( fAbsDeltaX / Main.s_Instance.m_fMaxMoveDistancePerFrame );
			float fRealMoveDisX = fDeltaX > 0 ? Main.s_Instance.m_fMaxMoveDistancePerFrame : (-Main.s_Instance.m_fMaxMoveDistancePerFrame);
			fDeltaX -= ( fRealMoveDisX * nTimes );
			for (int i = 0; i < nTimes; i++) {
				if (DoEjectingRealMove (0, fRealMoveDisX, m_fTotalEjectDisX, ref m_fCurEjectDisX)) {
					return; // end eject
				}
			}
			if (DoEjectingRealMove (0, fDeltaX, m_fTotalEjectDisX, ref m_fCurEjectDisX)) {
				return; // end eject
			}
		}
		else
		{
			if (DoEjectingRealMove (0, fDeltaX, m_fTotalEjectDisX, ref m_fCurEjectDisX)) {
				return;// end eject
			}
		}

		float fAbsDeltaY = Mathf.Abs( fDeltaY );
		if ( fAbsDeltaY > Main.s_Instance.m_fMaxMoveDistancePerFrame) {
			int nTimes = (int)( fDeltaY / Main.s_Instance.m_fMaxMoveDistancePerFrame );
			float fRealMoveDisY = fDeltaY > 0 ? Main.s_Instance.m_fMaxMoveDistancePerFrame : (-Main.s_Instance.m_fMaxMoveDistancePerFrame);
			fDeltaY -= ( fRealMoveDisY * nTimes );
			for (int i = 0; i < nTimes; i++) {
				if (DoEjectingRealMove ( 1, fRealMoveDisY, m_fTotalEjectDisY, ref m_fCurEjectDisY)) {
					return; // end eject
				}
			}
			if (DoEjectingRealMove (1, fDeltaY, m_fTotalEjectDisY, ref m_fCurEjectDisY)) {
				return; // end eject
			}
		}
		else
		{
			if (DoEjectingRealMove (1, fDeltaY, m_fTotalEjectDisY, ref m_fCurEjectDisY)) {
				return;// end eject
			}
		}
		*/
		vecTempPos = GetPos ();



		if ( ( ( m_fTotalEjectDisX == 0f ) || ( m_fTotalEjectDisX > 0 && m_fCurEjectDisX >= m_fTotalEjectDisX) || ( m_fTotalEjectDisX < 0 && m_fCurEjectDisX <= m_fTotalEjectDisX ) ) &&
			(  ( m_fTotalEjectDisY == 0f ) || ( m_fTotalEjectDisY > 0 && m_fCurEjectDisY >= m_fTotalEjectDisY) || ( m_fTotalEjectDisY < 0 && m_fCurEjectDisY <= m_fTotalEjectDisY ) )			
		)
		{
			EndEject ();
			return;
		}

		float fRealNextX = 0f;
		float fRealNextY = 0f;
		MapEditor.s_Instance.ClampMoveToWorldBorder ( GetRadius(), vecTempPos.x, vecTempPos.y, fDeltaX, fDeltaY, ref fRealNextX, ref fRealNextY);

		if ( ( m_fTotalEjectDisX == 0f ) || ( m_fTotalEjectDisX > 0 && m_fCurEjectDisX < m_fTotalEjectDisX ) || ( m_fTotalEjectDisX < 0 && m_fCurEjectDisX > m_fTotalEjectDisX )  ) {
			vecTempPos.x = fRealNextX;
		}
		if ( ( m_fTotalEjectDisY == 0f ) || ( m_fTotalEjectDisY > 0 && m_fCurEjectDisY < m_fTotalEjectDisY ) || ( m_fTotalEjectDisY < 0 && m_fCurEjectDisY > m_fTotalEjectDisY )  ) {
			vecTempPos.y = fRealNextY;
		}

		SetPos( vecTempPos );

		m_fCurEjectDisX += fDeltaX;
		m_fCurEjectDisY += fDeltaY;
	}

	// nAxis    0 - X    1 - Y
	public bool DoEjectingRealMove( int nAxis, float fDelta, float fTotalEjectDis, ref float fCurEjectDis )
	{
		fCurEjectDis += fDelta;

		if (
			 ( fTotalEjectDis == 0f ) ||
			 ( fTotalEjectDis > 0 && fCurEjectDis >= fTotalEjectDis ) ||
			 ( fTotalEjectDis < 0 && fCurEjectDis <= fTotalEjectDis ) 
		   ) 
		{
			EndEject ();
			return true;
		}

		vecTempPos = _rigid.position;
		bool bCanMoveX = true;
		bool bCanMoveY = true;
		if (nAxis == 0) { // X轴
			MapEditor.s_Instance.CheckIfWillExceedWorldBorder (this, fDelta, 0f, ref bCanMoveX, ref bCanMoveY);
			bCanMoveY = false;
		} else if (nAxis == 1) {
			MapEditor.s_Instance.CheckIfWillExceedWorldBorder (this, 0f, fDelta, ref bCanMoveX, ref bCanMoveY);
			bCanMoveX = false;
		}

		if (bCanMoveX) {
			vecTempPos.x += fDelta;
		}
		if (bCanMoveY) {
			vecTempPos.y += fDelta;
		}

		 SetPos( vecTempPos );

		//_rigid.MovePosition( vecTempPos );
		//_rigid.MoveRotation ( 0 );


		return false;
	}

	public void Ejecting()
	{
		if (IsDead ()) {
			return;
		}

		if (!m_bIsEjecting) {
			return;
		}



		if (m_fTimeClapse >= m_fEjectingTotalTime/*Main.s_Instance.m_fSpitRunTime*/) {
			EndEject ();
			return;
		}

		if (m_bRealTimeFollowMouse ) {
				vec2Temp = _Player.GetWorldCursorPos ()- this.GetPos ();
		

					vec2Temp.Normalize ();
					_dirx = vec2Temp.x;
					_diry = vec2Temp.y;
		}

		/*
		m_fTimeClapse += Time.fixedDeltaTime;
		vecTempPos.x = m_fStartPosX;
		vecTempPos.y = m_fStartPosY;
		vecTempPos.z = this.gameObject.transform.position.z;
		float S = CyberTreeMath.CalculateDistance( m_fEjectInitialSpeed, m_fEjectAccelerate, m_fTimeClapse ); //m_fEjectSpeed * m_fTimeClapse + 0.5f * m_fAccelerate * m_fTimeClapse * m_fTimeClapse;
		vecTempPos.x += S * _dirx;
		vecTempPos.y += S * _diry;
		*/
		m_fTimeClapse += Time.deltaTime;
		vecTempPos = GetPos ();
		float fDeltaX = Time.deltaTime * m_fEjectInitialSpeed * _dirx;
		float fDeltaY = Time.deltaTime * m_fEjectInitialSpeed * _diry;

		float fRealNextX = 0f;
		float fRealNextY = 0f;
		MapEditor.s_Instance.ClampMoveToWorldBorder ( GetRadius(), vecTempPos.x, vecTempPos.y, fDeltaX, fDeltaY, ref fRealNextX, ref fRealNextY);

		vecTempPos.x = fRealNextX;
		vecTempPos.y = fRealNextY;

		this.transform.position = vecTempPos;

		m_fEjectInitialSpeed += Time.deltaTime * m_fEjectAccelerate;
		if (m_fEjectInitialSpeed <= 0f) {
			EndEject();
		}

	//	SetPos ( vecTempPos );

        bool bRet = CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2); 
		this.gameObject.transform.position = vecTempPos2;
        if ( bRet )
        {
            EndEject();

        }

		SetDirLineVisible ( false );
	}

	public void SetDirLineVisible ( bool bVisible )
	{
		if (_ba == null) {
			return;
		}

		_ba._dir_line.gameObject.SetActive ( bVisible );
	}

	int m_nUnfoldStatus = 0;
	public bool CheckIfCanUnfold()
	{
		if (m_nUnfoldStatus > 0) {
			return false;
		}

		return true;
	}

	float m_fPreUnfoldTimeCount = 0f;
	public void BeginPreUnfold()
	{
		vecTempScale.x = Main.s_Instance.m_fUnfoldSale;
		vecTempScale.y = Main.s_Instance.m_fUnfoldSale;
		vecTempScale.z = 1f;
		_obsceneCircle.transform.localScale = vecTempScale;
		_obsceneCircle.gameObject.SetActive ( true );
//		_obsceneCircle.Begin ( Main.s_Instance.m_fMainTriggerPreUnfoldTime );
		m_nUnfoldStatus = 1;
		m_fPreUnfoldTimeCount = 0f;
	}

	void PreUnfolding()
	{
		if (m_nUnfoldStatus != 1) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		m_fPreUnfoldTimeCount += Time.deltaTime;
		if ( m_fPreUnfoldTimeCount >= Main.s_Instance.m_fMainTriggerPreUnfoldTime ) {
			_obsceneCircle.gameObject.SetActive ( false );
            vecTempScale = _obsceneCircle.transform.localScale;
            vecTempScale.x = 1f;
            vecTempScale.y = 1f;
            vecTempScale.z = 1f;
            _obsceneCircle.transform.localScale = vecTempScale;
			BeginUnfold ();
		}
	}

	float m_fUnfoldLeftTime = 0.0f;

	float m_fRealSize = 0.0f;
	public bool IsUnfolding()
	{
		return ( m_nUnfoldStatus == 2 );
	}

	void BeginUnfold( )
	{
		m_nUnfoldStatus = 2;
		m_fUnfoldLeftTime = Main.s_Instance.m_fMainTriggerUnfoldTime;
		vecTempSize.x = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.y = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.z = 1.0f;
		_Mouth.transform.localScale = vecTempSize;
		CalculateMianJiByRadius ();
	}


	void Unfolding()
	{
		if (m_nUnfoldStatus != 2) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		m_fUnfoldLeftTime -= Time.deltaTime;
		if (m_fUnfoldLeftTime <= 0.0f) {
			EndUnfold ();
		}

	}

    public void EndUnfold()
    {
		m_nUnfoldStatus = 0;
		vecTempScale.x = 1f;
		vecTempScale.y = 1f;
		vecTempScale.z = 1f;
		_Mouth.transform.localScale = vecTempScale;
		CalculateMianJiByRadius ();
	}


	public void ProcessNail( Nail nail )  
	{
		if (this.IsEjecting ()) {
			this.EndEject ();
		}

		this.gameObject.transform.parent = Main.s_Instance.m_goBallsBeNailed.transform;
	}

	public void ProcessPk( Ball ballOpponent )
	{
		if (Main.s_Instance.m_nTestStatus == 1) {
			return;
		}

		if (this._balltype != eBallType.ball_type_ball) {
			return;
		}

		if ((!this._Player.photonView.isMine) && (!ballOpponent._Player.photonView.isMine)) { // 跟路人甲无关，只跟主吃方和被吃方这两个客户端相关
			return;
		}


		if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId) { // 队友
			if (this.IsEjecting () || ballOpponent.IsEjecting ()) {
				return;
			}
			if (this.HaveShell () || ballOpponent.HaveShell ()) {
				return;
			}
		}


		if (IsDead () || ballOpponent.IsDead ()) {
			return;
		}

		if ( ( this._Player != null && this._Player.IsProtect () ) || ( ballOpponent._Player != null && ballOpponent._Player.IsProtect () )) {
			return;
		}

		int nRet = Ball.WhoIsBig (this, ballOpponent);
		if (nRet == 1) {
			this.PreEat (ballOpponent);
		}
		/*
		else if (nRet == -1) {
			ballOpponent.PreEat (this);
		}
		*/else {
			if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId) {
				this.EatTeammate (ballOpponent);
			}
		}
	}

	public void EatTeammate( Ball ballOpponent )
	{
		CircleCollider2D c1 = this._Trigger;
		CircleCollider2D c2 = ballOpponent._Trigger;
		float r1 = c1.bounds.size.x / 2f ;
		float r2 = c2.bounds.size.x / 2f ;
		float fTotal = r1 + r2;
		float fPercent = 1 - Vector2.Distance ( c1.bounds.center, c2.bounds.center ) / fTotal; 
		if (fPercent >= MapEditor.s_Instance.m_fYaQiuBaiFenBi * 0.9f ) {
			this.Eat ( ballOpponent );
		}
	}

	public void ProcessTattooFood( TattooFood food )
	{
//		if (!photonView.isMine) {
//			return;
//		}

		if (food.IsDestroyed ()) {
			return;
		}

		if (CheckIfTotallyCover( _Trigger, food._Trigger ) ) {
			EatTattooFood ( food );
		}
	}

	public void DoEatBeiShuDouZi ( TattooFood food )
	{
		float fCurSize = GetSize ();
		float fCurArea = CyberTreeMath.SizeToArea ( fCurSize, Main.s_Instance.m_nShit );
		float fNewArea = fCurArea * (float)food.GetParam( 0 );
		float fAreaToAdd = fNewArea - fCurArea;
		AddArea ( fAreaToAdd );
		//float fNewSize = CyberTreeMath.AreaToSize ( fCurArea, Main.s_Instance.m_nShit );
		//SetSize ( fNewSize );

		Main.s_Instance.m_MainPlayer.RemoveOnetattoo( food );
	}

	public void EatBeiShuDouZi ( TattooFood food )
	{
		switch (food._subtype) {
		case 0: // 单个球加体积
			{
				/*
				float fCurSize = GetSize ();
				float fCurArea = CyberTreeMath.SizeToArea ( fCurSize, Main.s_Instance.m_nShit );
				fCurArea *= (float)food.GetParam( 0 );
				float fNewSize = CyberTreeMath.AreaToSize ( fCurArea, Main.s_Instance.m_nShit );
				SetSize ( fNewSize );
				Main.s_Instance.m_MainPlayer.RemoveOnetattoo( food );
				*/
				DoEatBeiShuDouZi ( food );
			}
			break;
		case 1: // 整个队伍加体积
			{
				List<Ball> lst = Main.s_Instance.m_MainPlayer.GetBallList ();
				for (int i = lst.Count - 1; i >= 0; i--) {
					Ball ball = lst [i];
					if (ball == null) {
						continue;
					}
					ball.DoEatBeiShuDouZi ( food );
				}
			}
			break;
		}
	}

	public void EatTattooFood( TattooFood food )
	{
		switch (food._type) {
		case TattooFood.eTattooFoodType.TattooFood_BeiShuDouZi:
			{
				EatBeiShuDouZi ( food );
			}
			break;
		}
	}

	void PreEat( Ball ballOpponent )
	{
		if (this._balltype != eBallType.ball_type_ball) {
			return;
		}

        if (MapEditor.GetEatMode() == MapEditor.eEatMode.yaqiu ||
			(ballOpponent._balltype == eBallType.ball_type_bean || ballOpponent._balltype == eBallType.ball_type_thorn || ballOpponent._balltype == eBallType.ball_type_spore)
		
        )
        {
            if (!CheckIfPartiallyCover(ballOpponent))
            {
                return;
            }
        }
		else if (MapEditor.GetEatMode() == MapEditor.eEatMode.xiqiu) 
        {
			DrawBall ( ballOpponent );
			return;
		}
		this.Eat (ballOpponent);
	}

	public void DoDraw()
	{
		float fCurArea = GetArea (Main.s_Instance.m_nShit);
		float fAreaDelta = m_fPreDrawDeltaTime * MapEditor.s_Instance.m_fXiQiuSuDu;
		m_fPreDrawDeltaTime = 0.0f;
		float fNewArea = fCurArea + fAreaDelta;
		if (fNewArea <= 0.0f) {
			SetDead ( true );
			DestroyBall ();
			return;
		}
		Local_SetSize ( CyberTreeMath.AreaToSize ( fNewArea, Main.s_Instance.m_nShit ) ); 
		SetNeedSyncSize ();
	}

	void DrawBall(  Ball ballOpponent )
	{
		/*
		if (this.photonView.ownerId == ballOpponent.photonView.ownerId) {
			if (this.m_bShell && ballOpponent.m_bShell) {
				return;
			}
		}
		*/
		if (ballOpponent.IsDead() ) {
			return;
		}

		// 因为被吸的是绝对数量而不是自身体积的百分比，因此非常好计算。
		this.PreDraw(Time.deltaTime);
		ballOpponent.PreDraw ( -Time.deltaTime );
	}

	float m_fPreDrawDeltaTime = 0.0f;
	public void PreDraw( float fDeltaTime )
	{
		m_fPreDrawDeltaTime += fDeltaTime;

	}

	public void AddArea( float fAreaToAdd )
	{

			float fCurArea = CyberTreeMath.SizeToArea ( GetSize(), Main.s_Instance.m_nShit );
			float fNewArea = fCurArea + fAreaToAdd;
			float fNewSize = CyberTreeMath.AreaToSize ( fNewArea, Main.s_Instance.m_nShit );
			SetSize ( fNewSize );

	}

	public void AddSize( float fSizeToAdd )
	{
		if (IsUnfolding ()) {

		} else {
			SetSize (CyberTreeMath.CalculateNewSize ( GetSize(), fSizeToAdd, Main.s_Instance.m_nShit));
		}
	}

	void Eat( Ball ballOpponent )
	{
		EatBall ( ballOpponent );   
	}

	public void EatBean( Bean bean )
	{
		if (_Player == null) {
			return;
		}

		if (bean.IsDead ()) { // 避免重复吃豆子
			return;
		}

		bean.SetDead ( true );

		if ( _Player.photonView.isMine) { // 主角只在自己那个客户端涨体积，在别人的客户端吃了豆子就不涨了，只涨一边
			float fCurSize = GetSize ();
			float fBeanSize = Main.s_Instance.m_fBeanSize;
			float fAreaToAdd = CyberTreeMath.SizeToArea (fBeanSize, Main.s_Instance.m_nShit);
			float fCurArea = CyberTreeMath.SizeToArea ( fCurSize, Main.s_Instance.m_nShit );
			float fNewArea = fCurArea + fAreaToAdd;
			float fNewSize = CyberTreeMath.AreaToSize ( fNewArea, Main.s_Instance.m_nShit );
			_Player.SetBallSize( GetIndex(), fNewSize );
		}
	
		DestroyBean ( bean );
	}

    Thorn.ColorThornParam m_paramColorThorn = new Thorn.ColorThornParam();
    public void EatThorn( Thorn thorn )
	{
		if ( !_Player.photonView.isMine) { // 在“伪随机”机制做出来之前，暂时不要吃别人客户端的刺
			return;
		}

		if (thorn.IsDead ()) { // 避免重复吃刺
			return;
		}

		// 已经不能爆球了就别吃刺
		if (GetSize () < GetMinExplodeSize() || GetSize ()  < Main.s_Instance.m_fMinDiameterToEatThorn ) {
			return;
		}

		float fCurThornSize = GetThornSize ();
        int nColorIdx = thorn.GetColorIdx();
        m_paramColorThorn = MapEditor.GetColorThornParamByIdx(nColorIdx);
        float fAddedThornSize = Main.s_Instance.m_fThornSize;
        if (nColorIdx > 0)
        {
            fAddedThornSize = m_paramColorThorn.thorn_size;
        }

		// 吃刺也会增加球本身的总体积。但如果当前队伍已经不能爆刺了，则吃刺不增加球的体积
		float fCurBallSize = GetSize();
		float fAddedBallSize = Main.s_Instance.m_fThornContributeToBallSize;
		if (nColorIdx > 0)
		{
			fAddedBallSize = m_paramColorThorn.ball_size;
		}

		_Player.SetBallSize (GetIndex (), CyberTreeMath.CalculateNewSize (fCurBallSize, fAddedBallSize, Main.s_Instance.m_nShit));


		_Player.SetThornOfBallSize ( GetIndex(),  CyberTreeMath.CalculateNewSize ( fCurThornSize, fAddedThornSize, Main.s_Instance.m_nShit ));
		_Player.DestroyThorn ( thorn );

	}

	public void SetThornSize( float fThornSize )
	{
		Local_SetThornSize (fThornSize);
	}

	[PunRPC]
	public void RPC_SetThornSize( float fThornSize )
	{
		Local_SetThornSize (fThornSize);
	}

	float m_fThornSize = 0.0f;
	public void Local_SetThornSize( float fThornSize )
	{
		if (_thorn == null) {
			return;
		}

		m_fThornSize = fThornSize;
		float fBallSize = GetSize ();
		if (m_fThornSize > fBallSize) {
			m_fThornSize = fBallSize;
		}
		if (fBallSize == 0) {
			return;
		}
		float fThornRealScaleSize = m_fThornSize / fBallSize;
		vecTempScale.x = fThornRealScaleSize;
		vecTempScale.y = fThornRealScaleSize;
		vecTempScale.z = 1.0f;
		_thorn.transform.localScale = vecTempScale;
		//_thornForAlpha.transform.localScale = vecTempScale;
		if ( _Player.photonView.isMine && CheckIfExplode ()) {
			Explode ();
		}
	}

	public float GetThornSize()
	{
		return m_fThornSize;
	}

	bool  CheckIfExplode()
	{
		float fBallSize = GetSize ();
		if (m_fThornSize < fBallSize ) {
			return false;
		}

		if (fBallSize < 2.0f) {
			return false;
		}

		return true;
	}

	public static Vector2[] s_aryBallExplodeDirection = { 
		new Vector2( 0.0f, 1.0f ),
		new Vector2( 0.707f, 0.707f ),
		new Vector2( 1.0f, 0.0f ),
		new Vector2( 0.707f, -0.707f ),
		new Vector2( 0, -1.0f ),
		new Vector2( -0.707f, -0.707f ),
		new Vector2( -1.0f, 0.0f ),
		new Vector2( -0.707f, 0.707f ),

	};

	/*
	 * 爆裂的规则：
	 * 1、准备爆裂的母球，半径最小为2。不然没法爆，因为爆出的小球最小半径得是1（一个孢子的尺寸），并且爆了之后母球剩下的半径至少也得是1.
	 * 2、母球分一半的半径出来爆。
	 * 3、能爆成8等分尽量8等分（确保爆出来的每个小球的半径大于等于1）；不够8等分则能爆几个算几个。
	 * */
	public float GetMinExplodeSize()
	{
		return 2.0f * Main.BALL_MIN_SIZE;
	}

    float m_fShitExplodeTime = 0.0f;

	public void Explode ( float fPercent = 0f )
	{
		if ( !_Player.photonView.isMine ) {
			return;
		}

		if (!CheckIfReallyCanExplode ()) {
			return;
		}

		Main.s_Instance.BeginCameraSizeProtect (  Main.s_Instance.m_fExplodeRunTime );
		_Player.ExplodeBall ( GetIndex(), fPercent );
	}

	public bool CheckIfReallyCanExplode( float fPercent = 0f )
	{
		int nAvailableNum = _Player.GetCurAvailableBallCount ();
		if (nAvailableNum <= 1) {
			return false;
		}

		float fBallSize = GetSize ();
		float fMotherCurArea = CyberTreeMath.SizeToArea (fBallSize, Main.s_Instance.m_nShit);
		if (fPercent == 0f) {
			fPercent = Main.s_Instance.m_fExplodePercent;
		}
		float fChildsArea = fMotherCurArea * Main.s_Instance.m_fExplodePercent; // 母球分出来的面积比例
		int nExpectNum = (int)Main.s_Instance.m_fExpectExplodeNum;
		; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		float fChildSize = 0.0f;
		if (nExpectNum > nAvailableNum) {
			nExpectNum = nAvailableNum;
		}
		for (nRealNum = nExpectNum; nRealNum >= 1; nRealNum--) {
			fChildSize = CyberTreeMath.AreaToSize (fChildsArea / nRealNum, Main.s_Instance.m_nShit);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}

		if (nRealNum == 0) {
			return false;
		}

		return true;
	}

	public void DoExplode( float fExplodePosX, float fExplodePosY, double dOccurTime, float fPercent = 0f )
	{


		if (IsDead ()) {
			Debug.LogError ( "DoExplode   IsDead ()" );
			return;
		}

		if (Main.s_Instance.m_fExplodePercent <= 0f) {
			return;
		}

		int nAvailableNum = _Player.GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		float fDelayTime = (float)(PhotonNetwork.time - dOccurTime);
		//MapEditor.s_Instance.UpdateDebugInfo ( fDelayTime.ToString() );

		float fBallSize = GetSize ();
		float fMotherCurArea = CyberTreeMath.SizeToArea (fBallSize, Main.s_Instance.m_nShit);
		if (fPercent == 0f) {
			fPercent = Main.s_Instance.m_fExplodePercent;
		}
		float fChildsArea = fMotherCurArea * fPercent; // 母球分出来的面积比例
		int nExpectNum =  (int)Main.s_Instance.m_fExpectExplodeNum;; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		float fChildSize = 0.0f;
		if (nExpectNum > nAvailableNum) {
			nExpectNum = nAvailableNum;
		}
		for (nRealNum = nExpectNum; nRealNum >= 2; nRealNum--) {
			fChildSize = CyberTreeMath.AreaToSize (fChildsArea / nRealNum, Main.s_Instance.m_nShit);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}

		if (nRealNum < 2 ) {
			return;
		}

		SetThornSize (0.0f);

		float fRealRunTime =  Main.s_Instance.m_fExplodeRunTime - fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}

		// 生成小球
		float fRadiusMother = GetBoundsSize() / 2.0f;
		vecTempPos = GetPos();
		float fRunDistance = Main.s_Instance.m_fExplodeRunDistanceMultiple * fRadiusMother;
		for (int i = 0; i < nRealNum; i++) {
			Ball new_ball =  GenerateNewBall( fExplodePosX, fExplodePosY, fRealRunTime, fChildSize, fRunDistance, i );
			//Debug.Log ( "生成小球：" + new_ball.GetIndex() );
		}
		// 母球剩余
		float fMotherLeftArea = fMotherCurArea - fChildsArea;
		if (fMotherLeftArea <= 0f || Main.s_Instance.m_fExplodePercent >= 1f) {
			_Player.DestroyBall ( GetIndex() );
		} else {
			float fMotherLeftSize = CyberTreeMath.AreaToSize (fMotherLeftArea, Main.s_Instance.m_nShit);
			Local_SetSize (fMotherLeftSize);
			Local_SetShellInfo ( Main.s_Instance.m_fExplodeShellShrinkTotalTime, 0f, eShellType.explode_ball );
			Local_BeginShell ();
		}
	}

	Ball GenerateNewBall( float fExplodePosX, float fExplodePosY, float fRealRunTime, float fChildSize, float fRunDistance, int idx )
    {
		Ball new_ball = _Player.ReuseOneBall ();
		if (new_ball == null) {
			Debug.LogError ( "GenerateNewBall" );
			return null;
		}

		//MapEditor.s_Instance.UpdateDebugInfo ( fRealRunTime.ToString() );
		Vector2 vecDire = MapEditor.s_Instance.GetExplodeDirection ( idx );//s_aryBallExplodeDirection[idx];

		//float fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, fRealRunTime  );
		//float fAccelerate = CyberTreeMath.GetA ( fRunDistance, fRealRunTime);
		vecTempPos = GetPos ();
		vecTempPos.x = fExplodePosX;
		vecTempPos.y = fExplodePosY;
		new_ball.SetShellEnable ( false );  
		//new_ball.Local_BeginEject (fInitSpeed, fAccelerate, fRealRunTime, false, false, Main.s_Instance.m_fExplodeStayTime);
		float fSpeed = fRunDistance / fRealRunTime;
		new_ball.Local_SetSize(fChildSize);
		new_ball._dirx = vecDire.x;
		new_ball._diry = vecDire.y;
		new_ball.Local_SetShellInfo( Main.s_Instance.m_fExplodeShellShrinkTotalTime, 0f, eShellType.explode_ball );	
		new_ball.Local_SetPos ( vecTempPos );
		new_ball.Local_BeginEject_New( fSpeed, fRunDistance, false, false, Main.s_Instance.m_fExplodeStayTime);

		return new_ball;
    }

    public struct EatNode
    {
        public Ball eatenBall;
        public float eatenSize; 
    };

	List<EatNode> m_lstEaten = new List<EatNode>();

    void AddEatenNode(EatNode node)
    {
        m_lstEaten.Add( node );
    }

	void ProcessEatenNode()
    {
        for ( int i = m_lstEaten.Count - 1; i >= 0; i--  )
        {
		    EatNode node = m_lstEaten[i];

            if (node.eatenBall == null)
            {
				float fMotherSize = GetSize();
                float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, node.eatenSize, Main.s_Instance.m_nShit);
				SetSize(fNewSize);
                m_lstEaten.Remove( node );
            }
        }
    }

	bool CheckIfInEatenList( Ball the_eater, Ball ballOpponent)
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];
			if ( node.eatenBall == the_eater ||  node.eatenBall == ballOpponent)
            {
                return true;
            }
        }

        return false;
    }

	bool m_bEaten = false;
	public void SetEaten( bool val )
	{
		m_bEaten = val;
	}

	public void SetVisible( bool val )
	{
		this.gameObject.SetActive ( val );
	}

	public bool IsEaten()
	{
		return m_bEaten;
	}

	public virtual void SetDead( bool val )
	{
		m_bDead = val;
		this.gameObject.SetActive( !m_bDead );
		_Player.CheckIfPlayerDead(); // 判断下整个玩家死没有
		if (m_bDead ) { // 死了一个球
			ProcessGroupWhenDead();
			this.gameObject.name = "ball" + m_nIndex + "(dead)";
		} else {
			this.gameObject.name = "ball" + m_nIndex;
		}
	}

	void ProcessGroupWhenDead()
	{
		if (m_Group) {
			m_Group.Local_RemoveOneBall ( this );
		}
	}

	public void Hide()
	{
		vecTempPos = GetPos ();
		vecTempPos.x = -10000;
		vecTempPos.y = -10000;
		Local_SetPos ( vecTempPos );
	}

	// 发起“吃球”事件。发起该事件的客户端可能是主吃方，也可能是被吃方，以那边先触发为准
	public void EatBall( Ball ballOpponent ) 
	{
		if ( ( !this._Player.photonView.isMine ) && ( !ballOpponent._Player.photonView.isMine ) ) { // 发起吃球的客户端必须是MainPlayer
			return;
		}

		if (ballOpponent.IsEaten ()) { // 避免重复吃球 
			return;
		}

		if (ballOpponent.IsDead ()) {
			Debug.LogError ( "已经死了从个还在吃哦" );
			return;
		}

		float fEaterSize = GetSize ();
		float fPoorthingSize = ballOpponent.GetSize ();
		float fEaterArea = CyberTreeMath.SizeToArea ( fEaterSize, Main.s_Instance.m_nShit );
		float fPoorthingArea = CyberTreeMath.SizeToArea ( fPoorthingSize, Main.s_Instance.m_nShit );
		float fNewArea = fEaterArea + fPoorthingArea;
		float fNewSize = CyberTreeMath.AreaToSize( fNewArea, Main.s_Instance.m_nShit );
		//_Player.SetBallSize ( this.GetIndex(), fNewSize );
		this._Player.photonView.RPC ( "RPC_EatSucceed", PhotonTargets.All, this.GetIndex(), fNewSize );

		ballOpponent.SetEaten (true); // 先把被吃者状态置为“已吃”，但还不是“Dead状态”，Dead状态必须由该球的MainPlayer来发起
		ballOpponent.SetVisible( false );
		ballOpponent._Player.photonView.RPC ( "RPC_BeEaten", PhotonTargets.All, ballOpponent.GetIndex() );
	}

	public void DestroyBall()
	{
		
	}




	void FrameInterpolation( Vector3 dest )
	{
		Local_SetPos (dest.x, dest.y, dest.z);
	}


	public void Terminate()
	{
		//if (this.photonView.isMine) {
		//	PhotonNetwork.Destroy ( this.gameObject );
		//}
	}

	public static int WhoIsBig( Ball ball1, Ball ball2 )
	{
		float fSize1 = CyberTreeMath.FloatTrim ( ball1._Trigger.bounds.size.x );
		float fSize2 = CyberTreeMath.FloatTrim ( ball2._Trigger.bounds.size.x );
		if (fSize1 == fSize2) {
			return 0;
		} else if (fSize1 > fSize2) {
			return 1;
		} else {
			return -1;
		}
	}

	bool CheckIfTotallyCover( CircleCollider2D circle1, CircleCollider2D circle2  )
	{
		float r1 = circle1.bounds.size.x / 2.0f;
		float r2 = circle2.bounds.size.x / 2.0f;

		float deltaX = circle1.bounds.center.x - circle2.bounds.center.x;
		float deltaY = circle1.bounds.center.y - circle2.bounds.center.y;
		float dis = Mathf.Sqrt ( deltaX * deltaX + deltaY * deltaY );
		if (dis <= Mathf.Abs (r1 - r2)) {
			return true;
		}

		return false;
	}


	bool CheckIfPartiallyCover( Ball ballOpponent )
	{
		if (this._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId) {

		} else {
			if (m_fMianJi < MapEditor.s_Instance.m_fYaQiuTiaoJian * ballOpponent.GetMianJi ()) {
				return false;
			}
		}	
		CircleCollider2D c1 = this._Trigger;
		CircleCollider2D c2 = ballOpponent._Trigger;

		float  d;
		float  s,s1,s2,s3,angle1,angle2;
        float r1 = c1.bounds.size.x / 2f ;//m_fRadius;
        float r2 = c2.bounds.size.x / 2f ;//GetRadius ();

		d = Mathf.Sqrt((c1.bounds.center.x-c2.bounds.center.x)*(c1.bounds.center.x-c2.bounds.center.x)+(c1.bounds.center.y-c2.bounds.center.y)*(c1.bounds.center.y-c2.bounds.center.y));
		if (d >= (r1 + r2)) {//两圆相离
			return false;
		}
		if ((r1 - r2) >= d) {//两圆内含,c1大
			return true;
		}

		angle1 = Mathf.Acos((r1*r1+d*d-r2*r2)/(2*r1*d));
		angle2 = Mathf.Acos((r2*r2+d*d-r1*r1)/(2*r2*d));

			s1=angle1*r1*r1;
		    s2=angle2*r2*r2;
			s3=r1*d*Mathf.Sin(angle1);
			s=s1+s2-s3;

		float fCurPercent = s / ballOpponent.GetMianJi();
		//MapEditor.s_Instance.UpdateDebugInfo( fCurPercent.ToString() );
		if (fCurPercent >= MapEditor.s_Instance.m_fYaQiuBaiFenBi ) {
			return true;
		}

		return false;
	}

	public bool CheckIfCanForceSpit()
	{
		if (IsEjecting ()) {
			return false;
		}

		return true;
	}

	public bool CheckIfCanSpitBall( float fMotherSize, float fChildSize, ref float fMotherLeftSize  )
	{
		fMotherLeftSize= CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		return fMotherLeftSize > Main.BALL_MIN_SIZE;
	}

	public void  SpitBean()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.s_Instance.m_fBeanSize, ref fMotherLeftSize)) {
			return;
		}

		Bean spore = ResourceManager.ReuseBean(); // 孢子没必要具有网络属性，就是每个客户端的单机版
		if (spore == null) {
			return;
		}
		float dirx =  GetDir ().x;
		float diry =  GetDir ().y;

		spore._beanType = Bean.eBeanType.spore;
		spore.transform.parent = Main.s_Instance.m_goSpores.transform;
		//spore._srMain.color = this._srMouth.color;
		Local_SetSize ( fMotherLeftSize );
		spore.Local_SetDir( dirx, diry );
		vecTempPos = GetPos();
		vecTempPos.x += ( GetRadius() + spore._srMain.bounds.size.x ) * dirx;
		vecTempPos.y += ( GetRadius() + spore._srMain.bounds.size.x ) * diry;
		vecTempPos.z = -spore.GetSize();
		spore.Local_SetPos(vecTempPos.x, vecTempPos.y, vecTempPos.z);
		float fSpitBallInitSpeed = CyberTreeMath.GetV0 ( Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime  );
		float fSpitBallAccelerate = CyberTreeMath.GetA (  Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime);
		spore.Local_BeginEject ( fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitRunTime );
	}

	// 废弃。没有“孢子”的概念了。就吐豆子SpitBean
	/*
	public Spore SpitSpore()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return null;
		}
		Main.s_Instance.s_Params [0] = Main.eInstantiateType.Spore;
		Main.s_Instance.s_Params [1] = 444;
		GameObject goSpore = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preSpore, Vector3.zero, Main.s_Instance.s_Params );//GameObject.Instantiate( Main.s_Instance.m_preSpore );
		Spore spore = goSpore.GetComponent<Spore>();
		goSpore.transform.parent = Main.s_Instance.m_goSpores.transform;
		spore._srMain.color = this._srMain.color;
		SetSize ( fMotherLeftSize );
        spore.SetDir( _dirx, _diry );
        vecTempPos = GetPos();
        vecTempPos.x += ( GetRadius() + spore.GetRadius() ) * _dirx;
        vecTempPos.y += ( GetRadius() + spore.GetRadius() ) * _diry;
        vecTempPos.z = -spore.GetSize();
        spore.SetPos(vecTempPos);
        float fSpitBallInitSpeed = CyberTreeMath.GetV0 ( Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime  );
        float fSpitBallAccelerate = CyberTreeMath.GetA (  Main.s_Instance.m_fSpitSporeRunDistance, Main.s_Instance.m_fSpitRunTime);
        spore.BeginEject ( fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitRunTime );
		return spore;

	}
	*/



	public float GetBoundsSize()
	{
		return _srMouth.bounds.size.x; // ;_Trigger.bounds.size.x;
	}

	public void GetDir( ref float fDirX, ref float fDirY )
	{
		fDirX = _dirx;
		fDirY = _diry;
	}

	public Vector2 GetDir()
	{
		if (_Player.photonView.isMine ) {
			m_vecdirection.x = _dirx;
			m_vecdirection.y = _diry;
		} else {

		}
		return m_vecdirection;
	}

	public Vector3 GetPos()
	{
		return this.gameObject.transform.position;
	}

	public void GetPos( ref float fX, ref float fY )
	{
		fX = this.gameObject.transform.position.x;
		fY = this.gameObject.transform.position.y;
	}

	public float GetPosX()
	{
		return GetPos().x;
	}

	public float GetPosY()
	{
		return GetPos().y;
	}

	public void SetLocalPosition( Vector3 pos )
	{
		this.transform.localPosition = pos;
	}

	public Vector3 GetLocalPosition()
	{
		return this.transform.localPosition;
	}

	public void SetPos( Vector3 pos )
	{
		Local_SetPos ( pos );
	//	photonView.RPC ( "RPC_SetPos", PhotonTargets.All, pos.x, pos.y, pos.z );
	}

	[PunRPC]
	public void RPC_SetPos( float fX, float fY, float fZ )
	{
         Local_SetPos(fX, fY, fZ);
	}


	public void Local_SetPos( float fX, float fY, float fZ  )
	{
		vecTempPos.x = fX;
		vecTempPos.y = fY;
		vecTempPos.z = fZ;
		this.gameObject.transform.position = vecTempPos;
	}

	public void Local_SetPos( Vector3 pos  )
	{
		this.gameObject.transform.position = pos;
	}

	public void SetDir( Vector2 dir )
	{
		Local_SetDir ( dir );
	}

	public void SetDir( float fX, float fY )
	{
		//photonView.RPC ( "RPC_SetDir", PhotonTargets.All, fX, fY );
	}

	[PunRPC]
	public void RPC_SetDir( float fX, float fY )
	{
		Local_SetDir(  fX,  fY );
	}

	public void Local_SetDir(  Vector2 dir  )
	{
		this._dirx = dir.x;
		this._diry = dir.y;
	}

	public void Local_SetDir(  float fX, float fY  )
	{
		this._dirx = fX;
		this._diry = fY;
	}





	float m_fChildSize = 0.0f;
	float m_fMotherLeftSize = 0.0f;
	float m_fChildThornSize = 0.0f;
	float m_fMotherLeftThornSize = 0.0f;
	public void CalculateChildSize( float fForceSpitPercent )
	{
        /*
		float fChildSize = 0.0f;
		float fMotherSize = GetSize ();
		float fMotherLeftSize = 0.0f; 
		fChildSize = fMotherSize * fForceSpitPercent;
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}

		fMotherLeftSize = fMotherSize - fChildSize;
		if ( fChildSize >= Main.BALL_MIN_SIZE &&  fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		} 
		*/

        // 按面积来算，不能简单粗暴的按直径来算

        float fMotherSize = GetSize ();
		float fMotherThornSize = GetThornSize ();

        /*
		float fRealPercent = CyberTreeMath.AreaToSize (fForceSpitPercent, Main.s_Instance.m_nShit );
		fChildSize = fMotherSize * fRealPercent; 
		m_fChildThornSize = fMotherThornSize * fRealPercent; 
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}
		fMotherLeftSize = CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		m_fMotherLeftThornSize = Mathf.Sqrt ( fMotherThornSize * fMotherThornSize - m_fChildThornSize * m_fChildThornSize );
		if (fChildSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		}
        */

        float fMotherArea = CyberTreeMath.SizeToArea(fMotherSize, Main.s_Instance.m_nShit);
        float fChildArea = fForceSpitPercent * fMotherArea;
        float fChildSize = CyberTreeMath.AreaToSize(fChildArea, Main.s_Instance.m_nShit);
        float fMotherLeftArea = fMotherArea - fChildArea;
        float fMotherLeftSize = CyberTreeMath.AreaToSize(fMotherLeftArea, Main.s_Instance.m_nShit);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
        
    }

	public void ClearAvailableChildSize()
	{
		m_fChildSize = 0.0f;
	}

	public float GetAvailableChildSize( ref float fMotherLeftSize )
	{
		fMotherLeftSize = m_fMotherLeftSize;
		return m_fChildSize;
	}

	bool m_bShell = false;
	float m_fShellShrinkCurTime = -1f;
	float m_fShellTotalTime = 0f;

	public void SetShellInfo( float fTotalTime, float fStarTime )
	{
		if (_shell == null) {
			Debug.LogError ( "if (_shell == null) " );
			return;
		}

		/*
		if ( photonView.isMine )
		{
			photonView.RPC ( "Local_SetShellInfo", PhotonTargets.All, fTotalTime, fStarTime );
		}
		*/
	}

	public void Local_SetShellInfo( float fTotalTime, float fStarTime, eShellType type )
	{
		m_fShellTotalTime = fTotalTime;
		m_fShellShrinkCurTime = fStarTime;
		SetCurShellType ( type );
	}

	public enum eShellType
	{
		none,
		w_spilt_ball, // w键吐球
		r_split_ball, // r键分球
		explode_ball, // 炸球
	};
	eShellType m_eShellType = eShellType.none;

	public eShellType GetCurShellType()
	{
		return m_eShellType;
	}

	public void SetCurShellType( eShellType type )
	{
		m_eShellType = type;
	}

	public float GetShellTotalTimeByShellType( eShellType type )
	{
		switch (type) {
		case eShellType.w_spilt_ball:
			{
				return Main.s_Instance.m_fShellShrinkTotalTime;
			}
			break;
		case eShellType.r_split_ball:
			{
				return Main.s_Instance.m_fSplitShellShrinkTotalTime;
			}
			break;
		case eShellType.explode_ball:
			{
				return Main.s_Instance.m_fExplodeShellShrinkTotalTime;
			}
			break;
		}

		return 0f;
	}

	public float GetCurShellTime()
	{
		return m_fShellShrinkCurTime;
	}

	public void BeginShell()
	{
		if (_shell == null) {
			return;
		}

		/*
		if ( photonView.isMine )
		{
			photonView.RPC ( "RPC_BeginShell", PhotonTargets.All );
		}
		*/
	}

	[PunRPC]
	public void RPC_BeginShell( )
	{
		Local_BeginShell();
  }

	float m_fShellUnfoldScale = 0.1f;
	public void Local_BeginShell(  )
    {
		if (m_fShellTotalTime <= 0f) {
			return;
		}

        m_bShell = true;

		vecTempScale.x = s_vecShellInitScale.x * ( 1f + m_fShellUnfoldScale );
		vecTempScale.y = s_vecShellInitScale.x * ( 1f + m_fShellUnfoldScale );
        vecTempScale.z = 1.0f;
        SetShellSize(vecTempScale);
		_shell.gameObject.SetActive ( true );
		_Collider.enabled = true;
    }

	public bool HaveShell()
	{
		return m_bShell;
	}

	void Shell()
	{
		if (_shell == null) {
			return;
		}

		if (!m_bShell) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		if (_shell) {
			_shell.gameObject.SetActive (true);
		}
		m_fShellShrinkCurTime += Time.deltaTime;
		if (m_fShellTotalTime <= 0f) {
			m_fShellTotalTime = 1f;
			Debug.LogError ( "if (m_fShellTotalTime <= 0f) {" );
		}
		//float fLeftSize = 1.1f - 0.1f * m_fShellShrinkCurTime / m_fShellTotalTime/*Main.s_Instance.m_fShellShrinkTotalTime*/;
		float k = m_fShellUnfoldScale * ( 1f - m_fShellShrinkCurTime / m_fShellTotalTime );
		if (k <= 0f) {
			Local_EndShell ();
			return;
		}
		k += 1f;
		vecTempScale.x = s_vecShellInitScale.x * k;
		vecTempScale.y = s_vecShellInitScale.y * k;
		vecTempScale.z = 1f;

		SetShellSize(vecTempScale);
	}

	[PunRPC]
	public void RPC_EndShell()
	{
		Local_EndShell ();
	}

	void EndShell()
	{
		//photonView.RPC ( "RPC_EndShell", PhotonTargets.All );
	}

	public void Local_EndShell()
	{
		_Collider.enabled = false;
		m_bShell = false;
        m_fShellShrinkCurTime = -1f;

		vecTempScale.x = s_vecShellInitScale.x;
		vecTempScale.y = s_vecShellInitScale.x;
		vecTempScale.z = 1f;
		//_shell.transform.localScale = vecTempScale;
		SetShellSize(vecTempScale);

		/*
		cTempColor = _srShellForAlpha.color;
		cTempColor.a = 0.0f;
		_srShellForAlpha.color = cTempColor;
		*/
		if (_shell) {
			_shell.gameObject.SetActive (false);
		}

		m_eShellType = eShellType.none;
	}

	void SetShellSize( Vector3 vecScale )
	{
        if (!_shell)
        {
            return;
        }
		_shell.transform.localScale = vecScale;
		//_shellForAlpha.transform.localScale = vecScale;
	}

	public float m_fSpitBallInitSpeed = 5.0f;           // 分球的初速度
	public float m_fSpitBallAccelerate = 0.0f;         // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 

	public float m_fExplodeInitSpeed = 0.0f;
	public float m_fExplodeInitAccelerate = 0.0f;

	SpitBallTarget m_SpitBallTarget = null;
	float m_fSpitBallTargetParam = 0f;
	bool IfCanShowTarget()
	{
		if (IsEjecting ()) {
			return false;
		}


		return true;
	}

	public static void RongCuo_NotPointToMyself( ref float dirx, ref float diry  )
	{
		// 容错，不准往自己身上吐
		if (dirx == 0 && diry == 0f) {
			dirx = 0.5f;
			diry = 0.5f;
		}
	}

	public void ClearSpitTarget()
	{
		if (m_SpitBallTarget) {
			m_SpitBallTarget.SetBall ( null );
			m_SpitBallTarget.SetVisible (false);
		}
	}

	public void ShowTarget()
	{
		if (!IfCanShowTarget ()) {
			return;
		}

		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = ResourceManager.ReuseTarget ();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}
		m_SpitBallTarget.gameObject.SetActive ( true );
		m_SpitBallTarget.SetBall ( this );

		float fMotherSize = GetSize ();
		float fRadiusMother= GetBoundsSize () / 2.0f;

		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize ( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE  || fMotherLeftSize < fChildSize) {
			m_SpitBallTarget.gameObject.SetActive (false);
			return;
		} else {
			m_SpitBallTarget.gameObject.SetActive (true);
		}

		m_SpitBallTarget.SetSize ( fChildSize );
		float dirx = 0f;
		float diry = 0f;
		if (Main.s_Instance.m_nPlatform == 0) { // PC
			dirx = _dirx;
			diry = _diry;
		} else if (Main.s_Instance.m_nPlatform == 1) { // Mobile Phone
			dirx = Main.s_Instance.GetSpitDirection().x;
			diry = Main.s_Instance.GetSpitDirection().y;			
		}
		if (dirx == 0f && diry == 0f) {
			dirx = _dirx;
			diry = _diry;
		}

		RongCuo_NotPointToMyself ( ref dirx, ref diry );

		vecTempPos = GetPos ();
		float fDis = (Main.s_Instance.m_fSpitBallRunDistanceMultiple) * fRadiusMother;
		vecTempPos.x += fDis * dirx;
		vecTempPos.y += fDis * diry;
		m_SpitBallTarget.SetPos ( vecTempPos );
	}

	SpitBallTarget m_SplitBallTarget = null;
	public SpitBallTarget GetSplitTarget()
	{
		return m_SplitBallTarget;
	}

	public void UpdateSplitTarget ()
	{
		
	}

	public void ShowSplitTarget( float fCurOneBtnSplitDis )
	{
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = ResourceManager.ReuseTarget();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}
		m_SpitBallTarget.gameObject.SetActive ( true );
			
		float fSize = GetSize ();
		vecTempScale.x = fSize;
		vecTempScale.y = fSize;
		vecTempScale.z = 1.0f;
		m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

		float dirx = 0f;
		float diry = 0f;
		dirx = _dirx;
		diry = _diry;
		RongCuo_NotPointToMyself ( ref dirx, ref diry );


		vecTempPos = GetPos ();
		vecTempPos.x += fCurOneBtnSplitDis * dirx;
		vecTempPos.y += fCurOneBtnSplitDis * diry;
	//	Debug.Log (  fCurOneBtnSplitDis);

		m_SpitBallTarget.gameObject.transform.localPosition = vecTempPos;
	//	MapEditor.s_Instance._txtDebugInfo.text = m_SplitBallTarget.gameObject.transform.localPosition.ToString() + " _ " + vecTempPos.ToString ();

	}

	public void CalculateSpitBallRunParams( float fRadiusMother, float fRadiusChild, float fMultiple, ref float fInitSpeed, ref float fAccelerate )
	{
		float fRunDistance = fMultiple * fRadiusMother;
		fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fSpitRunTime  );
		fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fSpitRunTime);
	}

	public SpitBallTarget _relateTarget = null;
	public void RelateTarget( Ball ball )
	{
		return; // 废弃

		ball._relateTarget = m_SpitBallTarget;
	}

	public SpitBallTarget _relateSplitTarget = null;
	public void RelateSplitTarget( SpitBallTarget target )
	{
		_relateSplitTarget = target;
	}

	public void RefeshPosDueToOutOfScreen()
	{
/*		
		if (_balltype == eBallType.ball_type_thorn && _isSpited) {
			return;
		}
*/
		if (!CheckIfOutOfScreen ()) {
			return;
		}

		this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen ();
	}

	public bool CheckIfOutOfScreen()
	{
		Vector3 vecScreenPos = Camera.main.WorldToScreenPoint ( this.gameObject.transform.position );

		if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height) {
			return true;
		}

		return false;
	}

	/*
    public void AutoAttenuateThorn()
    {
        float fThornAttenuate = 0.0f;
        if (MapEditor.s_Instance)
        {
            fThornAttenuate = MapEditor.s_Instance.GetThornAttenuateSpeed();
        }
        float fCurThornSize = GetThornSize();
        fCurThornSize -= fCurThornSize * fThornAttenuate * Time.deltaTime;
        if (fCurThornSize < 0)
        {
            fCurThornSize = 0;
        }
        Local_SetThornSize(fCurThornSize);

    }
	*/

	/*
    public void AutoAttenuate( float val )
	{
		float fCurSize = GetSize ();
		if (fCurSize <= Main.BALL_MIN_SIZE) {
			return;
		}

		fCurSize -= fCurSize * val;
		if (fCurSize < Main.BALL_MIN_SIZE) {
			fCurSize = Main.BALL_MIN_SIZE;
		}
		Local_SetSize (  fCurSize );



   
    }
	*/

	void ShowSize()
	{
		if (_text) {
			string szInfo =  GetSize().ToString ("f2");
            //_text.text = szInfo;

            if (m_Group)
            {
                _text.text = GetLocalPosition().y.ToString("f2") + m_Group.GetPos().y.ToString("f2");
            }
            else
            {
                _text.text = GetLocalPosition().y.ToString("f2");
            }
		}

	}


    public void EatGrassSeed( int nGrassGUID )
    {
      //  photonView.RPC("RPC_EatGrassSeed", PhotonTargets.AllBuffered, this.photonView.ownerId, nGrassGUID, PhotonNetwork.time );
    }

    [PunRPC]
    public void RPC_EatGrassSeed( int nOwnerId, int nGrassGUID, double dCurTime )
    {
        Local_EatGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

    public void Local_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        MapEditor.s_Instance.ProcessGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

	public void SyncBaseInfo( bool bDirect = true )
	{
		return;

		if (Main.s_Instance.m_nTestStatus == 1) {
			return;
		}
		/*
		if (photonView == null) {
			return;
		}

        if (!photonView.isMine)
        {
            return;
        }

		if (photonView.ownerId < 0) {
			return;
		}
		*/
        if (IsEjecting())
        {
            return;
        }

		if (IsCaptured()) {
			return;
		}

        vecTempPos = GetPos ();
		//photonView.RPC ("RPC_SyncBaseInfo", PhotonTargets.Others, vecTempPos.x, vecTempPos.y, bDirect );
	}

	public bool DoNotMove()
	{	
		if (IsStaying ()) {
			return true;
		}

		if ( IsEjecting())
		{
			return true;
		}

		if (IsCaptured ()) {
			return true;
		}

		if (IsDead ()) {
			return true;
		}

		if (IsEaten ()) {
			return true;
		}

		if (GetMovingToCenterStatus () == 1) {
			return true;
		}

		return false;
	}

	float m_fLastShit = 0f;
	public void SetAdjustInfo_InGroup( float fPosX, float fPosY )
	{
		vecTempPos = GetLocalPosition ();  
		vecTempPos.x = fPosX;
		vecTempPos.y = fPosY;
		 SetLocalPosition ( vecTempPos );
	}

	float m_fAdjustSpeedX = 0f;
	float m_fAdjustSpeedY = 0f;
	int m_nAdjustShit = 10;
	public void SetAdjustInfo( float fPosX, float fPosY, int nDirect = 0  )
	{
		float fDeltaX = 0f;
		float fDeltaY = 0f;
		if (DoNotMove()) {
			return;
		}

		/*
		if (HaveShell () ) {
			m_nAdjustShit++;
			if (m_nAdjustShit >= Main.s_Instance.m_fShellAdjustShit) {
				nDirect = 1;
				m_nAdjustShit = 0;
			}
		}
		*/
		vecTempPos = GetPos ();

		if ((m_vecLastDirection.x > 0 && m_vecdirection.x < 0) || (m_vecLastDirection.x < 0 && m_vecdirection.x > 0)) {
			nDirect = 1;
		}

		if ( nDirect == 0 ) {
			fDeltaX = fPosX - vecTempPos.x;
			fDeltaY = fPosY - vecTempPos.y;
			SetExtraSpeedX (fDeltaX);
			SetExtraSpeedY (fDeltaY);
		} else {
			vecTempPos.x = fPosX;
			vecTempPos.y = fPosY;
			SetPos ( vecTempPos );
		}
		m_vecLastDirection = m_vecdirection;
	}

	[PunRPC]
	public void RPC_SyncBaseInfo( float fPosX, float fPosY, bool bDirect )
	{
		return;

		if (DoNotMove ()) {
			return;
		}


        vecTempPos = GetPos ();
		float fDeltaX = 0f;
		float fDeltaY = 0f;
		if (!_Player.IsMoving() || bDirect )
        {
			vecTempPos.x = fPosX;
			vecTempPos.y = fPosY;
			Local_SetPos(vecTempPos);
        }
        else
		{	
			fDeltaX = fPosX - vecTempPos.x;
			fDeltaY = fPosY - vecTempPos.y;

			if (false
			//	(m_vecdirection.x > 0 && fPosX > vecTempPos.x) ||
			//	(m_vecdirection.x < 0 && fPosX < vecTempPos.x) ||
			//	(m_vecLastDirection.x > 0 && m_vecdirection.x < 0) ||
			//	(m_vecLastDirection.x < 0 && m_vecdirection.x > 0)) 
			)
			{
				vecTempPos.x = fPosX;
				Local_SetPos (vecTempPos); 
				SetExtraSpeedX ( 0f );
			} else {
				SetExtraSpeedX ( fDeltaX );
			}

			if ( false
			//	(m_vecdirection.y > 0 && fPosY > vecTempPos.y) ||
			//	(m_vecdirection.y < 0 && fPosY < vecTempPos.y) ||
			//	(m_vecLastDirection.y > 0 && m_vecdirection.y < 0) ||
			//	(m_vecLastDirection.y < 0 && m_vecdirection.y > 0) 
			)
			 {
				vecTempPos.y = fPosY;
				Local_SetPos (vecTempPos); 
				SetExtraSpeedY ( 0f );
			} else {
				SetExtraSpeedY ( fDeltaY );
			}

			m_vecLastDirection = m_vecdirection;
        }
	}

	float m_fExtraSpeedX = 0f;
	float m_fExtraSpeedY = 0f;

	void SetExtraSpeedX( float fDeltaX )
	{
		m_fAdjustSpeedX = fDeltaX;
	}

	void ExtraMoveX()
	{
		if (m_fExtraSpeedX == 0f) {
			return;
		}


	}

	void SetExtraSpeedY( float fDeltaY )
	{
		m_fAdjustSpeedY = fDeltaY;
	}


	void UpdateSpitDir ()
	{
		if (!Main.s_Instance.IsForceSpitting ()) {
			return;
		}

		if ( Main.s_Instance.m_nPlatform != 1 ) // 这种“多点触控分球”只针对手机版，不针对PC版
		{
			return;
		}

		//MapEditor.s_Instance._txtDebugInfo.text = Main.s_Instance.GetSpitTouchTargetPos ().ToString ();
		//_vecSpitDir = Main.s_Instance.GetSpitTouchTargetPos () - this.GetPos ();
		//_vecSpitDir.Normalize ();
	}

	public void CanceSplit()
	{
	}

	public void CancelSpit()
	{
		/*
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		if (_relateSplitTarget) {
			DestroyTarget ( _relateSplitTarget );
		}

		if (m_SpitBallTarget) {
			DestroyTarget ( m_SpitBallTarget );
		}

		if (m_SplitBallTarget) {
			DestroyTarget ( m_SplitBallTarget );
		}
		*/
		if (m_SpitBallTarget) {
			m_SpitBallTarget.SetVisible (false);
		}
	}

	// 参数nPlatform是指吐球方的操作平台，显示方是什么平台无所谓。
	// m_vSpecialDirecton是双摇杆模式中用来控制分球方向的；
	// m_vecdirection是用来控制行走方向的，无论是不是双摇杆模式
	public void Spit( float fDelayTime, int nPlatform, float fChildSize, float fMotherLeftSize )
	{
		if (IsEjecting ()) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		float fRadiusMother = GetBoundsSize()  / 2.0f; // 尼玛你倒是在母球分出体积之前计取母球的半径啊

		float fMotherSize = GetSize ();

		bool bNew = false;
		Ball new_ball = _Player.ReuseOneBall ();
		if (new_ball == null) {
			return;
		}

		//Ball new_ball = goNewBall.GetComponent<Ball> ();
		
		//new_ball.transform.parent = _Player.transform;
		//CalculateNewBallBornPosAndRunDire ( new_ball, _dirx, _diry );
		float fRealRunTime =  Main.s_Instance.m_fSpitBallRunTime - fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}
		float fSpitBallInitSpeed = 0.0f;
		float fSpitBallAccelerate = 0.0f;
		float fRunDistance = ( Main.s_Instance.m_fSpitBallRunDistanceMultiple ) * fRadiusMother;
		//fSpitBallInitSpeed = CyberTreeMath.GetV0 (fRunDistance, fRealRunTime  );
		//fSpitBallAccelerate = CyberTreeMath.GetA ( fRunDistance, fRealRunTime);
		vecTempPos = GetPos ();

		float dirx = 0;
		float diry = 0;

		if (!IsMine()) { // Others  
			if (nPlatform == 0) { // PC
				dirx = m_vecdirection.x;
				diry = m_vecdirection.y;
			}
			else if (nPlatform == 1) // Mobile Phone
			{
				dirx = _Player.m_vSpecialDirecton.x;
				diry = _Player.m_vSpecialDirecton.y;

				if (dirx == 0 && diry == 0f) { // （这种情况仅出现在双摇杆模式）如果摇杆的方向为0，则把分球方向定为行走方向
					dirx = m_vecLastDirection.x;
					diry = m_vecLastDirection.y;
				}
			}
		}
		else // MainPlayer
		{
			if (Main.s_Instance.m_nPlatform == 0) { // PC
				dirx = _dirx;
				diry = _diry;
			}
			else if (Main.s_Instance.m_nPlatform == 1) {// Mobile Phone 双摇杆
				dirx = Main.s_Instance.GetSpitDirection ().x;
				diry = Main.s_Instance.GetSpitDirection ().y;

				if (dirx == 0 && diry == 0f) { // （这种情况仅出现在双摇杆模式）如果摇杆的方向为0，则把分球方向定为行走方向
					dirx = _dirx;
					diry = _diry;
				}
			} 
		}




		RongCuo_NotPointToMyself ( ref dirx, ref diry ); // 如果都还为零，则随便指定一个方向来容错，反正不能往自己身上吐

		new_ball.Local_SetShellInfo( Main.s_Instance.m_fShellShrinkTotalTime, 0f, eShellType.w_spilt_ball );
		new_ball.Local_SetPos ( vecTempPos );
		new_ball.Local_SetSize ( fChildSize );
		new_ball.Local_SetDir ( dirx, diry );
		//new_ball.Local_SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
		float fSpeed = fRunDistance / fRealRunTime;
		//new_ball.Local_BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, fRealRunTime);
		new_ball.Local_BeginEject_New ( fSpeed, fRunDistance );

		Local_SetShellInfo ( Main.s_Instance.m_fShellShrinkTotalTime, 0f, eShellType.w_spilt_ball );
		Local_BeginShell ();
		Local_SetSize ( fMotherLeftSize );
		/*
		if (bNew) {
			//new_ball.SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
			new_ball.SetShellInfo( Main.s_Instance.m_fShellShrinkTotalTime, 0f );
			new_ball.BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitBallRunTime);
			new_ball.SetPos ( vecTempPos );
			new_ball.SetSize ( fChildSize );
			new_ball.SetDir( dirx, diry);
		} else {
			new_ball.Local_SetShellInfo( Main.s_Instance.m_fShellShrinkTotalTime, 0f );
			new_ball.Local_SetPos ( vecTempPos );
			new_ball.Local_SetSize ( fChildSize );
			new_ball.Local_SetDir ( dirx, diry );
			//new_ball.Local_SetThornSize ( GetThornSize() * fChildSize / fMotherSize );
			new_ball.Local_BeginEject (fSpitBallInitSpeed, fSpitBallAccelerate, Main.s_Instance.m_fSpitBallRunTime);
		}
		*/
	}

	float m_fMoveSpeedX = 0.0f;
	float m_fMoveSpeedY = 0.0f;
	Vector2 m_vecdirection = new Vector2();
	public Vector2 m_vecLastDirection = new Vector2();
	public void BeginMove( Vector3 vecWorldCursorPos, float fRealTimeSpeedWithoutSize  )
	{
		if (IsInGroup ()) {
			BeginMove_InGroup ( vecWorldCursorPos, fRealTimeSpeedWithoutSize );
			return;
		}

		m_vecdirection = vecWorldCursorPos - this.transform.position;
		m_vecdirection.Normalize();

        if (_Player.IsMoving ()) {
			m_vecLastDirection = m_vecdirection;
		} else {
	
		}

		float fShitSize = GetSizeKaiGenHao();
		float fSpeed = fRealTimeSpeedWithoutSize / fShitSize;
		m_fMoveSpeedX = fSpeed * m_vecdirection.x + m_fAdjustSpeedX;
		m_fMoveSpeedY = fSpeed * m_vecdirection.y + m_fAdjustSpeedY;
	}

	public void BeginMove_InGroup( Vector3 vecWorldCursorPos, float fRealTimeSpeedWithoutSize  )
	{
		m_vecdirection = vecWorldCursorPos - this.transform.position;
		m_vecdirection.Normalize();

        if (_Player.IsMoving ()) {
			m_vecLastDirection = m_vecdirection;
		} else {

		}

		float fShitSize = GetSizeKaiGenHao();
		float fSpeed = fRealTimeSpeedWithoutSize / fShitSize;
		m_fMoveSpeedX = fSpeed * m_vecdirection.x;
		m_fMoveSpeedY = fSpeed * m_vecdirection.y;


		if (m_Group) {
			m_fMoveSpeedX -= m_Group.GetSpeed ().x;
			m_fMoveSpeedY -= m_Group.GetSpeed ().y;
		}

		// poppin test
		m_fMoveSpeedX = CGroup.AdjustInGroupLocalSpeed( m_fMoveSpeedX );
		m_fMoveSpeedY = CGroup.AdjustInGroupLocalSpeed( m_fMoveSpeedY );
	}

	float m_fShitSpeed = 0f;
	static Vector2 s_pos1 = new Vector2();
	static Vector2 s_pos2 = new Vector2();
	void Move() // none-mainplayer
	{
		if (_Player == null) {
			return;
		}

		if (_Player.photonView.isMine) {
			return;
		}

		if (!_Player.IsMoving()) {
			return;
		}

		if (IsDead ()) {
			return;
		}

		if (DoNotMove ()) {  
			return;
		}

		if (IsInGroup ()) {
			MoveInGroup ();
			return;
		}

		vecTempPos = GetPos ();

		float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
		float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;
		float fExpectedX = vecTempPos.x + fDeltaX;
		float fExpectedY = vecTempPos.y + fDeltaY;


		bool bCanMoveX = true;
		bool bCanMoveY = true;


		float fRealNextX = 0f;
		float fRealNextY = 0f;
		MapEditor.s_Instance.ClampMoveToWorldBorder ( GetRadius(), vecTempPos.x, vecTempPos.y, fDeltaX, fDeltaY, ref fRealNextX, ref fRealNextY);
			
		vecTempPos.x = fRealNextX;
		vecTempPos.y = fRealNextY;

		vecTempPos.z = -GetSize ();
		SetPos (vecTempPos);
	}

	void MoveInGroup() // none-mainplayer
	{
		float fDeltaX = m_fMoveSpeedX * Time.fixedDeltaTime;
		float fDeltaY = m_fMoveSpeedY * Time.fixedDeltaTime;

		//// 检测是否超越了场景边界
		bool bCanMoveX = true;
		bool bCanMoveY = true;
		///  end 检测是否超越了场景边界

		vecTempPos = GetLocalPosition ();
		if (bCanMoveX) {
			vecTempPos.x += fDeltaX;
		}
		if (bCanMoveY) {
			vecTempPos.y += fDeltaY;
		}
		SetLocalPosition ( vecTempPos );
	}

	/*
	public void SyncFullInfo()
	{
		vecTempPos = GetPos ();
		//photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.Others, m_bDead, vecTempPos.x, vecTempPos.y, GetSize(), m_fShellShrinkCurTime, m_fShellTotalTime, GetThornSize() );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( bool bDead, float fPosX, float fPosY, float fSize, float fShellCurTime, float m_fShellTotalTime, float fThornSize )
	{
		float fPosZ = -fSize;
		Local_SetPos ( fPosX,  fPosY, fPosZ);   
		Local_SetSize ( fSize );
		Local_SetThornSize ( fThornSize );
	
		if (fShellCurTime > 0f) {
			Local_SetShellInfo(m_fShellTotalTime, fShellCurTime);
			Local_BeginShell (); 
		}
	
		//Debug.Log ( "全量同步：" + photonView.viewID + " .... " + bDead );
	
		if (bDead) {
			SetDead (true);
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Add ( this );
		}
		else
		{
			SetDead (false);
			bool bNew = false;
			_Player.m_lstBalls.Remove ( this );
			_Player.m_lstRecycledBalls.Remove ( this );
			_Player.m_lstBalls.Add ( this );
		}
	}
	*/
	int m_nRealSplitNum = 0;
	float m_fSplitSize = 0f;
	float m_fSplitArea = 0f;
	float m_fLeftArea = 0f;
	float m_fTotalDis = 0f;
	float m_fSegDis = 0f;
	bool m_bMotherDeadAfterSplit = false;

	public void CalculateSplitChildNumAndSize()
	{
		m_nRealSplitNum = 0; 
		float fCurSize = GetSize ();
		float fCurArea = CyberTreeMath.SizeToArea (fCurSize, Main.s_Instance.m_nShit);
		m_fLeftArea = fCurArea;
		for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--) {
			float fSmallArea = fCurArea / i;
			float fSmallSize = CyberTreeMath.AreaToSize ( fSmallArea, Main.s_Instance.m_nShit );
			if (fSmallSize >= Main.BALL_MIN_SIZE) {
				m_nRealSplitNum = i; 
				m_fSplitArea = fSmallArea;
				m_fSplitSize = fSmallSize;
				break;
			}
		} // end for i
	}

	public void CalculateRealSplitInfo( float fTotalDistance, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis )
	{
		float m_fTotalDis = fTotalDistance; 
		if (m_fTotalDis > Main.s_Instance.m_fSplitMaxDistance) {
			m_fTotalDis = Main.s_Instance.m_fSplitMaxDistance;
		}
		m_fSegDis = m_fTotalDis / Main.s_Instance.m_nSplitNum; // 废弃
		fSegDis =  m_fTotalDis / Main.s_Instance.m_nSplitNum; 


		m_nRealSplitNum = 0; // 废弃
		nRealSplitNum = 0;
		float fCurSize = GetSize ();
		float fCurArea = CyberTreeMath.SizeToArea (fCurSize, Main.s_Instance.m_nShit);
		m_fLeftArea = fCurArea;
		for (int i = Main.s_Instance.m_nSplitNum; i >= 2; i--) {
			float fSmallArea = fCurArea / i;
			float fSmallSize = CyberTreeMath.AreaToSize ( fSmallArea, Main.s_Instance.m_nShit );
			if (fSmallSize >= Main.BALL_MIN_SIZE) {
				m_nRealSplitNum = i; // 废弃
				nRealSplitNum = i;
				fRealSplitChildSize = fSmallSize;
				m_fSplitArea = fSmallArea;
				m_fSplitSize = fSmallSize;
				break;
			}
		} // end for i


	}

	public void CalculateRealSplitInfo( float fWorldCurPosX,  float fWorldCurPosY, ref int nRealSplitNum, ref float fRealSplitChildSize, ref float fSegDis )
	{
		vecTempPos.x = fWorldCurPosX;
		vecTempPos.y = fWorldCurPosY;
		float fTotalDistance = Vector2.Distance ( GetPos(), vecTempPos );
		CalculateRealSplitInfo( fTotalDistance, ref nRealSplitNum, ref fRealSplitChildSize, ref fSegDis );
	}

	public void SplitOne( int nTimes )
	{
		/*
		if (m_nRealSplitNum <= 0) { // 废弃
			return;
		}
		bool bNew = false;
		Ball new_ball = _Player.ReuseOneBall ( photonView.isMine, ref bNew ); 
		if (new_ball == null) {
			return;
		}
		Debug.Log ( "spit_" + nTimes );
		if (!photonView.isMine) {
			_dirx = m_vecdirection.x;
			_diry = m_vecdirection.y;
		}

		float fDis = m_fSegDis * ( nTimes + 1 );
		float fInitSpeed = CyberTreeMath.GetV0 (fDis, Main.s_Instance.m_fSplitRunTime  );
		float fAccelerate = CyberTreeMath.GetA ( fDis, Main.s_Instance.m_fSplitRunTime);

		if (bNew) {
			new_ball.SetPos (GetPos ());
			new_ball.SetSize (m_fSplitSize);
			new_ball.SetDir (_dirx, _diry);
			new_ball.BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		} else {
			new_ball.Local_SetPos (GetPos ());
			new_ball.Local_SetSize (m_fSplitSize);
			new_ball.Local_SetDir (_dirx, _diry);
			new_ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fSplitRunTime, true, true);
		}

		m_fLeftArea -= m_fSplitArea;
		*/
	}

	public void CheckIfDeadAfterSplit()
	{
//		if (!photonView.isMine) {
//			return;
//		}

		if (m_fLeftArea < Main.BALL_MIN_SIZE) {
			DestroyBall();
			return;
		}

		float fLeftSize = CyberTreeMath.AreaToSize (m_fLeftArea, Main.s_Instance.m_nShit);
		SetSize ( fLeftSize );
	}

	bool m_bCaptured = false;
	public void SetBeCaptured( bool val, float speed = 0f, float dirx = 0f, float diry = 0f )
	{
		//photonView.RPC ( "RPC_SetBeCaptured", PhotonTargets.All, val, _fCapturedSpeed, _fCapturedDirX, _fCapturedDirY );
	}

	[PunRPC]
	public void RPC_SetBeCaptured( bool val, float speed = 0f, float dirx = 0f, float diry = 0f )
	{
		m_bCaptured = val;

		if (m_bCaptured) {
			if (IsEjecting ()) {
				EndEject ();
			}
		}

		_fCapturedSpeed = speed;
		_fCapturedDirX = dirx;
		_fCapturedDirY = diry;
	}

	public bool IsCaptured()
	{
		return m_bCaptured;
	}

	/// 回收利用相关
	int m_nIndex = 0;
	public void SetIndex( int nIndex )
	{
		m_nIndex = nIndex;
	}

	public int GetIndex()
	{
		return m_nIndex;
	}

	int m_nMovingToCenterStatus = 0; // 0 - 还没开始移动  1 - 开始移动  2 - 移动完成
	public int GetMovingToCenterStatus()
	{
		return m_nMovingToCenterStatus;
	}

	public bool IsMovingToCenter()
	{
		return m_nMovingToCenterStatus == 1;
	}

	public void SetBallsCenter( Vector3 vecBallsCenter )
	{
		_direction = vecBallsCenter - GetPos ();
		_direction.Normalize ();
	}

	public void  SetMovingToCenterStatus( int nMovingToCenterStatus )
	{
		m_nMovingToCenterStatus = nMovingToCenterStatus;
	}

	public void DoMovingToCenter()
	{
		if (m_nMovingToCenterStatus != 1) {
			return;
		}

		vecTempPos = GetPos ();
		vecTempPos.x += _speed.x * Time.deltaTime;
		vecTempPos.y += _speed.y * Time.deltaTime;
		this._rigid.MovePosition ( vecTempPos );
		_rigid.MoveRotation (0);
		vecTempPos = GetPos ();
		vecTempPos.z = -GetSize();
		Local_SetPos ( vecTempPos );
		//Local_SetPos ( vecTempPos );
	}

	public void CalculateMoveToCenterRealSpeed()
	{
		float fSpeed =  Main.s_Instance.m_fBallMoveToCenterBaseSpeed;
		if (Main.s_Instance.m_nShit == 2) {
			fSpeed = fSpeed / GetSize2KaiGenHao ();
		} else if (Main.s_Instance.m_nShit == 3) {
			fSpeed = fSpeed / GetSize3KaiGenHao ();
		}
		_speed.x = fSpeed * _direction.x + m_fAdjustSpeedX;
		_speed.y = fSpeed * _direction.y + m_fAdjustSpeedY;
	}
		
	public bool IfBallPosInvalid()
	{
		if (GetPos().x < -5000f || GetPos().y < -5000f) {
			return true;
		}

		return false;
	}

	public Dictionary<int, Ball> m_dicCollideList = new Dictionary<int, Ball> ();
	public void AddToCollideDic( Ball ball )
	{
		m_dicCollideList [ball.GetIndex()] = ball;
	}

	public void RemoveFromCollideDic( Ball ball  )
	{
		m_dicCollideList.Remove ( ball.GetIndex() );

		if (m_dicCollideList.Count == 0) {
			CGroup group = GetGroup ();
			if (group) {
				group.RemoveOneBall ( this );
			}
			SetGroup ( null );
		}
	}

	public bool AlreadyEnter( Ball ball )
	{
		Ball the_ball = null;
		return m_dicCollideList.TryGetValue (ball.GetIndex(), out the_ball );
	}

	public int GetCollideBallsNum()
	{
		return m_dicCollideList.Count;
	}

	public Dictionary<int, Ball> GetCollideBallsDic()
	{
		return m_dicCollideList;
	}

	CGroup m_Group = null;
	public bool IsInGroup()
	{
		return ( m_Group != null );
	}

	public void SetGroup( CGroup group )
	{
		m_Group = group;
		if (m_Group) {
			this.transform.parent = m_Group.transform;
		} else {
			this.transform.parent = _Player.transform;
		}
	}

	public CGroup GetGroup()
	{
		return m_Group;
	}

	bool m_bIsAdjustMoveProtect = false;
	float m_fAdjustMoveProtectTime = 0f;
	public void BeginAdjustMoveProtectTime()
	{
		m_fAdjustMoveProtectTime = 0f;
		m_bIsAdjustMoveProtect = true;
	}

	public void EndAdjustMoveProtectTime()
	{
		m_fAdjustMoveProtectTime = 0f;
		m_bIsAdjustMoveProtect = false;
	}

	public bool IsAdjustMoveProtect()
	{
		return m_bIsAdjustMoveProtect;
	}

	void AdjustMoveProtectLoop()
	{
		if (!m_bIsAdjustMoveProtect) {
			return;
		}

		m_fAdjustMoveProtectTime += Time.deltaTime;
		if (m_fAdjustMoveProtectTime >= Main.s_Instance.m_fAdjustMoveProtectInterval) {
			EndAdjustMoveProtectTime ();
		}
	}

    Dictionary<int, int> m_dicGroupRoundId = new Dictionary<int, int>();
    public void GroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
            nCount++;
            m_dicGroupRoundId[nGroupId] = nCount;
        }
        else
        {
            m_dicGroupRoundId[nGroupId] = 0;
        }
    }

    public int GetGroupCount(int nGroupId)
    {
        int nCount = 0;
        if (m_dicGroupRoundId.TryGetValue(nGroupId, out nCount))
        {
        }
        return nCount;
    }
}