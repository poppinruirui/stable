﻿using System;
using System.Collections;
 using UnityEngine.UI;
 
using UnityEngine;
using UnityEngine.SceneManagement; 
 
 
namespace Com.KeMengWorld.ElectricMusicBall
{
    public class GameManager : Photon.PunBehaviour
	{
		public Camera m_MainCam;
		public GameObject m_goBalls;
		public Text m_textDebugInfo;

		void Start()
		{
			InstantiatePlayer ();
		}

		void Update()
		{
			if ( m_goBalls == null )
			{
				return;
			}
			
			string szInfo = "";

		}

        #region Photon Messages
 
 
        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }
 
		public override void OnPhotonPlayerConnected( PhotonPlayer other  )
		{
			Debug.Log( "OnPhotonPlayerConnected() " + other.NickName ); // not seen if you're the player connecting
		    

			if ( PhotonNetwork.isMasterClient ) 
			{
				Debug.Log( "OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient ); // called before OnPhotonPlayerDisconnected
		 
		 
//				LoadArena();
			}
			else
			{
	
			}
		}

		public void AddBallToBallList( GameObject goBall )
		{
			goBall.transform.parent =  m_goBalls.transform;
		}

		void InstantiatePlayer( )
		{
			if (playerPrefab == null) 
			{
				Debug.LogError( "我勒个擦，player的prefab都是空的" );
			}
			else
			{
				//Debug.Log("We are Instantiating LocalPlayer from "+Application.loadedLevelName);
				// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
				if (PlayerManager.LocalPlayerInstance==null)
				{
					//Debug.Log("We are Instantiating LocalPlayer from "+Application.loadedLevelName);
					// we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
					GameObject goPlayer =  PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f,0f,0f), Quaternion.identity, 0);
					//AddBallToBallList( goPlayer );
				}else
				{
					Debug.Log("Ignoring scene load for "+Application.loadedLevelName);
				}
			}
				
		}
		 
		 
		public override void OnPhotonPlayerDisconnected( PhotonPlayer other  )
		{
			Debug.Log( "OnPhotonPlayerDisconnected() " + other.NickName ); // seen when other disconnects
		 
		 
			if ( PhotonNetwork.isMasterClient ) 
			{
				Debug.Log( "OnPhotonPlayerConnected isMasterClient " + PhotonNetwork.isMasterClient ); // called before OnPhotonPlayerDisconnected
		 
		 
			//	LoadArena();
			}
		}
 
        #endregion
 
		#region public variables
		[Tooltip("The prefab to use for representing the player")]
		public GameObject playerPrefab;

		#endregion

        #region Public Methods
 
 
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }
 
 
        #endregion  
		
		// Master Client就是开启该房间的那个客户端
		void LoadArena()
		{
			if ( ! PhotonNetwork.isMasterClient )  // 设计意图：只有Master Client有资格Load Level（开启新的场景）
			{
				Debug.LogError( "PhotonNetwork : Trying to Load a level but we are not the master Client" );
				return; // 官网上的代码少了这句
			}
			//Debug.Log( "PhotonNetwork : Loading Level : " + PhotonNetwork.room.PlayerCount );
			PhotonNetwork.LoadLevel("Room for 1"/*PhotonNetwork.room.playerCount*/);
		}
 		
		
		public void ShowDebugInfo( string szInfo )
		{
			m_textDebugInfo.text = szInfo;
		}
		
    }
}