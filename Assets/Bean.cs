﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bean :  Ball {

	static List<float> m_lstNewBeanApplication = new List<float> ();

	public SpriteRenderer _srMain;
    float m_fLiveTime = 0.0f;

	public enum eBeanType
	{
		scene,		          // 场景上的最普通的豆子
		spray,                // 喷泉喷出的豆子
		spore,                // 玩家喷出来的孢子
	};


	public eBeanType _beanType = eBeanType.scene;
		

	// Use this for initialization
	void Start ()
	{
		
	}

	void Awake()
	{
		_balltype = eBallType.ball_type_bean;

		Init ();
	}

	public override void  Init ()
	{
        int nSprayID = 0;
   
		//_srMain.color = ColorPalette.RandomFoodColor ();


		if (_beanType == eBeanType.scene) {
			this.transform.transform.parent = Main.s_Instance.m_goBeans.transform;
		} else if (_beanType == eBeanType.spray ) {
           
        }

      //  m_fLiveTime = Main.s_Instance.m_fSprayBeanLiveTime;


    }

    public void SetLiveTime( float fLiveTime )
    {
        m_fLiveTime = fLiveTime;
    }
	
	// Update is called once per frame
	public override void Update () {
		Spraying ();
        LiveTimeCounting();
		Ejecting();
    }

	public override void SetDead( bool val )
	{
		m_bDead = val;
		this.gameObject.SetActive( !m_bDead );
		if (m_bDead ) { // 死了一个球


		} else {
		}
	}

	public override void FixedUpdate()
	{

	}

	public static void ApplyGenerateOneNewBean ()
	{
		m_lstNewBeanApplication.Add ( (float)PhotonNetwork.time/*Main.s_Instance.m_fGenerateNewBeanTimeInterval*/ );
	}

	static int m_nSceneBeanRebornIndex = 0;
	public static void GenerateNewBeanLoop()
	{
		//Debug.Log ( Main.s_Instance.m_goBeans.transform.childCount + "," + m_lstNewBeanApplication.Count );
		if (m_lstNewBeanApplication.Count == 0) {
			return;
		}

		if (m_nSceneBeanRebornIndex >= m_lstNewBeanApplication.Count) {
			m_nSceneBeanRebornIndex = 0;
		}
		if (m_nSceneBeanRebornIndex >= m_lstNewBeanApplication.Count) {
			return;
		}
		if (PhotonNetwork.time - m_lstNewBeanApplication [m_nSceneBeanRebornIndex] >= Main.s_Instance.m_fGenerateNewBeanTimeInterval) {
			m_lstNewBeanApplication.RemoveAt ( m_nSceneBeanRebornIndex );
			Main.s_Instance.GenerateBean ();
		} else {
			m_nSceneBeanRebornIndex++;
		}
	}

	float _speedX = 0.0f;
	float _speedY = 0.0f;
	bool m_bSpraying = false;
	public void BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
	//	photonView.RPC ( "RPC_BeginSpray", PhotonTargets.AllBuffered, fInitSpeed, fAccelerate, fDirection );
	}

	[PunRPC]
	public void RPC_BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
		Local_BeginSpray (fInitSpeed, fAccelerate, fDirection);
	}

	float m_fInitSpeed = 0.0f;
	float m_fAccelerate = 0.0f;
    float m_fDirection = 1.0f;
	public void Local_BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
		m_fInitSpeed = fInitSpeed;
		m_fAccelerate = fAccelerate;
        m_fDirection = fDirection;
        m_bSpraying = true;
	}

	public void Spraying()
	{
		if (!m_bSpraying) {
			return;
		}
        //Debug.Log(m_fInitSpeed);
		vecTempPos = this.transform.localPosition;
        _speedX = Mathf.Sin(m_fDirection) * m_fInitSpeed;
        _speedY = Mathf.Cos(m_fDirection) * m_fInitSpeed;
        vecTempPos.x += _speedX * Time.fixedDeltaTime;
		vecTempPos.y += _speedY * Time.fixedDeltaTime;
		this.transform.localPosition = vecTempPos;
		m_fInitSpeed += m_fAccelerate * Time.fixedDeltaTime;
		if (m_fInitSpeed <= 0.0f) {
			EndSpray ();
		}
	}

	public void EndSpray()
	{
		m_bSpraying = false;
	}
    void LiveTimeCounting()
    {
        if (_beanType != eBeanType.spray)
        {
            return;
        }

        m_fLiveTime -= Time.fixedDeltaTime;
        if ( m_fLiveTime <= 0.0f )
        {
            Ball.DestroyBean( this );
        }
    }
    
}
