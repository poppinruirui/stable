﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public static  void UpdateDropdownView( Dropdown dropdownItem,  List<string> showNames)
	{
		if (dropdownItem == null) {
			return;
		}

		dropdownItem.options.Clear();
		Dropdown.OptionData tempData;
		for (int i = 0; i < showNames.Count; i++)
		{
			tempData = new Dropdown.OptionData();
			tempData.text = showNames[i];
			dropdownItem.options.Add(tempData);
		}
		if (showNames.Count > 0) {
			dropdownItem.captionText.text = showNames [0];
		}
	}
}
