﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCollider_New : MonoBehaviour {

	public BallTrigger_New.eTriggerType _type;
	public Ball _ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	// 接触
	void OnCollisionEnter2D(Collision2D other)
	{
		if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
		{
			return;
		}
		if (this._type != BallTrigger_New.eTriggerType.ball ) {
			return;
		}

		if (other.transform.gameObject.tag == "ball_root") {
			Ball ballOpponent = other.transform.gameObject.GetComponent<Ball>();
			if (ballOpponent) {
				if (this._ball._Player.photonView.ownerId != ballOpponent._Player.photonView.ownerId) {
					Physics2D.IgnoreCollision (this._ball._Collider, ballOpponent._Collider);
				} 
				else {
					if (true/*this._ball._Player.photonView.isMine*/) {// 只在MainPlayer主动作这项操作，其余客户端被动同步
						this._ball._Player.ProcessCollideEnterGroup (this._ball, ballOpponent);
					}
				}

			}

		}
	}
		
	// 胶着状态(继续PK)
	void OnCollisionStay2D(Collision2D other)
	{
		
	}


	
	// 分离	
	// 以下两种情况应该会自动激发 OnCollisionExit2D操作，待测：
	// 1、球体死亡应该（即gameObject.SetActive( false )）
	// 2、球体壳消失（即_Collider的enable变为false）

	void OnCollisionExit2D(Collision2D other)
	{
		if (true/*this._ball._Player.photonView.isMine*/) { // 只在MainPlayer主动作这项操作，其余客户端被动同步
			if (other.transform.gameObject.tag == "ball_root") {
				Ball ballOpponent = other.transform.gameObject.GetComponent<Ball> ();
				if (ballOpponent) {
					if (this._ball._Player.photonView.ownerId == ballOpponent._Player.photonView.ownerId) {
						this._ball._Player.ProcessCollideExitGroup (this._ball, ballOpponent);
					}
				}
			}		
		}
	}
}
