﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeMath : MonoBehaviour {

    public static Vector3 s_vecTempPos = new Vector3();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 计算位移
	public static float CalculateDistance( float v0, float a, float t )
	{
		return ( v0 * t + 0.5f * a * t * t );
	}

    // 已知位移和时间求初速度（末速度为零）
    public static float GetV0(float s, float t)
    {
        return (2.0f * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static float GetA(float s, float t)
    {
        return -2.0f * s / (t * t);
    }

    // 计算新吐出的球初始位置和冲刺方向
    public static void CalculateSpittedObjBornPos( float fMotherPosX, float fMotherPosY, float fMotherRadius, float fMotherDirX, float fMotherDirY, float fChildRadius, ref float fChildPosX, ref float fChildPosY )
	{
		float fDis = fMotherRadius +  fChildRadius;
		fChildPosX = fMotherPosX + fDis * fMotherDirX;
		fChildPosY = fMotherPosY + fDis * fMotherDirY;
	}

    public static bool ClampBallPosWithinSCreen( Vector3 srcPos, ref Vector3 destPos )
    {
        bool bRet = false;
		destPos = srcPos;
		return false;

        s_vecTempPos = Camera.main.WorldToScreenPoint(srcPos);
        if (s_vecTempPos.x > Screen.width)
        {
            s_vecTempPos.x = Screen.width;
            bRet = true;
        }
        if (s_vecTempPos.x < 0)
        {
            s_vecTempPos.x = 0;
            bRet = true;
        }
        if (s_vecTempPos.y > Screen.height)
        {
            s_vecTempPos.y = Screen.height;
            bRet = true;
        }
        if (s_vecTempPos.y < 0)
        {
            s_vecTempPos.y = 0;
            bRet = true;
        }
        destPos = Camera.main.ScreenToWorldPoint(s_vecTempPos);
        return bRet;
    }


	public static void CalculateSpittedObjRunParams( float fRunDistance, float fRunTime, ref float fInitSpeed, ref float fAccelerate )
	{
		fInitSpeed = GetV0 (fRunDistance, fRunTime  );
		fAccelerate = GetA ( fRunDistance, fRunTime);
	}

	const float c_shit = 1.0f / 3.0f;
	public static float Kai3CiFang( float src )
	{
		return Mathf.Sqrt( src );//Mathf.Pow ( src , c_shit );
	}

	public static float CalculateNewSize( float fCurSize, float fAddedSize, int nShit, int op = 1 )
	{
		float fNewSize = 0.0f;
		if (nShit == 2) {
			fNewSize = Mathf.Sqrt (fCurSize * fCurSize + ( fAddedSize * fAddedSize ) * op );
		} else if (nShit == 3) {
			fNewSize = Mathf.Pow ( fCurSize * fCurSize * fCurSize + ( fAddedSize * fAddedSize * fAddedSize ) * op , c_shit );
		}

		return fNewSize;
	}

	public static float SizeToArea( float fSize, int nShit )
	{
		float fArea = 0.0f;
		if (nShit == 2) {
			fArea = fSize * fSize;
		} else if( nShit == 3 )  {
			fArea = fSize * fSize * fSize;
		}
		return fArea;
	}

	public static float AreaToSize( float fArea, int nShit )
	{
		float fSize = 0.0f;
		if (nShit == 2) {
			fSize = Mathf.Sqrt( fArea );
		} else if( nShit == 3 )  {
			fSize = Mathf.Pow ( fArea, c_shit );
		}
		return fSize;
	}

    public static string ReOrganizeFilePath( string szFileName )
    {
        string[] ary = szFileName.Split( '/' );
        string ret = "";
        for ( int i = 0; i < ary.Length; i++ )
        {
            ret += ary[i];
            ret += '\\';
        }
        return ret;
    }

	public static bool CheckIfPartiallyCover_CircleCollider2D( CircleCollider2D c1, CircleCollider2D c2, float fMianJi2 )
	{
		float  d;
		float  s,s1,s2,s3,angle1,angle2;
		float r1 = c1.bounds.size.x / 2f ;
		float r2 = c2.bounds.size.x / 2f ;

		d = Mathf.Sqrt((c1.bounds.center.x-c2.bounds.center.x)*(c1.bounds.center.x-c2.bounds.center.x)+(c1.bounds.center.y-c2.bounds.center.y)*(c1.bounds.center.y-c2.bounds.center.y));
		if (d >= (r1 + r2)) {//两圆相离
			return false;
		}
		if ((r1 - r2) >= d) {//两圆内含,c1大
			return true;
		}

		angle1 = Mathf.Acos((r1*r1+d*d-r2*r2)/(2*r1*d));
		angle2 = Mathf.Acos((r2*r2+d*d-r1*r1)/(2*r2*d));

		s1=angle1*r1*r1;
		s2=angle2*r2*r2;
		s3=r1*d*Mathf.Sin(angle1);
		s=s1+s2-s3;

		float fCurPercent = s / fMianJi2;
		if (fCurPercent >= MapEditor.s_Instance.m_fYaQiuBaiFenBi ) {
			return true;
		}

		return false;
	}

	public static bool CheckIfArriveDest( Vector2 pos, Vector2 dest, float fDis )
	{
		float fShit = Vector2.Distance (pos, dest);
		if ( fShit <= fDis) {
			return true;
		}
		return false;
	}

	public static float FloatTrim( float raw, int n = 2 )
	{
		return (((int)( raw * 100)) / 100 );
	}
}
