﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_Split : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public static bool s_bUsingUi = false;

	bool m_bSplitting = false;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void OnPointerDown(PointerEventData evt)
	{
		if (this.gameObject.name == "btnOneBtnSplit") { // “力度分球”开始蓄力
			/*
			if (Main.s_Instance.m_nPlatform == 1) { // 只针对手机版
				if (Input.touchCount > 0) {
					Touch touch = Input.GetTouch (Input.touchCount - 1);
					Main.s_Instance.BeginPreOneBtnSplit (touch.fingerId, touch.position);				
				}
			} else {

			}
			*/
			// 由于双摇杆分球的操作过于复杂，现在取消双摇杆分球模式，只保留“力度分”模式。
			Main.s_Instance.BeginRSplit();
		}
    }

    public void OnPointerUp(PointerEventData evt)
    {
		if (this.gameObject.name == "btnOneBtnSplit") {	

	//		if (Main.s_Instance.m_nPlatform == 1) {
				Main.s_Instance.EndPreOneBtnSplit ();	
		//	}

		}



    }

	
}
