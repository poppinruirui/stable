using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;

public class Player : Photon.PunBehaviour , IPunObservable 
{
	public bool _isRebornProtected = true;

	public Color _color_inner;
	public Color _color_ring;
	public Color _color_poison;

	float m_fProtectTime= 0.0f;

	public List<Ball> m_lstBalls = new List<Ball>();
	int m_nBallIndex = 0;

	public Vector2 m_vecPos = new Vector2 ();

	static Vector3 vecTempPos = new Vector3 ();
	static Vector2 vec2Temp = new Vector2 ();

	public CGroupManager m_GroupManager;

	public CGroup m_Group;

	// Use this for initialization
	void Start () 
	{
		Init ();
	}

	void Awake()
	{
		
	}

	public void Reset ()
	{
		SetMoving (false);
	}
	
	// Update is called once per frame
	void Update () {
		if ( m_bObserve ) {
			return;
		}

		ProtectTimeCount ();
		//SyncBallsInfo ();
		//Splitting ();
		//Syncing_FullInfo ();
		//PreDrawLoop ();
		SpittingSpore ();
		//Attenuate ();
		//MoveToCenter ();
		//TestExplode ();
		Initing ();
		SyncMoveInfo ();
		AdjustMoveInfo ();  
		ClearSpitTargetCounting ();
		//SyncPosAfterEjectCounting ();
		//ShowDebugInfo ();
	}

	void ShowDebugInfo ()
	{
		if (photonView.isMine) {
			return;
		}

		string szInfo = "";
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				szInfo += "0";
			} else {
				szInfo += "1";
			}
			szInfo += ",";
		}
		szInfo += " ______________   ";
		szInfo += GetCurAvailableBallCount ();
	}

	public int GetCurAvailableBallCount()
	{
		return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurLiveBallsNum ();
	}  

	public void Init()
	{
		_isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

		if (Main.s_Instance == null|| Main.s_Instance.m_goBalls == null) {
			Main.m_lstDelayInit.Add ( photonView );
			return;
		}

		this.gameObject.name = "player_" + photonView.ownerId;
		this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;


		int color_index = 0;
		_color_ring = ColorPalette.RandomOuterRingColor (ref color_index);
		_color_inner = ColorPalette.RandomInnerFillColor ( photonView.ownerId/*color_index*/ );
		_color_poison = ColorPalette.RandomPoisonFillColor ( photonView.ownerId/*color_index*/);

		if (!photonView.isMine) { // 不是本机，是其它客户端的Player
			BeginInit();
		}

		m_nCurSkinIndex = Main.s_Instance.GetSpriteIdByPhotonOwnerId ( photonView.ownerId );

		if (photonView.isMine && AccountManager.s_bObserve) {
			SetObserve ( true );
		}
	}

	// 因为球球数众多，必须分帧处理
	bool m_bIniting = false;
	bool m_bInitCompleted = false;

	public bool IsInitCompleted()
	{
		return m_bInitCompleted;
	}

	void BeginInit()
	{
		m_bIniting = true;
	}

	void EndInit()
	{
		m_bIniting = false;

		// 球体客户端资源初始化完毕，可以来一次全量同步了
		photonView.RPC ( "RPC_FuckMe", PhotonTargets.Others );
	}

	void Initing()
	{
		if (!m_bIniting) {
			return;
		}

		// 配置文件初始化成功之前，不要初始化Player。因为网络读取文件是异步的。
		if (!MapEditor.s_Instance.IsMapInitCompleted ()) {
			return;
		}

		if (m_lstBalls.Count >= Main.s_Instance.m_fMaxBallNumPerPlayer) {
			EndInit ();
			return;
		}

		Ball ball = GameObject.Instantiate ( Main.s_Instance.m_preBall ).GetComponent<Ball>();
		AddOneBall ( ball );
	}

	public void FuckYou()
	{
		
	}

	// 有新玩家进房间，把已经在房间的玩家信息来一次全量同步
	public void FuckMe()
	{
		return;

		m_szPlayerName = MapEditor.GetMainPlayerName ();
		photonView.RPC ( "RPC_FuckMe", PhotonTargets.Others );
	}

	[PunRPC]
	public void RPC_FuckMe()
	{
		if (!photonView.isMine) {
			return;
		}

		Main.s_Instance.m_MainPlayer.SyncFullInfo ();
	}

	byte[] _bytesFullInfo = new byte[1400];

	public void FuckTest()
	{
		double fBefore = Time.time;
		int n = 64;
		float a = 111.222f;
		float b = 333.444f;
		float c = 555.666f;
		int nPointer = 0;
		byte[] bytesNum = BitConverter.GetBytes (n);
		bytesNum.CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		for (int i = 0; i < n; i++) {
			byte[] bytesIndex = BitConverter.GetBytes (i);
			byte[] bytesA = BitConverter.GetBytes (a);
			byte[] bytesB = BitConverter.GetBytes (b);
			byte[] bytesC = BitConverter.GetBytes (b);
			bytesIndex.CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(byte);
			bytesA.CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
			bytesB.CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
			bytesC.CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
		} // end for
		photonView.RPC ( "RPC_FuckTest", PhotonTargets.Others, _bytesFullInfo );
	}

	[PunRPC]
	public void RPC_FuckTest( byte[] bytes )
	{
		int nPointer = 0;
		int nNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof(byte);
		for (int i = 0; i < nNum; i++) {
			int nIndex = (byte)BitConverter.ToChar (bytes, nPointer);
			nPointer += sizeof(byte);
			float a = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			float b = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			float c = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);

		}
	}


	public void PhotonRemoveObject( GameObject go )
	{
		PhotonNetwork.Destroy( go );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{

	}

	public void BeginProtect()
	{
		photonView.RPC ( "RPC_BeginProtect", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginProtect()
	{
		m_fProtectTime = Main.s_Instance.m_fBornProtectTime;
	}

	void ProtectTimeCount()
	{
		if (m_fProtectTime > 0.0f) {
			m_fProtectTime -= Time.deltaTime;
		}
	}

	public bool IsProtect()
	{
		return m_fProtectTime > 0.0f;
	}

	int m_nKingExplodeIndex = 0;
	bool m_bKingExploding = false;
	public void KingExplode ()
	{

	}

	public void SyncMapInfo()
	{
		if (!PhotonNetwork.isMasterClient) {
			return;
		}

		List<Spray> lstSprays = MapEditor.s_Instance.GetSprayList ();
		for (int i = 0; i < lstSprays.Count; i++) {
			Spray spray = lstSprays [i];
			int nDrawStaus = spray.GetDrawStatus ();
			float fParam1 = 0f;
			float fParam2 = 0f;
			float fParam3 = 0f;
			float fParam4 = 0f;
			if (nDrawStaus == 1) {
				fParam1 = spray.GetObsCircleStatus ();
				fParam2 = spray.GetObsCircleTimeCount ();
			} else if (nDrawStaus == 2) {
				fParam1 = spray.GetDrawTimeLeft();
			}
			fParam3 = spray.GetMeatBeanTimeLeft();

			photonView.RPC ( "RPC_SyncSprayInfo", PhotonTargets.Others, spray.GetGUID(), nDrawStaus, fParam1, fParam2, fParam3 );
		}
	}

	[PunRPC]
	public void RPC_SyncSprayInfo( int nSprayGUID, int nDrawStatus, float fParam1, float fParam2, float fParam3 )
	{
		MapEditor.s_Instance.SyncSprayInfo (nSprayGUID, nDrawStatus, fParam1, fParam2, fParam3 );
	}

	string m_szPlayerName = "";
	public string GetPlayerName()
	{
		return m_szPlayerName;
	}

	public void SetPlayerName( string szPlayerName )
	{
		m_szPlayerName = szPlayerName;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetPlayerName ( m_szPlayerName );
		}
	}

	// 凡事要一分为二的来看。编程准则中强调的“尽量不要去遍历”，指的是大数量级的列表，比如几十万个节点的列表。这种几十个节点的小微列表怎么不可以去遍历？
	// 抛开数量级谈优化都是耍流氓。 为了一些根本没必要的优化，倒弄一些Bug出来。
	public int GetCurLiveBallsNum()
	{
		int n = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball == null || ball.IsDead ()) {
				continue;
			}
			n++;
		}
		return n;
	}

	public Ball GetOneLiveBall( ref int nLiveNum )
	{
		Ball ball = null;
		nLiveNum = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			if (!(m_lstBalls [i].IsDead ())) {
				ball = m_lstBalls [i];
				nLiveNum++;
			} 
		}
		return ball;
	}

	public int GetCurLiveBallsNumNotInGroup()
	{
		int n = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if ( ball.IsDead () ) {
				continue;
			}
			if (ball.IsInGroup ()) {
				continue;
			}
			n++;
		}
		return n;
	}

	bool m_bSyncingFullInfo = false;
	int m_SyncFullInfoIndex = 0;
	public void SyncFullInfo()
	{
		int nPointer = 0;
		string szPlayerName = GetPlayerName ();
		byte[] bytesPlayerName = StringManager.String2Bytes ( szPlayerName );
		int nLenOfPlayerName = bytesPlayerName.Length;
		byte[] bytesLenOfPlayerName = BitConverter.GetBytes (nLenOfPlayerName);
		bytesLenOfPlayerName.CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		bytesPlayerName.CopyTo (_bytesFullInfo, nPointer);
		nPointer += nLenOfPlayerName;

		BitConverter.GetBytes (AccountManager.s_bObserve?1:0).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);

		int nNum = GetCurLiveBallsNum ();
		BitConverter.GetBytes (nNum).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (m_nCurSkinIndex).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(ushort);
		BitConverter.GetBytes (IsMoving()?1:0).CopyTo (_bytesFullInfo, nPointer);
		nPointer += sizeof(byte);

		// Group的信息
		int nGroupNum = m_GroupManager.GetLiveGroupNum();
		BitConverter.GetBytes (nGroupNum).CopyTo( _bytesFullInfo, nPointer );
		nPointer += sizeof(byte);

		CGroup[] lstGroup = m_GroupManager.GetGroupList ();
		for (int i = 0; i < lstGroup.Length; i++) {
		//foreach( KeyValuePair<int, CGroup> pair in dicLiveGroup ){
			CGroup group = lstGroup[i];
			if (group == null || group.GetDismissed ()) {
				//Debug.LogError ( "从个有Bug！！group == null || group.GetDismissed () " );
				continue;
			}
			int nGroupId = group.GetID ();
			float fGroupPosX = group.GetPos ().x;
			float fGroupPosY = group.GetPos ().y;
			BitConverter.GetBytes (nGroupId).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(byte);
			BitConverter.GetBytes (fGroupPosX).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
			BitConverter.GetBytes (fGroupPosY).CopyTo (_bytesFullInfo, nPointer);
			nPointer += sizeof(float);
		}
		// end Group的信息

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetIndex () != i) {
				Debug.LogError ( "从个的？？ball.GetIndex () != i" );
				continue;
			}

			byte byGroupID = CGroup.INVALID_GROUP_ID;
			CGroup group = ball.GetGroup ();
			if (group) {
				byGroupID = (byte)group.GetID ();
			}

			float fSize = ball.GetSize ();
			ushort usSize = (ushort)(fSize * 100f);

			float fWorldPosX = ball.GetPos ().x;
			float fWorldPosY = ball.GetPos ().y;

			float fLocalPosX = ball.GetLocalPosition ().x;
			float fLocalPosY = ball.GetLocalPosition ().y;

			float fPosX = ball.IsInGroup()?fLocalPosX:fWorldPosX;
			float fPosY = ball.IsInGroup()?fLocalPosY:fWorldPosY;

			float fCurShellTime = ball.GetCurShellTime ();
			ushort usCurShellTime = (ushort)( fCurShellTime * 100f );

			BitConverter.GetBytes (ball.GetIndex()).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(byte);
			BitConverter.GetBytes (byGroupID).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(byte);
			BitConverter.GetBytes (usSize).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(ushort);
			BitConverter.GetBytes (fPosX).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes (fPosY).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes (usCurShellTime).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(ushort);
			byte byShellType = (byte)ball.GetCurShellType ();
			BitConverter.GetBytes (byShellType).CopyTo( _bytesFullInfo, nPointer );
			nPointer += sizeof(byte);
		} // end for
		photonView.RPC ( "RPC_SyncFullInfo", PhotonTargets.AllViaServer,  _bytesFullInfo );
	}

	[PunRPC]
	public void RPC_SyncFullInfo( byte[] bytes )
	{
		if (photonView.isMine) { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
			return;
		}

		int nPointer = 0;
		int nLenOfPlayerName = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		string szPlayerName = StringManager.Bytes2String ( bytes, nPointer, nLenOfPlayerName );
		SetPlayerName ( szPlayerName );
		nPointer += nLenOfPlayerName;

		byte byObserve = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		SetObserve ( byObserve == 1?true:false );

		int nNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		m_nCurSkinIndex = (ushort)BitConverter.ToUInt16(bytes, nPointer);
		nPointer += sizeof (ushort);
		SetSkin ( m_nCurSkinIndex );
		byte byIsMoving =  (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		Local_SetMoving ( byIsMoving == 1?true:false );

		//// Group信息
		int nGroupNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		for (int i = 0; i < nGroupNum; i++) {
			int nGroupId = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);
			float fGroupPosX = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			float fGroupPosY = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);

			CGroup group = GetGroupByIndex ( nGroupId );
			if (group == null) {
				group = m_GroupManager.NewGroup( nGroupId );
			}
			group.SetDismissed ( false );

			vecTempPos.x = fGroupPosX;
			vecTempPos.y = fGroupPosY;
			vecTempPos.z = 0f;
			group.SetPos (vecTempPos);
		}
		//// end Group信息

		int nNumOfBallInGroup = 0;
		for (int i = 0; i < nNum; i++) {
			int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);
			if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
				Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
				continue;
			}
			Ball ball = m_lstBalls [nIndex];
			ball.SetDead ( false );

			byte byGroupId = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);
			if ( byGroupId != CGroup.INVALID_GROUP_ID ) {
				CGroup group = GetGroupByIndex ( byGroupId );
				if (group == null) {
					Debug.LogError ("有Bug!! group == null");
				} else {
					group.Local_DrawOneBall ( ball );
				}
				//m_Group.AddOneBall ( ball );
				//nNumOfBallInGroup++;
			}

			ushort usSize = BitConverter.ToUInt16(bytes, nPointer);
			nPointer += sizeof(ushort);
			float fSize = (float)usSize / 100f;
			ball.SetSize ( fSize );

			float fPosX = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);

			vecTempPos.x = fPosX;
			vecTempPos.y = fPosY;
			vecTempPos.z = -fSize;
			if ( ball.IsInGroup() ) {
				ball.SetLocalPosition ( vecTempPos );
			} else {
				ball.SetPos (vecTempPos);
			}

			ushort usCurShellTime = BitConverter.ToUInt16(bytes, nPointer);
			nPointer += sizeof(ushort);
			float fCurShellTime = (float)usCurShellTime / 100f;

			byte byShellType = (byte)BitConverter.ToChar(bytes, nPointer);
			Ball.eShellType eCurShellType = (Ball.eShellType)byShellType;
			nPointer += sizeof(byte);

			ball.Local_SetShellInfo (ball.GetShellTotalTimeByShellType (eCurShellType), fCurShellTime, eCurShellType);
			if ( eCurShellType != Ball.eShellType.none )
			{
				ball.Local_BeginShell ();
			}
		}

		m_bInitCompleted = true;
	}

	bool m_bObserve = false;
	public void SetObserve( bool val )
	{
		m_bObserve = val;
		if (m_bObserve) {
			this.gameObject.SetActive ( false );
		}
	}
	/*
	void Syncing_FullInfo()
	{
		return;

		if (m_SyncFullInfoIndex >= m_lstBalls.Count) {
			m_bSyncingFullInfo = false;
			return;
		}

		if (!m_bSyncingFullInfo) {
			return;
		}

		Ball ball = m_lstBalls[m_SyncFullInfoIndex];
		if (ball && ( !ball.IsDead() ) ) {
			ball.SyncFullInfo ();
		}

		m_SyncFullInfoIndex++;
	}
	*/
	Vector3 m_vWorldCursorPos = new Vector3();
	public Vector2 m_vSpecialDirecton = new Vector2();
	public float m_fRealTimeSpeedWithoutSize;
	float m_fSyncMoveInfoCount = 0f;

	public void SyncMoveInfo( Vector3 vecWorldCursorPos, Vector2 vecSpecialDirection, float fRealTimeSpeedWithoutSize )
	{
		m_fSyncMoveInfoCount += Time.deltaTime;
		if (m_fSyncMoveInfoCount < Main.s_Instance.m_fSyncMoveCountInterval) {
			return;
		}

		m_fSyncMoveInfoCount = 0f;
		m_vWorldCursorPos = vecWorldCursorPos;
		DoMove ( vecWorldCursorPos.x, vecWorldCursorPos.y, vecSpecialDirection.x, vecSpecialDirection.y, fRealTimeSpeedWithoutSize );
	}

	public Vector3 GetWorldCursorPos ()
	{
		return m_vWorldCursorPos;
	}

	public void DoMove ( float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_DoMove", PhotonTargets.AllViaServer, fWorldCursorPosX, fWorldCursorPosY, fSpecialDirectionX, fSpecialDirectionY, fRealTimeSpeedWithoutSize );
	}

	// 终于领悟了帧同步的真谛：同步指令而不是同步全量数据

	static Vector2 vec2TempPos = new Vector2 ();
	static Vector3 vec3TempPos = new Vector3 ();

	[PunRPC]
	public void RPC_DoMove( float fWorldCursorPosX, float fWorldCursorPosY, float fSpecialDirectionX, float fSpecialDirectionY, float fRealTimeSpeedWithoutSize )
	{
		if (photonView.isMine) {
			return;
		}

		m_fRealTimeSpeedWithoutSize = fRealTimeSpeedWithoutSize;
		m_vSpecialDirecton.x = fSpecialDirectionX;
		m_vSpecialDirecton.y = fSpecialDirectionY;
		m_vWorldCursorPos.x = fWorldCursorPosX;
		m_vWorldCursorPos.y = fWorldCursorPosY;
		vec3TempPos.x = fWorldCursorPosX;
		vec3TempPos.y = fWorldCursorPosY;
		for (int i = 0; i <  m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead()) {
				continue;
			}
			ball.BeginMove ( vec3TempPos, m_fRealTimeSpeedWithoutSize );
		}


		for (int i = 0; i < m_GroupManager.GetGroupList().Length; i++) {
			CGroup group = m_GroupManager.GetGroupList() [i];
			if (group == null || group.GetDismissed ()) {
				continue;
			}
			group.BeginMove ( vec3TempPos, fRealTimeSpeedWithoutSize); // 这儿可以优化，暂时㐀 poppin to youhua

		}
	}

	bool m_bMoving = false;
	public void SetMoving( bool val )
	{
		if (!photonView.isMine) {
			return;
		}
		photonView.RPC ( "RPC_SetMoving", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetMoving( bool val )
	{
		Local_SetMoving ( val );
	}

	public void Local_SetMoving( bool val )
	{
		if ( m_bMoving == false && val == true )
		{
			StartMove ();
		}
		m_bMoving = val;
		if (m_bMoving) {
			//EndMoveToCenter ();
		} else {
			StopMove ();
		}
	}

	public bool IsMoving()
	{
		return m_bMoving;
	}



	public void RemoveOneBall( Ball ball )
	{
		return;// No!!!!!!!!!!!!!

		m_lstBalls.Remove ( ball );
	}
		
	int m_nSyncBallsInfoIndex = 0;
	const float c_SyncBallsInfoInterval = 0.5f;
	float m_fSyncBallsInfoCount = 0.0f;
	public void SyncBallsInfo ()
	{
		return;

		if (!photonView.isMine) {
			return;
		}
	    
		/*
		if (m_fSyncBallsInfoCount < c_SyncBallsInfoInterval) {
			m_fSyncBallsInfoCount += Time.deltaTime;
			return;
		}
		m_fSyncBallsInfoCount = 0f;

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball == null ) {
				continue;
			}

			if (IsDead ()) {
				continue;
			}
			ball.SyncBaseInfo ();
		}
		*/
  
		int nCount = m_lstBalls.Count / 15;
		if (nCount < 1) {
			nCount = 1;
		}
		for (int i = 0; i < nCount; i++) {
			DoSyncBaseInfo ();
		}

	}

	void DoSyncBaseInfo()
	{
		if (m_lstBalls.Count == 0) {
			return;
		}

		if (m_nBallIndex >= m_lstBalls.Count) {
			m_nBallIndex = 0;
		}

		Ball ball = m_lstBalls [m_nBallIndex];
		if (ball != null) {
			ball.SyncBaseInfo ( false );
		} 
		m_nBallIndex++;
	}

	float m_fClearSpitTargetTotalTime = 0f;
	public void ClearSpitTarget()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.ClearSpitTarget ();
		}
	}

	public void BeginClearSpitTargetCount( float fTotalTime )
	{
		m_fClearSpitTargetTotalTime = fTotalTime * 0.7f;
	}

	void ClearSpitTargetCounting()
	{
		if (m_fClearSpitTargetTotalTime <= 0f) {
			return;
		}
		m_fClearSpitTargetTotalTime -= Time.deltaTime;
		if (m_fClearSpitTargetTotalTime <= 0f) {
			EndClearSpitTargetCount ();
		}
	}

	public void EndClearSpitTargetCount()
	{
		ClearSpitTarget ();
	}

	int m_nSpitIndex = 0;
	bool m_bSpitting = false;
/*
	public void DoSpit( int nPlatform )
	{
		Main.s_Instance.BeginCameraSizeProtect ( Main.s_Instance.m_fSpitRunTime );
		photonView.RPC ( "RPC_DoSpit", PhotonTargets.All, m_fSpitPercent, nPlatform, PhotonNetwork.time );
	}

	[PunRPC]
	public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );
		for ( int i = 0; i < m_lstBalls.Count; i++ ) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.CalculateChildSize ( fPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}
			ball.Spit ( fDelayTime, nPlatform, fChildSize, fMotherLeftSize);
		}

		if (photonView.isMine) {
			BeginClearSpitTargetCount (Main.s_Instance.m_fSpitRunTime);
		}

	}
*/
	public void DoSpit( int nPlatform )
	{
		Main.s_Instance.BeginCameraSizeProtect ( Main.s_Instance.m_fSpitRunTime );
		int nPointer = 0;
		int nNum = GetCurLiveBallsNum ();
		BitConverter.GetBytes (nNum).CopyTo (_bytesAdjustEjectInfo, 0);
		nPointer += sizeof (byte);
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetIndex () != i) {
				Debug.LogError ( "从个的？？ball.GetIndex () != i" );
				continue;
			}

			nNum++;

			float fPosX = ball.GetPos ().x;
			short sPosX = (short)( fPosX * 100f);
			float fPosY = ball.GetPos ().y;
			short sPosY = (short)( fPosY * 100f);

			BitConverter.GetBytes (ball.GetIndex()).CopyTo( _bytesAdjustEjectInfo, nPointer );
			nPointer += sizeof(byte);
			BitConverter.GetBytes (sPosX).CopyTo( _bytesAdjustEjectInfo, nPointer );
			nPointer += sizeof(short);
			BitConverter.GetBytes (sPosY).CopyTo( _bytesAdjustEjectInfo, nPointer );
			nPointer += sizeof(short);
		} // end for

		photonView.RPC ( "RPC_DoSpit", PhotonTargets.All, m_fSpitPercent, nPlatform, PhotonNetwork.time, _bytesAdjustEjectInfo );
	}

	[PunRPC]
	public void RPC_DoSpit( float fPercent, int nPlatform, double dOccurTime, byte[] bytes )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		int nPointer = 0;
		int nNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		for (int i = 0; i < nNum; i++) {
			int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);
			if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
				Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
				continue;
			}
			Ball ball = m_lstBalls [nIndex];
			if (ball.IsDead()) {
				Debug.LogError ( "从个的？？明明活着怎么是死亡状态" );
				continue;
			}

			short sPosX = BitConverter.ToInt16(bytes, nPointer);
			nPointer += sizeof(short);
			float fPosX = (float)sPosX / 100f;

			short sPosY = BitConverter.ToInt16(bytes, nPointer);
			nPointer += sizeof(short);
			float fPosY = (float)sPosY / 100f;

			if (!photonView.isMine) {
				ball.SetAdjustInfo (fPosX, fPosY, 1);
			}


			ball.CalculateChildSize ( fPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}
			ball.Spit ( fDelayTime, nPlatform, fChildSize, fMotherLeftSize);

		} // end for

		if (photonView.isMine) {
			BeginClearSpitTargetCount (Main.s_Instance.m_fSpitRunTime);
		}

	}



	public void Spitting( float fPercent )
	{
		m_fSpitPercent = fPercent;
		int nAvailableNum = GetCurAvailableBallCount ();
		int nCount = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls[i];
			if (ball.IsDead ()) {
				continue;
			}

			if (!ball.CheckIfCanForceSpit ()) {
				continue;
			}

			ball.CalculateChildSize ( fPercent );
			float fMotherLeftSize = 0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE || fMotherLeftSize < fChildSize) {
				continue;
			}

			ball.ShowTarget ();

			nCount++;
			if (nCount >= nAvailableNum) {
				break;
			}
		} // end for
	}
	float m_fSpitPercent = 0f;

	void EndSpit()
	{
		m_bSpitting = false;
	}

	float m_fPreDrawCount = 0.0f;
	void PreDrawLoop()
	{
		if (MapEditor.GetEatMode() != MapEditor.eEatMode.xiqiu) {
			return;
		}

		if (m_fPreDrawCount < 0.3f) {
			m_fPreDrawCount += Time.deltaTime;
			return;
		}
		m_fPreDrawCount = 0.0f;

		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead()) {
				continue;
			}
			ball.DoDraw ();
		}
	}

	Vector3 m_vecBalslsCenter = new Vector3();
	public Vector3 GetBallsCenter()
	{
		m_vecBalslsCenter.x = 0f;
		m_vecBalslsCenter.y = 0f;
		int nRealNum = 0;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			nRealNum++;
			vecTempPos = ball.GetPos ();
			m_vecBalslsCenter.x += vecTempPos.x;
			m_vecBalslsCenter.y += vecTempPos.y;
		}
		m_vecBalslsCenter.x /= nRealNum;
		m_vecBalslsCenter.y /= nRealNum;

		return m_vecBalslsCenter;
	}


	public void MergeAll()
	{
		photonView.RPC ( "RPC_MergeAll", PhotonTargets.All);
	}

	List<int> m_lstToMergeAllBalls = new List<int> ();
	[PunRPC]
	public void RPC_MergeAll()
	{
		int nNum = GetCurLiveBallsNum ();
		if (nNum < 2) {
			return;
		}

		vecTempPos = GetBallsCenter ();
		Ball ball = null;

		m_lstToMergeAllBalls.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++ ) {
			ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			m_lstToMergeAllBalls.Add ( ball.GetIndex() );
		}

		float fArea = 0f;
		for ( int i = 0; i < m_lstToMergeAllBalls.Count; i++ ) {
			ball = m_lstBalls [m_lstToMergeAllBalls[i]];
			if (ball.IsDead ()) {
				continue;
			}
			float fSize = ball.GetSize ();
			fArea += CyberTreeMath.SizeToArea (fSize, Main.s_Instance.m_nShit);
			if (i == m_lstToMergeAllBalls.Count - 1) {
				SetBallSize ( ball.GetIndex(), CyberTreeMath.AreaToSize (fArea, Main.s_Instance.m_nShit));
				vecTempPos.z = -ball.GetSize ();
				ball.SetPos (vecTempPos);
			} else {
				DestroyBall( ball.GetIndex () );
			}
		} // end for
	}

	public void Test_AddAllBallSize( float fVal )
	{
		photonView.RPC ( "RPC_AddAllBallSize", PhotonTargets.All, fVal);
	}

	[PunRPC]
	public void RPC_AddAllBallSize( float fVal )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.SetSize ( ball.GetSize() + fVal );
		}
	}

	public void SetThornOfBallSize( int nIndex, float fSize )
	{
		photonView.RPC ( "RPC_SetThornOfBallSize", PhotonTargets.All, nIndex, fSize );
	}

	[PunRPC]
	public void RPC_SetThornOfBallSize( int nIndex, float fSize )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls [nIndex];
		if (ball.IsDead ()) {
			Debug.LogError ( "从个的？？这个球都死球了还在SetSize" );
			return;
		}

		ball.SetThornSize ( fSize );
	}

	public void SetBallSize( int nIndex, float fSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_SetBallSize", PhotonTargets.All, nIndex, fSize );
	}

	[PunRPC]
	public void RPC_SetBallSize( int nIndex, float fSize )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls [nIndex];
		if (ball.IsDead ()) {
			Debug.LogError ( "从个的？？这个球都死球了还在SetSize" );
			return;
		}

		ball.Local_SetSize ( fSize );
	}

	public void DeleteBall( int nIndex )
	{
		photonView.RPC ( "RPC_DeleteBall", PhotonTargets.All, nIndex );
	}

	[PunRPC]
	public void RPC_DeleteBall( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return;
		}

		Ball ball = m_lstBalls[nIndex];
		ball.SetDead ( true );
	}

	public struct sSplitInfo
	{
		public Vector3 pos;
		public 	Vector2 dir;
		public int my_max_split_num;
		public int my_real_split_num;
		public int split_couting;
		public float real_split_size;
		public float seg_dis;
		public bool dead_after_split;
		public Ball _ball;

		public bool CheckIfDeadAfterSplit( ref float fMotherLeftSize )
		{
			float fMotherCurSize = _ball.GetSize ();
			float fMotherCurArea = CyberTreeMath.SizeToArea ( fMotherCurSize, Main.s_Instance.m_nShit );
			float fChildArea = CyberTreeMath.SizeToArea ( real_split_size, Main.s_Instance.m_nShit );
			fMotherCurArea -= fChildArea * my_real_split_num;
			if (fMotherCurArea <= 0f) {
				dead_after_split = true;
				return dead_after_split;
			} else {
				fMotherLeftSize = CyberTreeMath.AreaToSize ( fMotherCurArea, Main.s_Instance.m_nShit );
				if (fMotherLeftSize < Main.BALL_MIN_SIZE) {
					dead_after_split = true;
					return dead_after_split;
				}
			}
			dead_after_split = false;

			return dead_after_split;
		}
	};

	List<sSplitInfo> m_lstRecycledSpiltInfo = new List<sSplitInfo>();
	sSplitInfo ReuseSplitInfo()
	{
		sSplitInfo info;
		if (m_lstRecycledSpiltInfo.Count > 0) {
			info = m_lstRecycledSpiltInfo [0];
			m_lstRecycledSpiltInfo.RemoveAt (0);
			return info;
		}
		info = new sSplitInfo ();
		return info;
	}

	void RecycleSplitInfo( sSplitInfo info )
	{
		m_lstRecycledSpiltInfo.Add ( info );
	}

	void ClearSplitInfoList()
	{
		for (int i = 0; i < m_lstSplitBalls.Count; i++) {
			RecycleSplitInfo ( m_lstSplitBalls[i] );
		}
		m_lstSplitBalls.Clear ();
	}

	bool m_bSplitting = false;
	int m_nCurSplitTimes = 0;
	int m_nRealSplitNum = 0;
	List<sSplitInfo> m_lstSplitBalls = new List<sSplitInfo> ();
	byte[] _bytesSplit = new byte[90];
	public void OneBtnSplitBall( int nPlatform, Vector3 vecWorldCursorPos )
	{
		/*
		if (m_bSplitting) { // 正在分，还没分完呢，不要进行新一轮的分
			return;
		}	
		photonView.RPC ( "RPC_OneBtnSplitBall", PhotonTargets.All, PhotonNetwork.time, nPlatform, vecWorldCursorPos.x, vecWorldCursorPos.y  );
		*/
		// 废弃以前的“R键分球”流程
		int nCurLiveBallNum = 0;
		Ball ball = GetOneLiveBall( ref nCurLiveBallNum ); // 暂定只有一个球的时候才分
		if (nCurLiveBallNum > 1) {
			return;
		}

		// 计算这个球可不可以分够策划配置的个数，如果不能，那么可以具体可以分多少个
		int nExpectNum =  (int)Main.s_Instance.m_nSplitNum; 
		int nRealNum = 0;
		float fChildSize = 0.0f;
		float fMotherCurArea = CyberTreeMath.SizeToArea ( ball.GetSize(), Main.s_Instance.m_nShit);
		float fChildsArea = 0f;
		for (nRealNum = nExpectNum; nRealNum >= 2; nRealNum--) { // 至少分为2个，最多分策划配置的个数
			fChildsArea = fMotherCurArea / nRealNum;
			fChildSize = CyberTreeMath.AreaToSize (fChildsArea, Main.s_Instance.m_nShit);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}
		int nCurAvailableNum = GetCurAvailableBallCount ();
		if (nRealNum > nCurAvailableNum - 1) {
			nRealNum = nCurAvailableNum - 1;
		}
		if (nRealNum < 2 ) {
			return;
		}

		SetBallSize ( ball.GetIndex(), fChildSize  );

		int nChildNum = nRealNum - 1;

		Vector2 vecSplitDir = vecWorldCursorPos - ball.GetPos ();
		vecSplitDir.Normalize ();
		float fTotalDistance = Vector2.Distance ( vecWorldCursorPos, ball.GetPos () );

		int nPointer = 0;
		BitConverter.GetBytes (nRealNum).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (ball.GetIndex()).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);
		BitConverter.GetBytes (vecSplitDir.x).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (vecSplitDir.y).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (fTotalDistance).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (fChildSize).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(float);
		BitConverter.GetBytes (PhotonNetwork.time).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(double);

		GetSomeLiveBallIndex ( nChildNum );
		BitConverter.GetBytes (nChildNum).CopyTo (_bytesSplit, nPointer);
		nPointer += sizeof(byte);

		if (nChildNum != m_lstSomeAvailabelBallIndex.Count) {
			Debug.LogError ( "有Bug：nChildNum != m_lstSomeAvailabelBallIndex.Count" );
		}

		for (int i = 0; i < m_lstSomeAvailabelBallIndex.Count; i++) { // 比想要分的个数(nRealNum)少一个，因为母球自己还占一个
			BitConverter.GetBytes (m_lstSomeAvailabelBallIndex[i]).CopyTo (_bytesSplit, nPointer);
			nPointer += sizeof(byte);
		}

		photonView.RPC ( "RPC_OneBtnSplitBall_New", PhotonTargets.AllViaServer,  _bytesSplit );
	}

	[PunRPC]
	public void RPC_OneBtnSplitBall_New( byte[] bytes )
	{
		int nPointer = 0;
		int nRealNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);



		int nMotherIndex = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		Ball ballMother = GetBallByIndex ( nMotherIndex );
		if (ballMother == null) {
			Debug.LogError ( "出Bug了。初始化的时候每个球都是实例化了的，不可能为null" );
		}
		ballMother.SetDead ( false );

		float vecDirX = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		float vecDirY = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		float fTotalDistance = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		float fChildSize = BitConverter.ToSingle(bytes, nPointer);
		nPointer += sizeof (float);
		double fOccureTime = BitConverter.ToDouble(bytes, nPointer);
		nPointer += sizeof (double);
		float fDelayTime = (float)( PhotonNetwork.time - fOccureTime );

		int nChildNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		if (nChildNum != (nRealNum - 1)) {
			Debug.LogError ( "有Bug:nChildNum != (nRealNum - 1)" );
		}

		float fRealRunTime = Main.s_Instance.m_fSplitRunTime; /*- fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}
*/
		if (fTotalDistance > Main.s_Instance.m_fSplitMaxDistance) {
			fTotalDistance = Main.s_Instance.m_fSplitMaxDistance;
		}
		float fSegDis = fTotalDistance / nRealNum;
		vec2Temp.x = vecDirX;
		vec2Temp.y = vecDirY;

		bool bRealTimeFollowMouse = false;

		ballMother.SetShellEnable ( false );
		ballMother.SetSize ( fChildSize );
		ballMother.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, 0f, Ball.eShellType.r_split_ball );
		ballMother.Local_BeginShell ();

		for (int i = 0; i < nChildNum; i++) {
			int nChildIndex = (byte)BitConverter.ToChar(bytes, nPointer);  
			nPointer += sizeof (byte);

			Ball ballChild = GetBallByIndex ( nChildIndex );
			if (ballChild == null) {
				Debug.LogError ( "出Bug了。初始化的时候每个球都是实例化了的，不可能为null" );
			}
			ballChild.Local_SetPos (ballMother.GetPos());
			ballChild.RebornInit ();
			float fMyDistance = fSegDis * (i + 1);
			float fMySpeed = fMyDistance / fRealRunTime;
			ballChild.SetShellEnable ( false );
			ballChild.Local_SetSize (fChildSize);
			ballChild.Local_SetDir (vec2Temp);
			ballChild.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, fDelayTime, Ball.eShellType.r_split_ball );
			ballChild.Local_BeginEject_New( fMySpeed, fMyDistance);

		}

	}	

	public void OneBtnSplitBall( int nPlatform, float fTotalDistance )
	{
		if (m_bSplitting) { // 正在分，还没分完呢，不要进行新一轮的分
			return;
		}	
		photonView.RPC ( "RPC_OneBtnSplitBall_Mobile", PhotonTargets.All, nPlatform, fTotalDistance  );
	}




	public void PreOneBtnSplitting( float fCurOneBtnSplitDis )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.ShowSplitTarget ( fCurOneBtnSplitDis );
		}
	}

	public void CancelSplit()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.CancelSpit ();
		}
	}

	public void PreOneBtnSplitBall( int op, float param1, float param2 )
	{
		Ball ball = null;
		int nCurAvailableBallCount = GetCurAvailableBallCount ();
		if (nCurAvailableBallCount <= 0) {
			CancelSplit ();
			return;
		}
		int nPreTotalNum = 0;
		ClearSplitInfoList ();

		float fMotherLeftSize = 0f;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			ball = m_lstBalls [i];
			if (ball == null) {
				continue;
			}
			if (ball.IsDead ()) {
				continue;
			}
			sSplitInfo info = ReuseSplitInfo();
			info._ball = ball;
			info.my_max_split_num = 0;
			info.real_split_size = 0f;
			info.seg_dis = 0f;

			if (op == 0) {
				ball.CalculateRealSplitInfo (param1, param2, ref info.my_max_split_num, ref info.real_split_size, ref info.seg_dis);
			} else {
				ball.CalculateRealSplitInfo (param1, ref info.my_max_split_num, ref info.real_split_size, ref info.seg_dis);
			}
			nPreTotalNum += info.my_max_split_num;
			info.pos = ball.GetPos ();
			info.dir = ball.GetDir ();
			if ( (!photonView.isMine) && !IsMoving ()) {
				info.dir = ball.m_vecLastDirection;
			}
			info.split_couting = 0;
			info.my_real_split_num = 0;
			if (info.my_max_split_num <= 0) {
				info._ball.CanceSplit ();
			}
			//info.CheckIfDeadAfterSplit ( ref fMotherLeftSize );
			m_lstSplitBalls.Add (info);
		}

		bool bEnd = true;
		while ( nCurAvailableBallCount > 0) {
			bEnd = true;
			for (int i = 0; i < m_lstSplitBalls.Count; i++) {
				sSplitInfo info = m_lstSplitBalls [i];
				if (info.my_real_split_num < info.my_max_split_num) {
					info.my_real_split_num++;
					m_lstSplitBalls [i] = info;
					nCurAvailableBallCount--;
					bEnd = false;
				}
			}// end for
			if (bEnd) {
				break;
			}
		} // end while

	}

	[PunRPC]
	public void RPC_OneBtnSplitBall_Mobile( double dOccurTime, int nPlatform, float fTotalDistance )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		PreOneBtnSplitBall ( 1, fTotalDistance, 0 );

		m_bSplitting = true;
		m_nCurSplitTimes = 0;

		while (m_bSplitting) {
			Splitting ( fDelayTime, nPlatform );
		}
	}


	[PunRPC]
	public void RPC_OneBtnSplitBall( double dOccurTime, int nPlatform, float fWorldCursorPosX, float fWorldCursorPosY )
	{
		float fDelayTime = (float)( PhotonNetwork.time - dOccurTime );

		PreOneBtnSplitBall ( 0, fWorldCursorPosX, fWorldCursorPosY );

		m_bSplitting = true;
		m_nCurSplitTimes = 0;

		while (m_bSplitting) {
			Splitting ( fDelayTime, nPlatform );
		}
	}

	void Splitting( float fDelayTime, int nPlatform )
	{
		if (!m_bSplitting) {
			return;
		}

		Ball ball = null;
		bool bEnd = true;
		for (int i = 0; i < m_lstSplitBalls.Count; i++ ) {
			sSplitInfo info  = m_lstSplitBalls [i];
			if ( info.split_couting >= info.my_real_split_num) {
				info._ball.CanceSplit ();
				continue;
			}
			info._ball.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, 0f, Ball.eShellType.r_split_ball );
			info._ball.Local_BeginShell ();
			SplitOne ( fDelayTime, nPlatform, m_nCurSplitTimes, info);
			info.split_couting++;
			m_lstSplitBalls [i] = info;
			bEnd = false;
		}
		m_nCurSplitTimes++;

		if (bEnd) {
			EndOneBtnSplit ();
		}
	}

	public void EndOneBtnSplit()
	{
		m_bSplitting = false;
		float fMotherLeftSize = 0f;
		for (int i = 0; i < m_lstSplitBalls.Count; i++) {
			sSplitInfo info = m_lstSplitBalls [i];
			if (info.CheckIfDeadAfterSplit ( ref fMotherLeftSize )) {
				if (photonView.isMine) {
					DestroyBall( info._ball.GetIndex() );
				}
			} else {
				info._ball.Local_SetSize ( fMotherLeftSize );
				info._ball.Local_BeginShell ();
			}
		}
	}

	public void SplitOne( float fDelayTime, int nPlatform, int nTimes, sSplitInfo info )
	{
		bool bNew = false;
		Ball new_ball = ReuseOneBall (); 
		if (new_ball == null) {
			//Debug.LogError ( "SplitOne  new_ball == null" );
			return;
		}

		if (info.seg_dis < 1 ) {
			info.seg_dis = 1f;
		}
		float fDis = info.seg_dis * ( nTimes + 1 );

		float fRealRunTime =  Main.s_Instance.m_fSplitRunTime - fDelayTime;
		if (fRealRunTime < Main.s_Instance.m_fMinRunTime) {
			fRealRunTime = Main.s_Instance.m_fMinRunTime;
		}
		//float fInitSpeed = CyberTreeMath.GetV0 ( fDis, fRealRunTime  );
		//float fAccelerate = CyberTreeMath.GetA ( fDis, fRealRunTime);

		bool bRealTimeFollowMouse = false;


		Ball.RongCuo_NotPointToMyself ( ref info.dir.x, ref info.dir.y );

		float fSpeed = fDis / fRealRunTime;

		new_ball.SetShellEnable ( false );
		new_ball.Local_SetPos (info.pos);
		new_ball.Local_SetSize (info.real_split_size);
		new_ball.Local_SetDir (info.dir);
		new_ball.Local_SetShellInfo ( Main.s_Instance.m_fSplitShellShrinkTotalTime, 0f, Ball.eShellType.r_split_ball );
		new_ball.Local_BeginEject_New( fSpeed, fDis, bRealTimeFollowMouse, true);
	}

	public struct sExplodeInfo
	{

	};

	List<int> m_lstToExplodeBalls = new List<int>();
	public void OneBtnExplode()
	{
		m_lstToExplodeBalls.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			m_lstToExplodeBalls.Add ( ball.GetIndex() );
		}

		for (int i = 0; i < m_lstToExplodeBalls.Count; i++) {
			ExplodeBall ( m_lstToExplodeBalls[i] );
		}
	}

	public void StartDaTaoSha()
	{
		photonView.RPC ( "RPC_StartDaTaoSha", PhotonTargets.AllBuffered, (float)PhotonNetwork.time );
	}

	[PunRPC]
	public void RPC_StartDaTaoSha( float fStarttime )
	{
		DaTaoSha.s_Instance.StartDaTaoSha (fStarttime);
		DaTaoSha.s_Instance.m_btnStart.gameObject.SetActive ( false );
	}

	public void EatDaTaoShaFood()
	{

	}

	public int GetCurMainPlayerBallCount()
	{
		return m_lstBalls.Count;
	}

	public int GetCurBallCount()
	{
		return m_lstBalls.Count;
	}

	public int GetCurTotalBallCountIncludeRecycled()
	{
		return ( m_lstBalls.Count + m_lstRecycledBalls.Count );
	}

	public int GetAvailableBallCount()
	{
		return (int)Main.s_Instance.m_fMaxBallNumPerPlayer - GetCurBallCount ();
	}

	public void RemoveOnetattoo( TattooFood food )
	{
		food.SetDestroyed ( true );
		photonView.RPC ( "RPC_RemoveOnetattoo", PhotonTargets.AllBuffered, food.GetGUID() );
	}

	[PunRPC]
	public void RPC_RemoveOnetattoo( int nGUID )
	{
		TattooFood food = null;
		Main.s_Instance.RemoveOnetattoo (nGUID);
	}

	public void GenerateOneFood( int id, int nGUID )
	{
		if (!PhotonNetwork.isMasterClient) {
			return;
		}

		float fDis = DaTaoSha.s_Instance.m_Render.bounds.size.x / 2f * 0.707f;
		vecTempPos.x = (float)UnityEngine.Random.Range ( -fDis, fDis );
    	vecTempPos.y = (float)UnityEngine.Random.Range ( -fDis, fDis );
		photonView.RPC ("RPC_GenerateOneFood", PhotonTargets.AllBuffered, id, nGUID, vecTempPos.x, vecTempPos.y);
	}

	[PunRPC]
	public void RPC_GenerateOneFood( int id, int nGUID, float posX, float posY )
	{
		bool ret = false;
		MapEditor.sBeiShuDouZiConfig config = MapEditor.s_Instance.GetFoodConfig (id, ref ret);
		if (!ret) {
			return;
		}

		GameObject goFood = ResourceManager.s_Instance.PrefabInstantiate ( "prefab/TattooFood/preTF_0" );
		TattooFood food = goFood.GetComponent<TattooFood> ();
		food.SetGUID ( nGUID );
		food._type = TattooFood.eTattooFoodType.TattooFood_BeiShuDouZi;
		food._subtype = config.id;
		food.SetParam( 0, (object)config.value );
		food.gameObject.SetActive ( true );
		/*
		if (config.id == 0) {
			food.SetColor ( Color.green );
		} else {
			food.SetColor ( Color.red );
		}
		*/
		vecTempPos.x = posX;
		vecTempPos.y = posY;
		vecTempPos.z = 0f;
		food.transform.localPosition = vecTempPos;
		Main.s_Instance.AddOnetattooFood ( food );
		DaTaoSha.s_Instance.AddOneFood ( food );
	}

	public Ball GetOneAvailableBall()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball && (!ball.IsDead ())) {
				return ball;
			}
		}
		return null;
	}

	public List<Ball> GetBallList()
	{
		return m_lstBalls;
	}

	bool m_bSpittingSpore = false;
	public void BeginSpitSpore()
	{
		photonView.RPC ( "RPC_BeginSpitSpore", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_BeginSpitSpore()
	{
		m_bSpittingSpore = true;
	}

	public bool IsSpittingSpore()
	{
		return m_bSpittingSpore;
	}

	float m_fSpitSporeTimeCount = 0f;
	public void SpittingSpore()
	{
		if (!m_bSpittingSpore) {
			return;
		}

		m_fSpitSporeTimeCount += Time.deltaTime;
		if (m_fSpitSporeTimeCount < Main.s_Instance.m_fSpitSporeTimeInterval) {
			return;
		}
		m_fSpitSporeTimeCount = 0f;

		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.SpitBean ();
		}
	}

	public void EndSpitSpore()
	{
		photonView.RPC ( "RPC_EndSpitSpore", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_EndSpitSpore()
	{
		m_bSpittingSpore = false;
	}

	float m_nAttenuateTime = 0;
	void Attenuate()
	{
		return; // 衰减需要优化

		if (m_nAttenuateTime < 0.3f) {
			m_nAttenuateTime += Time.deltaTime;
			return;
		}
		m_nAttenuateTime = 0f;
		Ball ball = null;
		float fPercent = Main.s_Instance.m_fAutoAttenuate * Time.deltaTime;
		float fPercentThorn = Main.s_Instance.m_fThornAttenuate * Time.deltaTime;
		for (int i = m_lstBalls.Count - 1; i >= 0; i--) {
			ball = m_lstBalls[i];
			if (ball == null) {
				Debug.LogError ( "ball null" );
				continue;
			}
			if (ball.IsDead ()) {
				continue;
			}
			float fCurSize = ball.GetSize ();
			if (fCurSize <= Main.s_Instance.m_fBornMinDiameter) {
				continue;
			}
			float fDelta = fCurSize * fPercent;
			float fLeftSize = fCurSize - fDelta;
			if (fLeftSize <= Main.s_Instance.m_fBornMinDiameter) {
				continue;
			}
			ball.Local_SetSize ( fLeftSize );

			float fCurThornSize = ball.GetThornSize ();
			float fDeltaThorn = fCurThornSize * fPercentThorn;
			float fLeftThornSize = fCurThornSize - fDeltaThorn;
			if (fLeftThornSize < 0f) {
				fLeftThornSize = 0f;
			}
			ball.Local_SetThornSize ( fLeftThornSize );
		}
	}

	public void BeginBeanSprayPreDraw( bool bAll, int nSprayId = 0 )
	{
		photonView.RPC ( "RPC_BeginBeanSprayPreDraw", PhotonTargets.All, (float)PhotonNetwork.time, bAll, nSprayId );
	}

	[PunRPC]
	public void RPC_BeginBeanSprayPreDraw( float fPreDrawBeginTinme, bool bAll, int nSprayId )
	{
		MapEditor.s_Instance.BeginSprayPreDraw ( fPreDrawBeginTinme, bAll, nSprayId );
	}

	public void SprayEatBall( int nSprayGUID, float fEatSize )
	{
		photonView.RPC ( "RPC_SprayEatBall", PhotonTargets.All, nSprayGUID, fEatSize );

	}

	[PunRPC]
	public void RPC_SprayEatBall( int nSprayGUID, float fEatSize )
	{
		MapEditor.s_Instance.SprayEatBall ( nSprayGUID, fEatSize );
	}

	public static Player FindPlayerByOwnerId( int nOwnerId )
	{
		GameObject goPlayer = GameObject.Find ("Balls/player_" + nOwnerId);
		if (goPlayer == null) {
			Debug.LogError ( "player error" );
			return null;
		}
		Player player = goPlayer.GetComponent<Player> ();
		return player;
	}

	public Ball FindBallByViewId( int nViewId )
	{
		/*
		for (int i = 0; i < m_lstBalls.Count; i++) { // 这种查找效率太低，后期优化一下
			if (m_lstBalls [i] != null && m_lstBalls [i].photonView.viewID == nViewId) {
				return m_lstBalls [i];
			}
		}
		*/

		return null;
	}

	public void CaptureOneBall( int nSprayID, int nOwnerId, int nViewId )
	{
		photonView.RPC ( "RPC_CaptureOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId );

	}

	[PunRPC]
	public void RPC_CaptureOneBall( int nSprayID, int nOwnerId, int nViewID )
	{
		Spray spray = MapEditor.s_Instance.FindSprayById ( nSprayID );
		if (spray == null) {
			Debug.LogError ( "RPC_CaptureOneBall spray null" );
			return;
		}
		Player player = Player.FindPlayerByOwnerId ( nOwnerId );
		if (player == null) {
			Debug.LogError ( "RPC_CaptureOneBall player null" );
			return;
		}
		Ball ball = player.FindBallByViewId ( nViewID );
		if ( ball == null ){
			Debug.LogError ( "RPC_CaptureOneBall ball null" );
			return;
		}
		spray.DoCaptureOneBall ( ball );
	}

	public void ReleaseOneBall( int nSprayID, int nOwnerId, int nViewId )
	{
		photonView.RPC ( "RPC_ReleaseOneBall", PhotonTargets.All, nSprayID, nOwnerId, nViewId );

	}

	[PunRPC]
	public void RPC_ReleaseOneBall( int nSprayID, int nOwnerId, int nViewID )
	{
		Spray spray = MapEditor.s_Instance.FindSprayById ( nSprayID );
		if (spray == null) {
			Debug.LogError ( "RPC_CaptureOneBall spray null" );
			return;
		}
		Player player = Player.FindPlayerByOwnerId ( nOwnerId );
		if (player == null) {
			Debug.LogError ( "RPC_CaptureOneBall player null" );
			return;
		}
		Ball ball = player.FindBallByViewId ( nViewID );
		if ( ball == null ){
			Debug.LogError ( "RPC_CaptureOneBall ball null" );
			return;
		}
		spray.DoReleaseOneBall ( ball );
	}


	List<int> m_lstLiveBall = new List<int> ();

	public void RefreshLiveBalls()
	{

	}

	public Ball GetBallByIndex( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
			Debug.LogError ( "nIndex < 0 || nIndex >= m_lstBalls.Count" );
			return null;
		}
		return m_lstBalls[nIndex];
	}

	int m_nBallNum = 0;
	Ball ReallyInstantiateOneBall()
	{
		vecTempPos.x = -10000f;
		vecTempPos.y = -10000f;
		GameObject goBall = Main.s_Instance.PhotonInstantiate( Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params) ;
		goBall.transform.parent = this.transform;
		goBall.transform.position = vecTempPos;
		Ball ball = goBall.GetComponent<Ball> ();
		return ball;
	}

	public void AddOneBall( Ball ball )
	{
		int nIndex = m_lstBalls.Count;
		m_lstBalls.Add ( ball );
		ball.SetIndex ( nIndex );
		ball.transform.parent = this.transform;
		ball.SetPlayer ( this );
		ball.SetDead ( true );
		ball.SetEaten ( false );
	}

	List<int> m_lstSomeAvailabelBallIndex = new List<int>();
	public void GetSomeLiveBallIndex( int nNeedNum )
	{
		m_lstSomeAvailabelBallIndex.Clear ();
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				m_lstSomeAvailabelBallIndex.Add ( ball.GetIndex() );
			}
			if (m_lstSomeAvailabelBallIndex.Count >= nNeedNum) {
				return;
			}
		}
	}

	public Ball ReuseOneBall()
	{
		Ball ball = null;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				ball.SetDead ( false );
				ball.RebornInit ();
				return ball;
			}
		}

		//Debug.LogError ( "从个的？？ReuseOneBall居然失败了！！！！！！" );
		return null;
	}

	public List<Ball> m_lstRecycledBalls = new List<Ball> ();
	public void RecycleOneBall( Ball ball )
	{
		//MapEditor.s_Instance.LogTestInfo ( ball.photonView.viewID, "RecycleOneBall" + ball.photonView.viewID );
		//MapEditor.s_Instance._inputTest.text += (ball.photonView.viewID + "\n");

		ball.SetDead ( true );
//		Debug.Log ( "RecycleOneBall: " + ball.photonView.viewID );
		bool bRet = m_lstBalls.Remove ( ball );
		if (!bRet) {
			Debug.LogError ("bool bRet = m_lstBalls.Remove ( ball );");
		} else {
//			Debug.Log ( "111111111111111111: " + ball.photonView.viewID );
		}

		m_lstRecycledBalls.Add ( ball );
	}

	public void AddOneBallToRecycled( Ball ball )
	{
		ball.SetDead ( true );
		m_lstRecycledBalls.Add ( ball );
	}


	public void EndInitAllBalls()
	{
		
	}

	bool m_bDead = false;
	public bool CheckIfPlayerDead()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (!ball.IsDead ()) { // 只要还有一个球没死，玩家就没死
				m_bDead = false;
				return m_bDead;
			}
		}
		m_bDead = true;
		return m_bDead;
	}

	public bool IsDead()
	{
		return m_bDead;
	}

	bool m_bReborn = false;
	public void SetRebornCompleted( bool val )
	{
		m_bReborn = val;
	}

	public bool IsRebornCompleted()
	{
		return m_bReborn;
	}

	// 主角所有的球球向中心点靠拢
	bool m_bAllBallsMovingToCenter = false;
	public void BeginMoveToCenter()
	{
		photonView.RPC ( "RPC_BeginMoveToCenter", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_BeginMoveToCenter()
	{
		GetBallsCenter ();
		m_bAllBallsMovingToCenter = true;
	}

	void MoveToCenter()
	{
		return; // poppin test

		if (!m_bAllBallsMovingToCenter) {
			return;
		}

		bool bEnd = true;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			if (ball.GetMovingToCenterStatus () == 2) {
				continue;
			}
			bEnd = false;

			if (ball.GetMovingToCenterStatus () == 0) { // 还没开始移动
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.SetMovingToCenterStatus(1);
				ball.CalculateMoveToCenterRealSpeed ();
			}
			else if (ball.GetMovingToCenterStatus () == 1) { // 移动中
				GetBallsCenter ();
				ball.SetBallsCenter ( m_vecBalslsCenter );
				ball.CalculateMoveToCenterRealSpeed ();
				ball.DoMovingToCenter();
				//if (CyberTreeMath.CheckIfArriveDest (ball.GetPos (), m_vecBalslsCenter, 1f)) {
				//	ball.SetMovingToCenterStatus (2);
				//}
			}
		}
		if (bEnd) {
			EndMoveToCenter ();
		}
	}

	public void StartMove()
	{

	}

	public void StopMove ()
	{
		if (photonView.isMine) {
			AdjustMoveInfo (1);
		}
	}

	public void EndMoveToCenter()
	{
		m_bAllBallsMovingToCenter = false;
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetMovingToCenterStatus (0);
		}
	}

	public void UpdateIndicatorType( int type )
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (type == CtrlMode.DIR_INDICATOR_ARROW) {
				
			} else if (type == CtrlMode.DIR_INDICATOR_LINE) {

			}
		}

	}

	public void SetPos( Vector2 pos )
	{
		m_vecPos = pos;
	}

	public Vector2 GetPos()
	{
		return m_vecPos;
	}

	int m_nTestExplodeStatus = 0; // 0 - none   1 - 正在炸开    2 - 正在收回
	float m_fExplodeTimeCount = 0f;
	float m_fMoveToCenterTimeCount = 0f;
	bool m_bTestPlayer = false;
	void TestExplode()
	{
		if (!m_bTestPlayer) {
			return;
		}

		if (m_nTestExplodeStatus == 0) {
			int nShit = (int)UnityEngine.Random.Range ( 0.0f, 50.0f );
			if (nShit == 5) {
				float fRunDistance = Main.s_Instance.m_fExplodeRunDistanceMultiple * 20.0f;
				float fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fExplodeRunTime  );
				float fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fExplodeRunTime);
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					Vector2 vecDire = MapEditor.s_Instance.GetExplodeDirection ( i );
					ball._dirx = vecDire.x;
					ball._diry = vecDire.y;
					ball.Local_SetShellInfo ( 8f, 0f, Ball.eShellType.explode_ball );
					ball.Local_BeginEject (fInitSpeed, fAccelerate, Main.s_Instance.m_fExplodeRunTime, false, false, Main.s_Instance.m_fExplodeStayTime);
				}

				m_fExplodeTimeCount = 0f;
				SetTestExplodeStatus (1);
			}
		} else if (m_nTestExplodeStatus == 1) {
			m_fExplodeTimeCount += Time.deltaTime;
			if (m_fExplodeTimeCount >= Main.s_Instance.m_fExplodeRunTime) {
				RPC_BeginMoveToCenter ();
				m_fMoveToCenterTimeCount = 0f;
				SetTestExplodeStatus (2);
			}
		} else if (m_nTestExplodeStatus == 2) {
			m_fMoveToCenterTimeCount += Time.deltaTime;
			if (m_fMoveToCenterTimeCount >= 16f ) {
				for (int i = 0; i < m_lstBalls.Count; i++) {
					Ball ball = m_lstBalls [i];
					ball.Local_SetPos ( GetPos() );
				}
				SetTestExplodeStatus (0);
			}
		}
	}

	bool CheckIfAllBallsEndMoveToCenter()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.GetMovingToCenterStatus () != 2) {
				return false;
			}
		} // end for
		return true;
	}

	void SetTestExplodeStatus( int nStatus )
	{
		m_nTestExplodeStatus = nStatus;
	}

	public void SetTestPlayer( bool bVal )
	{
		m_bTestPlayer = bVal;
	}

	void SyncMoveInfo ()
	{
		if (Main.s_Instance == null) {
			return;
		}
		if (Main.s_Instance.m_MainPlayer == null) {
			return;
		}
		Main.s_Instance.m_MainPlayer.SyncMoveInfo ( Main.s_Instance._playeraction._world_cursor.transform.position, Main.s_Instance._playeraction.m_vec2MoveInfo, Main.s_Instance._playeraction._ball_realtime_velocity_without_size); 
	}

	float m_nAdjustMoveInfoFrameCount = 0f;
	public const float c_nAdjustMoveInfoSyncInterval = 0.5f; // 多少秒校验一次
	byte[] _bytesAdjustMoveInfo = new byte[1024]; // 这个数据块的大小是可以优化的 poppin to do
	byte[] _bytesAdjustEjectInfo = new byte[512]; // 这个数据块的大小是可以优化的 poppin to do
	int m_nAdjustBallPosIndex = 0;

	void DoAdjustMoveInfo(int nDirect = 0 )
	{
		Ball ball = null;
		bool bFinding = true;
		while (bFinding) {
			if (m_nAdjustBallPosIndex >= m_lstBalls.Count) {
				m_nAdjustBallPosIndex = 0;
			}
			ball = m_lstBalls [m_nAdjustBallPosIndex];
			m_nAdjustBallPosIndex++;
			if (ball.IsDead ()) {
				continue;
			}
			bFinding = false;
		}
		byte byDirect = (byte)nDirect;
		byte byIndex = (byte)ball.GetIndex ();
		float fPosX = ball.GetPos ().x;
		short sPosX = (short)( fPosX * 100f);
		float fPosY = ball.GetPos ().y;
		short sPosY = (short)( fPosY * 100f);
		photonView.RPC ("RPC_AdjustMoveInfo", PhotonTargets.Others, byDirect, byIndex, sPosX, sPosY );
	}

	void AdjustMoveInfo( int nDirect = 0, int nOtherOperate = 0 )
	{
		if (!photonView.isMine) {
			return;
		}

		if (!MapEditor.s_Instance.IsMapInitCompleted ()) {
			return;
		}

		if (  m_lstBalls.Count < Main.s_Instance.m_fMaxBallNumPerPlayer ) {
			return;
		}

		if (nDirect == 0) {
			m_nAdjustMoveInfoFrameCount += Time.deltaTime;
			if (m_nAdjustMoveInfoFrameCount < c_nAdjustMoveInfoSyncInterval) {
				return;
			}
			m_nAdjustMoveInfoFrameCount = 0f;
		}

		int nPointer = 0;
		int nNum = GetCurLiveBallsNum();
		BitConverter.GetBytes (nDirect).CopyTo (_bytesAdjustMoveInfo, nPointer);
		nPointer += sizeof (byte);
		BitConverter.GetBytes (nNum).CopyTo (_bytesAdjustMoveInfo, nPointer);
		nPointer += sizeof (byte);

		int nGroupNum = m_GroupManager.GetLiveGroupNum(); // poppin to do
		BitConverter.GetBytes (nGroupNum).CopyTo (_bytesAdjustMoveInfo, nPointer);
		nPointer += sizeof (byte);

		int nTestBallNum = 0;
		int nTestGroupNum = 0;
		//foreach( KeyValuePair<int, CGroup> pair in dicLiveGroup ){
		CGroup[] lstGroup = m_GroupManager.GetGroupList();
		for ( int i = 0 ; i < lstGroup.Length; i++ ){
			CGroup group = lstGroup[i];
			if (group == null ) {
				//Debug.LogError ( "MainPlayer 从个的？？group == null " );
				continue;
			}

			if (group.GetDismissed ()) {
				//Debug.LogError ( "MainPlayer 从个的？？group.GetDismissed () " );
				continue;
			}

				
			float fPosX = group.GetPos ().x;
			float fPosY = group.GetPos ().y;
			BitConverter.GetBytes ((byte)group.GetID()).CopyTo (_bytesAdjustMoveInfo, nPointer);
			nPointer += sizeof(byte);
			BitConverter.GetBytes (fPosX).CopyTo (_bytesAdjustMoveInfo, nPointer);
			nPointer += sizeof(float);
			BitConverter.GetBytes (fPosY).CopyTo (_bytesAdjustMoveInfo, nPointer);
			nPointer += sizeof(float);
			nTestGroupNum++;
		}

		for (int i = 0; i < m_lstBalls.Count; i++) { 
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

			if (ball.GetIndex () != i) {
				Debug.LogError ( "MainPlayer 从个的？？ball.GetIndex () != i" );
				continue;
			}

			if (ball.GetIndex () >= 64) {
				Debug.LogError ( "MainPlayer 从个的？？居然都超过64了" );
				continue;
			}


			byte byGroupId = CGroup.INVALID_GROUP_ID;
			CGroup group = ball.GetGroup ();
			if (group) {
				byGroupId = (byte)group.GetID ();
			}

            float fPosX = byGroupId != CGroup.INVALID_GROUP_ID ? ball.GetLocalPosition().x : ball.GetPos ().x;
            float fPosY = byGroupId != CGroup.INVALID_GROUP_ID ? ball.GetLocalPosition().y : ball.GetPos ().y;

			BitConverter.GetBytes (byGroupId).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(byte);
			BitConverter.GetBytes (ball.GetIndex()).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(byte);
            BitConverter.GetBytes(ball.GetGroupCount(byGroupId)).CopyTo(_bytesAdjustMoveInfo, nPointer);
            nPointer += sizeof(byte);
            BitConverter.GetBytes (fPosX).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(float);
			BitConverter.GetBytes (fPosY).CopyTo( _bytesAdjustMoveInfo, nPointer );
			nPointer += sizeof(float);



			nTestBallNum++;
		} // end for

		if (nTestBallNum != nNum) {
			Debug.LogError ( "nTestBallNum != nNum" );
		}

		if (nTestGroupNum != nGroupNum) {
			Debug.LogError ( "nTestGroupNum != nGroupNum" );
		}

		photonView.RPC ( "RPC_AdjustMoveInfo", PhotonTargets.AllViaServer,  _bytesAdjustMoveInfo );

		ProcessOtherOperate ( nOtherOperate );
	}

	[PunRPC]
	public void RPC_AdjustMoveInfo( byte[] bytes/*byte byDirect, byte byIndex, short sPosX, short sPosY*/ )
	{
		if (photonView.isMine) { // 全量信息是同步给其它客户端的，自己没必要再去解析一遍
			return;
		}

		if (!IsInitCompleted ()) {
			return;
		}

		int nPointer = 0;
		int nDirect = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		int nBallsNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);
		int nGroupNum = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);

		for (int i = 0; i < nGroupNum; i++) {

			bool bCanDo = true;
			byte byGroupId = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof(byte);
			CGroup group = GetGroupByIndex ( (int)byGroupId );
			if (group == null ) {
				Debug.LogError ( "OtherClient 有Bug!!group == null  "   );
				bCanDo = false;
			}

			if ( group && group.GetDismissed ()) {
				//Debug.LogError ( "OtherClient  有Bug!!group.GetDismissed () "   );
				bCanDo = false;
			}

			if ( group && group.GetID () != byGroupId) {
				Debug.LogError ( " OtherClient group.GetID () != byGroupId :" + group.GetID () + "_" + byGroupId  );
				bCanDo = false;
			}

			float fPosX = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);

			vec2TempPos.x = fPosX;
			vec2TempPos.y = fPosY;

			if (bCanDo) {
				group.SetAdjustInfo (fPosX, fPosY, nDirect);
			}
	
		}
		//// end Group


		for (int i = 0; i < nBallsNum; i++) { 
			byte byGroupId = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);

			int nIndex = (byte)BitConverter.ToChar(bytes, nPointer);
			nPointer += sizeof (byte);

            int nGroupRoundCount = (byte)BitConverter.ToChar(bytes, nPointer);
            nPointer += sizeof(byte);

            bool bCanDo = true;
			if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
				Debug.LogError ( "从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count: " + nIndex + "_" + nBallsNum);
				bCanDo = false;
			}
			Ball ball = m_lstBalls [nIndex];
			if (ball.IsDead()) {
				Debug.LogError ( "从个的？？明明活着怎么是死亡状态" );
				bCanDo = false;
			}

			ball.gameObject.name = "ball_" + nIndex;


			if (ball.GetIndex () != nIndex) {
				Debug.LogError ( "从个的？？ball.GetIndex () != nIndex :" + ball.GetIndex () + "_" + nIndex );
				bCanDo = false;
			}
				
			float fPosX = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle(bytes, nPointer);
			nPointer += sizeof(float);

			if (bCanDo) {
				if (byGroupId != CGroup.INVALID_GROUP_ID) { // in group
					CGroup group1 = GetGroupByIndex (byGroupId);
					if (group1 == null || group1.GetDismissed () || group1.GetID () != byGroupId) {
						//Debug.LogError ("从个的111111111111111");
						bCanDo = false;
					}
					CGroup group2 = ball.GetGroup ();
					if (group2 == null || group2.GetDismissed () || group2.GetID () != byGroupId) {
						//Debug.LogError ("从个的222222222222222222222");
						bCanDo = false;
					}
					if (group1 && group2) {
						if (group1.GetID () != group2.GetID () || group1.transform != group2.transform) {
							//Debug.LogError ("从个的333333333333333333333333");
							bCanDo = false;
						}

                        if (group1 != group2)
                        {
                            bCanDo = false;
                        }

                        if (ball.transform.parent != group1.transform || ball.transform.parent != group2.transform ) {
							//Debug.LogError ("从个的44444444444444444444444444");
							bCanDo = false;

						}
						if (ball.transform.parent == this.transform) {
							//Debug.LogError ("从个的555555555555555555555555555555");
							bCanDo = false;
						}
					}

					if (bCanDo) {
                        ball.SetAdjustInfo_InGroup (fPosX, fPosY); 
                    }
                    else {
						
					}
				} else {

					foreach (Transform child in this.transform) {
						if (child.tag == "Group") {
							CGroup group = child.gameObject.GetComponent<CGroup> ();
							if (group && ball.transform.parent == group.transform ) {
								//Debug.LogError ("从个的66666666666666666666666");
								bCanDo = false;
								break;
							}
						}
					}
					if (bCanDo) {
						ball.SetAdjustInfo (fPosX, fPosY, nDirect);
					} else {
						//Debug.LogError ("从个的77777777777777");
					}
				}
			} // end if cando
      
        } // end for

	
	}

	public void ProcessOtherOperate ( int nOtherOperate )
	{
		switch (nOtherOperate) {
		case 1:
			{
				EndEject ();
			}
			break;
		}
	}

	public void EndEject ()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.EndEject ();
		}
	}

	public void EatBall( int nPlayerPhotonID, int nBallIndex )
	{

	}

	[PunRPC]
	public void RPC_BeEaten( int nBallIndex )
	{
		Ball ball = m_lstBalls[nBallIndex];
		ball.SetEaten ( true );
		if ( photonView.isMine) {
			DestroyBall ( nBallIndex );
		}
	}

	[PunRPC]
	public void RPC_EatSucceed( int nBallIndex, float fNewSize )
	{
		if (!photonView.isMine) { // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
			return;
		}

		SetBallSize ( nBallIndex, fNewSize );
	}

	public void DestroyBall( int nBallIndex ) // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
	{
		if (!photonView.isMine) { // 必须由MainPlayer来执行销毁球操作，否则时序会混乱
			return;
		}

		photonView.RPC ( "RPC_DestroyBall", PhotonTargets.All,  nBallIndex );
	}

	[PunRPC]
	public void RPC_DestroyBall( int nBallIndex )
	{
		Ball ball = m_lstBalls[nBallIndex];
		ball.SetDead ( true );
	}

	public void ExplodeBall( int nBallIndex, float fPercent = 0f )
	{
		if (photonView.isMine) {
			Ball ball = GetBallByIndex (nBallIndex );
			photonView.RPC ("RPC_WoriniGeGui", PhotonTargets.All,  ball.GetPos().x, ball.GetPos().y,nBallIndex, PhotonNetwork.time, fPercent);
		}
	}

	[PunRPC]
	public void RPC_WoriniGeGui( float fExplodePosX, float fExplodePosY, int nBallIndex, double dOccurTime, float fPercent = 0f )
	{
		Ball ball = m_lstBalls[nBallIndex];
		ball.DoExplode ( fExplodePosX, fExplodePosY, dOccurTime, fPercent );
	}

	int m_nCurSkinIndex = 0;
	public void ChangeSkin()
	{
		if (photonView.isMine) {
			m_nCurSkinIndex++;
			if (m_nCurSkinIndex >= Main.s_Instance.GetSpriteArrayLength()) {
				m_nCurSkinIndex = 0;
			}
			photonView.RPC ("RPC_ChangeSkin", PhotonTargets.All, m_nCurSkinIndex);
		}
	}

	[PunRPC]
	public void RPC_ChangeSkin( int nIndex )
	{
		SetSkin( nIndex );
	}

	public void SetSkin( int nIndex )
	{
		Sprite sprite = Main.s_Instance.GetSpriteBySpriteId ( nIndex );
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			ball.SetSprite ( sprite );
		}
	}

	public void DoReborn()
	{
		photonView.RPC ( "RPC_DoReborn", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_DoReborn()
	{
		Ball ball = ReuseOneBall();
		if (ball == null) {
			return;
		}

		vecTempPos = Vector3.zero;
		if ( MapEditor.s_Instance )
		{
			vecTempPos = MapEditor.s_Instance.RandomGetRebornPos();
		}

		BeginProtect ();

		// 重生
		ball.SetSize ( Main.s_Instance.m_fBornMinDiameter );   
		ball.SetPos ( vecTempPos );

		SetRebornCompleted ( true );
	}

	float m_SyncPosAfterEjectTotalTime = 0f;
	public void BeginSyncPosAfterEject( float fTotalTime )
	{
		m_SyncPosAfterEjectTotalTime = fTotalTime;
	}

	void SyncPosAfterEjectCounting()
	{
		if (m_SyncPosAfterEjectTotalTime <= 0f) {
			return;
		}
		m_SyncPosAfterEjectTotalTime -= Time.deltaTime;
		if (m_SyncPosAfterEjectTotalTime <= 0f) {
			EndSyncPosAfterEject ();
		}
	}

	public void EndSyncPosAfterEject( )
	{
		if (photonView.isMine) {
			photonView.RPC ("RPC_EndSyncPosAfterEject", PhotonTargets.All);
		}
	}

	[PunRPC]
	public void RPC_EndSyncPosAfterEject( )
	{
		AdjustMoveInfo ( 1, 1 );
	}

	public void BeginShell ()
	{
		photonView.RPC ( "RPC_BeginShell", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_BeginShell ()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}
			ball.Local_BeginShell ();
		}
	}

	public void BeginPreUnfold()
	{
		photonView.RPC ( "RPC_BeginPreUnfold", PhotonTargets.All);
	}

	[PunRPC]
	public void RPC_BeginPreUnfold()
	{
		for (int i = 0; i < m_lstBalls.Count; i++) {
			Ball ball = m_lstBalls [i];
			if (ball.IsDead ()) {
				continue;
			}

			ball.BeginPreUnfold ();
		}
	}


	// 还是那句话，所有的事件由MainPlayer发起，其余客户端只是被动的遵照执行。以免时序混乱。
	public void ProcessCollideExitGroup(  Ball ball1, Ball ball2  )
	{
		if (!photonView.isMine) {
			return;
		}
   
		ball1.RemoveFromCollideDic ( ball2 );
		ball2.RemoveFromCollideDic ( ball1 );
	}

	public void DoProcessCollideExitGroup(  Ball ball1, Ball ball2  )
	{
		if (!photonView.isMine) {
			return;
		}

		ball1.AddToCollideDic ( ball2 );
		ball2.AddToCollideDic ( ball1 );

		if (ball1.AlreadyEnter (ball2)) { // 因为碰撞的触发模式都是“两次”式。所以要作一个判断，不要重复执行
			ball1.RemoveFromCollideDic (ball2);	
			CGroup group = ball1.GetGroup ();
			if (group && !group.GetDismissed() && ball1.GetCollideBallsNum () == 0) {
				group.RemoveOneBall (ball1);
			}
		}
	}

	// right here
	public void ProcessCollideEnterGroup( Ball ball1, Ball ball2 )
	{
        if (!photonView.isMine) {
			return;
		}
   
		ball1.AddToCollideDic ( ball2 );
		ball2.AddToCollideDic ( ball1 );

		CGroup group1 = ball1.GetGroup ();
		CGroup group2 = ball2.GetGroup ();

		if (group1 != null && group2 != null) {
			if (group1.GetID () == group2.GetID ()) { // 这两个球球已经在同一组了
				return;
			}
			if (group1.GetDismissed () || group2.GetDismissed ()) {
				return;
			}
		
			if (group1.GetBallCount () > group2.GetBallCount ()) {
           //     group1.Local_DrawAllBallsFromAnotherGroup(group2);
                group1.DrawAllBallsFromAnotherGroup (group2);
            } else
            {
            //     group2.Local_DrawAllBallsFromAnotherGroup(group1);
                group2.DrawAllBallsFromAnotherGroup (group1);
            }

		} else if (group1 != null) {
        //    group1.Local_DrawOneBall(ball2);
            group1.DrawOneBall (ball2);
        } else if (group2 != null) {
      //      group2.Local_DrawOneBall(ball1);
            group2.DrawOneBall(ball1);
		} else {
          //  Local_CreateNewGroup(ball1, ball2);
            CreateNewGroup(ball1, ball2);
        }
	}

    public void Local_CreateNewGroup(Ball ball1, Ball ball2)
    {
        if (ball1.IsInGroup() || ball2.IsInGroup())
        { // 不要重复建立
            return;
        }
        CGroup group = GetOneAvailableGroup();
        group.Local_DrawOneBall(ball1);
        group.Local_DrawOneBall(ball2);
    }

    byte[] bytesCreateGroup = new byte[32];
	public void CreateNewGroup( Ball ball1, Ball ball2 )
	{
		if (!photonView.isMine) {
			return;
		}

		if (ball1.IsInGroup () || ball2.IsInGroup ()) { // 不要重复建立
			return;
		} 

		CGroup group = GetOneAvailableGroup ();
			
		int nPointer = 0;
		BitConverter.GetBytes (group.GetID()).CopyTo (bytesCreateGroup, nPointer); // Group ID
		nPointer += sizeof (byte);

		float fGroupPosX = group.GetPos ().x;
		float fGroupPosY = group.GetPos ().y;
		BitConverter.GetBytes (fGroupPosX).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);
		BitConverter.GetBytes (fGroupPosY).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);

		BitConverter.GetBytes (ball1.GetIndex()).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(byte);
		float fPosX = ball1.GetLocalPosition ().x;
		float fPosY = ball1.GetLocalPosition ().y;
		BitConverter.GetBytes (fPosX).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);
		BitConverter.GetBytes (fPosY).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);

		BitConverter.GetBytes (ball2.GetIndex()).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(byte);
		fPosX = ball2.GetLocalPosition ().x;
		fPosY = ball2.GetLocalPosition ().y;
		BitConverter.GetBytes (fPosX).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);
		BitConverter.GetBytes (fPosY).CopyTo( bytesCreateGroup, nPointer );
		nPointer += sizeof(float);



		photonView.RPC ( "RPC_CreateNewGroup", PhotonTargets.AllViaServer, bytesCreateGroup, PhotonNetwork.time );
	}

	[PunRPC]
	public void RPC_CreateNewGroup( byte[] bytes, double dOccurTime )
	{
        if (!photonView.isMine)
        {
            float fDelayTime = (float)(PhotonNetwork.time - dOccurTime);
        }

		int nPointer = 0;
		int nGroupId = (byte)BitConverter.ToChar(bytes, nPointer);
		nPointer += sizeof (byte);

		////创建Group
		if (nGroupId < 0 || nGroupId >= m_GroupManager.GetGroupList().Length) {
			Debug.LogError ( "出Bug了。nGroupId < 0 || nGroupId >= m_lstGroup.Length" );
			return;
		}
		CGroup group = GetGroupByIndex ( nGroupId );
		if (group == null) { 
			group = m_GroupManager.NewGroup ( nGroupId );
		}
		group.SetDismissed ( false );
		/// end 创建Group
		float fGroupPosX = BitConverter.ToSingle (bytes, nPointer);
		nPointer += sizeof(float);
		float fGroupPosY = BitConverter.ToSingle (bytes, nPointer);
		nPointer += sizeof(float);
        group.SetAdjustInfo(fGroupPosX, fGroupPosY, 1);


        for (int i = 0; i < 2; i++) {
			int nIndex = (byte)BitConverter.ToChar (bytes, nPointer);
			nPointer += sizeof(byte);
			if (nIndex < 0 || nIndex >= m_lstBalls.Count) {
				Debug.LogError ("从个的？？nIndex < 0 || nIndex >= m_lstBalls.Count");
				return;
			}
			Ball ball = m_lstBalls [nIndex];
			if (ball.IsDead ()) {
				if (photonView.isMine) {
					Debug.LogError ("从个的？？明明活着怎么是死亡状态11111111111");
				} else {
					Debug.LogError ("从个的？？明明活着怎么是死亡状态22222222222");
				}
			}
			float fPosX = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			float fPosY = BitConverter.ToSingle (bytes, nPointer);
			nPointer += sizeof(float);
			group.Local_DrawOneBall ( ball, true );

		} // end for

		group.SetDismissed ( false );
	}

	public CGroup GetGroupByIndex( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_GroupManager.GetGroupList().Length) {
			return null;
		}
		return m_GroupManager.GetGroupList()[nIndex];
	}

	public CGroup GetOneAvailableGroup()
	{
		CGroup group = null;
		int nIndex = 0;
		CGroup[] lstGroup = m_GroupManager.GetGroupList ();
		for (int i = 0; i < lstGroup.Length; i++) {
			nIndex = i;
			group = lstGroup [i];
			if (group == null) {
				break;
			}

			if ( group.GetDismissed() ) {
				group.SetDismissed ( false );
				break;
			}
		}
		if (group == null) {
			group = m_GroupManager.NewGroup ( nIndex );
		}
		return group;
	}

	public void RemoveOneBallFromGroup( byte nGroupIndex, byte nBallIndex )
	{
		photonView.RPC ( "RPC_RemoveOneBallFromGroup", PhotonTargets.AllViaServer, nGroupIndex, nBallIndex );
	}

	[PunRPC]
	public void RPC_RemoveOneBallFromGroup( byte nGroupIndex, byte nBallIndex )
	{
		CGroup group = GetGroupByIndex ((int)nGroupIndex);
		if (group == null) {
			Debug.LogError ( "从个出Bug了？group == null" );
			return;
		}

		Ball ball = GetBallByIndex ( (int)nBallIndex );
		if (ball == null) {
			Debug.LogError ( "从个出Bug了？ball == null" );
			return;
		}
		ball.SetGroup ( null );
		group.Local_RemoveOneBall ( ball );
	}

	public void DismissGroup( int nGroupIndex )
	{
		photonView.RPC ( "RPC_DismissGroup", PhotonTargets.AllViaServer, nGroupIndex ); 
	}

	[PunRPC]
	public void RPC_DismissGroup( int nGroupIndex )
	{
		CGroup group = GetGroupByIndex (nGroupIndex);
		if (group == null) {
			Debug.LogError ( "从个出Bug了？group == null" );
			return;
		}
		group.Local_Dismiss ();
	}

	byte[] bytesGroupDrawOneBall = new byte[16];
	public void GroupDrawOneBall( int nGroupIndex, Ball ball )
	{
		int nPointer = 0;

		BitConverter.GetBytes (nGroupIndex).CopyTo (bytesGroupDrawOneBall, nPointer); // Group ID
		nPointer += sizeof (byte);
		BitConverter.GetBytes (ball.GetIndex()).CopyTo (bytesGroupDrawOneBall, nPointer); // ball ID
		nPointer += sizeof (byte);
		float fPosX = ball.GetPos ().x;
		float fPosY = ball.GetPos ().y;
		BitConverter.GetBytes (fPosX).CopyTo( bytesGroupDrawOneBall, nPointer );
		nPointer += sizeof(float);
		BitConverter.GetBytes (fPosY).CopyTo( bytesGroupDrawOneBall, nPointer );
		nPointer += sizeof(float);
		photonView.RPC ( "RPC_GroupDrawOneBall", PhotonTargets.AllViaServer, bytesGroupDrawOneBall ); 
	}

	[PunRPC]
	public void RPC_GroupDrawOneBall( byte[] bytes )
	{
		int nPointer = 0;
		int nGroupIndex = (byte)BitConverter.ToChar (bytes, nPointer);
		nPointer += sizeof(byte);
		int nBallIndex = (byte)BitConverter.ToChar (bytes, nPointer);
		nPointer += sizeof(byte);
		float fPosX = BitConverter.ToSingle (bytes, nPointer);
		nPointer += sizeof(float);
		float fPosY = BitConverter.ToSingle (bytes, nPointer);
		nPointer += sizeof(float);
		CGroup group = GetGroupByIndex (nGroupIndex);
		if (group == null) {
			//Debug.LogError ( "从个出Bug了？group == null" );
			return;
		}
		Ball ball = GetBallByIndex ( nBallIndex );
		if (ball == null) {
			Debug.LogError ( "从个出Bug了？ball == null" );
			return;
		}
	
		group.Local_DrawOneBall ( ball );
	}

	public void DrawAllBallsFromAnotherGroup( int nGroupIndex1, int nGroupIndex2 )
	{
		photonView.RPC ( "RPC_DrawAllBallsFromAnotherGroup", PhotonTargets.AllViaServer, nGroupIndex1, nGroupIndex2 ); 
	}

	[PunRPC]
	public void RPC_DrawAllBallsFromAnotherGroup( int nGroupIndex1, int nGroupIndex2 )
	{
		CGroup group1 = GetGroupByIndex (nGroupIndex1);
		if (group1 == null) {
			Debug.LogError ( "从个出Bug了？group == null" );
			return;
		}
		CGroup group2 = GetGroupByIndex (nGroupIndex2);
		if (group2 == null) {
			Debug.LogError ( "从个出Bug了？group == null" );
			return;
		}

		group1.Local_DrawAllBallsFromAnotherGroup ( group2, true );
	}

	public void AddBallToGroup( Ball ball )
	{
		
	}

	public void DestroyThorn( Thorn thorn )
	{
		photonView.RPC ( "RPC_DestroyThorn", PhotonTargets.AllViaServer, thorn.GetIndex() ); 
	}

	[PunRPC]
	public void RPC_DestroyThorn( int nThornIndex )
	{
		Main.s_Instance.DestroyThorn ( nThornIndex );
	}

	public void SyncReArrangeGroupInfo( byte[] bytes )
	{
		photonView.RPC ( "RPC_SyncReArrangeGroupInfo", PhotonTargets.AllViaServer, bytes ); 
	}

	[PunRPC]
	public void RPC_SyncReArrangeGroupInfo( byte[] bytes )
	{
		if (photonView.isMine) {
			return;
		}

		m_GroupManager.AnalyzeReArrangeGroupInfo(bytes );
	}

	public void AddToGroup( Ball ball )
	{
		photonView.RPC ( "RPC_AddToGroup", PhotonTargets.AllViaServer, ball.GetIndex() ); 
	}

	[PunRPC]
	public void RPC_AddToGroup( int nBallIndex )
	{
		Ball ball = GetBallByIndex ( nBallIndex );
		if (ball.IsDead ()) {
			Debug.LogError ( "从个的？？有BUg？ 球怎么是死的 " );
		}

		m_Group.AddOneBall ( ball );
	}

} // end Player
