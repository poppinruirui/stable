﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;
public class StringManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static bool CheckPolygonNameValid( string val )
	{
		if (val.Length == 0) {
			return false;
		}
		return true;
	}

	public static XmlDocument CreateXmlByText( string szXmlContent, ref XmlNode root )
	{
		XmlDocument myXmlDoc = new XmlDocument();
		myXmlDoc.LoadXml ( szXmlContent );
		root = myXmlDoc.SelectSingleNode("root");
		return myXmlDoc;
	}

	public static byte[] String2Bytes( string str )
	{
		return System.Text.Encoding.Unicode.GetBytes ( str );
	}

	public static string Bytes2String( byte[] bytes, int nStartIndex, int nLength )
	{
		return System.Text.Encoding.Unicode.GetString (bytes, nStartIndex, nLength);
	}

    static byte[] _bytes = null;
    static int _pointer = 0;
    public static void BeginPushData(byte[] bytes)
    {
        _bytes = bytes;
        _pointer = 0;
    }

    public static int GetCurPointerPos()
    {
        return _pointer;
    }

    public static void SetCurPointerPos(int pointer)
    {
        _pointer = pointer;
    }

    public static void BeginPopData(byte[] bytes)
    {
        _bytes = bytes;
        _pointer = 0;
    }

    public static void PushData_Byte( byte val )
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(byte);
    }

    public static byte PopData_Byte()
    {
        byte val = (byte)BitConverter.ToChar(_bytes, _pointer);
        _pointer += sizeof(byte);
        return val;
    }
 
    public static void PushData_Int(int val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(int);
    }

    public static int PopData_Int()
    {
        int val = BitConverter.ToInt32(_bytes, _pointer);
        _pointer += sizeof(int);
        return val;
    }
    
    public static void PushData_Float( float val)
    {
        BitConverter.GetBytes(val).CopyTo(_bytes, _pointer);
        _pointer += sizeof(float);
    }

    public static  float PopData_Float()
    {
        float val = BitConverter.ToSingle(_bytes, _pointer);
        _pointer += sizeof(float);
        return val;
    }
}
