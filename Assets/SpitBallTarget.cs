﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpitBallTarget : MonoBehaviour {

	Ball m_ball = null;
	public SpriteRenderer m_sr = null;
	float m_fTotalTime = 0f;
	bool m_bCouting = false;
	static Vector3 vecTempPos = new Vector3 ();
	static Vector3 vecTempScale = new Vector3 ();

	// Use this for initialization
	void Awake () {
		
		m_sr = this.gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (m_ball != null) {
			vecTempPos = m_vecPos;
			vecTempPos.x /= m_ball.GetSize();
			vecTempPos.y /= m_ball.GetSize ();
			float fSize = m_fSize / m_ball.GetSize ();
			vecTempScale.x = fSize;
			vecTempScale.y = fSize;
			vecTempScale.z = 1f;
			this.transform.localPosition = vecTempPos;
			this.transform.localScale = vecTempScale;
		}
		*/
	}

	public void SetVisible (bool val)
	{
		this.gameObject.SetActive ( val );
	}

	public void SetBall( Ball ball )
	{
		m_ball = ball;
	}

	public float GetBoundsSize()
	{
		return m_sr.bounds.size.x;
	}

	Vector3 m_vecPos = new Vector3();
	public void SetPos( Vector3 pos )
	{
		this.transform.position = pos;
	}

	float m_fSize = 0f;
	public void SetSize( float size )
	{
		m_fSize = size;
		vecTempScale.x = m_fSize;
		vecTempScale.y = m_fSize;
		vecTempScale.z = 1f;
		this.transform.localScale = vecTempScale;
	}
}
