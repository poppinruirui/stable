﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using SonicBloom.Koreo;

public class BallAction : MonoBehaviour
{
    public const string KOREO_EVENT_TRACK_EXPLOSION_BEAT = "TRACK_EXPLOSION_BEAT";
        
    public float _total_scale_min = 0.1f;
    public float _total_scale_max = 50.0f;
    public float _total_scale_ratio = 0.0f;
    public float _total_scale_inpoisonable_ratio = 0.3f;
    
    public float _poison_scale_min = 0.0f;
    public float _poison_scale_max = 1.0f;
    public float _poison_scale_inc = 0.0f;    
    public float _poison_scale_inc_delta = 0.1f;

	public Vector2 _direction = new Vector2( -0.5f, -0.5f );
	public float _ball_realtime_velocity = 0.0f;

    int _color_index = -1;

  //  RandomMovment _random_movement = new RandomMovment();

    public float _velocity = 0.2f;    
    const float _dir_space = 1.5f;
    
    public GameObject _dir_indicator;
    public LineRenderer _dir_line;

	Ball _ball = null;

    Vector3 vecTempPos1 = new Vector3();
    Vector3 vecTempPos2 = new Vector3();

    void Awake()
	{
		_ball = this.gameObject.GetComponent<Ball> ();
	}

	void Start()
    {
     //   this.RandomColor();
     //   this.RandomScale(true);

        this._total_scale_ratio = (this.transform.localScale.x / this._total_scale_max);
   //     this._poison_scale_inc_delta /= this._total_scale_ratio;
        
    //    this._random_movement._transform = this.transform;        
    //    this._random_movement._move_velocity /= this._total_scale_ratio;
    //    this.RandomPoisonFill(true);
   //     this._random_movement.RandomMove(true, null);
   //     Koreographer.Instance.RegisterForEvents(KOREO_EVENT_TRACK_EXPLOSION_BEAT,
   //                                             this.OnKoreoEventTrackExplosionBeat);
	}

    void OnDestroy()
    {
 //       if (Koreographer.Instance != null) {
 //           Koreographer.Instance.UnregisterForAllEvents(this);
 //       }
    }    
	
	void Update()
    {
	}

	void FixedUpdate()
    {
        if (false/*CtrlMode.GetCtrlMode() == CtrlMode.CTRL_MODE_NONE*/) {
          //  this._random_movement.RandomMove(false, null);
        }
        if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_NONE) {
            this._dir_line.enabled = false;
            this._dir_indicator.GetComponent<Renderer>().enabled = false;            
        }
        //this.RandomScale(false);
        //this.RandomPoisonFill(false);
	}

	// 这里要好好优化一下，太多无谓的运算了！！！！！！ poppin to youhua
    void UpdateDirIndicator(Vector3 cursor_position, Vector2 direction)
    {
      if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_ARROW) {
			if (_dir_line) {
				this._dir_line.enabled = false;
			}
            this._dir_indicator.GetComponent<Renderer>().enabled = true;
            this._dir_indicator.transform.localPosition =
                new Vector3(direction.x * BallAction._dir_space,
                            direction.y * BallAction._dir_space,
                            this._dir_indicator.transform.localPosition.z);
            this._dir_indicator.transform.rotation = Quaternion.FromToRotation(Vector2.right, direction);
        }
        else if (CtrlMode.GetDirIndicatorType() == CtrlMode.DIR_INDICATOR_LINE) {
			_dir_line.gameObject.SetActive ( true);
            this._dir_line.enabled = true;
            this._dir_indicator.GetComponent<Renderer>().enabled = false;
            this._dir_line.SetPosition(0, new Vector3(cursor_position.x,
                                                      cursor_position.y,
                                                      this._dir_line.transform.position.z));
            this._dir_line.SetPosition(1, new Vector3(/*this.transform.position.x,
                                                      this.transform.position.y,*/ _ball.transform.position.x, _ball.transform.position.y,
                                                      this._dir_line.transform.position.z));
			//this._dir_indicator.GetComponent<Renderer>().enabled = false;
        }
        else {
            this._dir_line.enabled = false;
            this._dir_indicator.GetComponent<Renderer>().enabled = false;
        }
    }

	/* 关于“壳”的规则
	规则1：有壳状态不要乱入
	规则2：已经乱入的情况下（炸球、R键分球），按固定的规则弹开，这个规则必须没有二义性
	*/
	public float m_fRiNiGeGuiSpeed = 0f;
	static Vector2 s_pos1 = new Vector2();
	static Vector2 s_pos2 = new Vector2();
	public void UpdateDir ( Vector3 cursor_position )
	{
		_direction = cursor_position - this.transform.position;
		_direction.Normalize();
		_ball.SetDir (_direction)  ;
	}



	public void UpdatePosition_InGroup(Vector3 cursor_position, float ball_velocity )
	{
        CGroup group = _ball.GetGroup ();
		if ( group == null )
		{
			Debug.LogError ( "有bug！！group == null" );
			return;
		}

		_direction = cursor_position - this.transform.position;
		_direction.Normalize();

        Vector2 group_speed = group.GetSpeed();

		_ball._dirx = _direction.x;
		_ball._diry = _direction.y;

		float fRealSpeed = GetRealSpeedBySize ( ball_velocity );
        float fRealSpeedX = fRealSpeed * _direction.x - group_speed.x;
        float fRealSpeedY = fRealSpeed * _direction.y - group_speed.y;
        

		fRealSpeedX = CGroup.AdjustInGroupLocalSpeed ( fRealSpeedX );
		fRealSpeedY = CGroup.AdjustInGroupLocalSpeed ( fRealSpeedY );

		vecTempPos1 = _ball.GetLocalPosition();

		float fDeltaX = fRealSpeedX * Time.fixedDeltaTime;
		float fDeltaY = fRealSpeedY * Time.fixedDeltaTime;

		//// 检测是否超越了场景边界
		bool bCanMoveX = true;
		bool bCanMoveY = true;
		MapEditor.s_Instance.CheckIfWillExceedWorldBorder ( _ball, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
		if ( ( !bCanMoveX ) && ( !bCanMoveY ) ) {

		}
		///  end 检测是否超越了场景边界
		if (bCanMoveX) {
			vecTempPos1.x += fDeltaX;
		}
		if (bCanMoveY) {
			vecTempPos1.y += fDeltaY;
		}

		vecTempPos1.z = -_ball.GetSize ();
		_ball.SetLocalPosition (vecTempPos1);

		this.UpdateDirIndicator(cursor_position, _direction);
	}

	public float GetRealSpeedBySize( float ball_velocity_without_size )
	{
		float fShitSize = 0f;
		if (Main.s_Instance.m_nShit == 2) {
			fShitSize = _ball.GetSize2KaiGenHao ();
		} else if (Main.s_Instance.m_nShit == 3) {
			fShitSize = _ball.GetSize3KaiGenHao ();
		}

		return ( ball_velocity_without_size / fShitSize );
	}

	public void UpdatePosition(Vector3 cursor_position, float ball_velocity )
	{
		_direction = cursor_position - this.transform.position;
		_direction.Normalize();

        _ball_realtime_velocity = GetRealSpeedBySize ( ball_velocity );

		_ball._dirx = _direction.x;
		_ball._diry = _direction.y;

		float fDeltaX = _direction.x * Time.fixedDeltaTime * _ball_realtime_velocity;
		float fDeltaY = _direction.y * Time.fixedDeltaTime * _ball_realtime_velocity;

		vecTempPos1 = _ball.GetPos();

		float fExpectedX = vecTempPos1.x + fDeltaX;
		float fExpectedY = vecTempPos1.y + fDeltaY;
		List<Ball> lst = _ball._Player.GetBallList ();
		bool bCanMoveX = true;
		bool bCanMoveY = true;

		float fRealNextX = 0f;
		float fRealNextY = 0f;
		MapEditor.s_Instance.ClampMoveToWorldBorder ( _ball.GetRadius(), vecTempPos1.x, vecTempPos1.y, fDeltaX, fDeltaY, ref fRealNextX, ref fRealNextY);

		vecTempPos1.x = fRealNextX;
		vecTempPos1.y = fRealNextY;

		if ( ( !_ball.DoNotMove() ) ) {
			vecTempPos1.z = -_ball.GetSize ();
			_ball.SetPos (vecTempPos1);
		}

		this.UpdateDirIndicator(cursor_position, _direction);
   }  

	bool IsCanUpdatePos( Ball ball )
	{
		if (ball.IsCaptured ()) {
			return false;
		}

		if (ball.IsEjecting()) {
			return false;
		}

		if (ball.IsStaying ()) {
			return false;
		}

		return true;
	}

	public float GetRealTimeVelocity()
	{
		return /*Main.s_Instance.m_fBallBaseSpeed*/_ball_realtime_velocity / _ball.GetSize();
	}
    
    void RandomColor()
    {
		return;

        this._color_index = 0;
        GameObject outer_ring = this.transform.Find("OuterRing").gameObject;
        outer_ring.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomOuterRingColor(ref this._color_index);
        GameObject inner_fill = this.transform.Find("OuterRing/InnerFill").gameObject;
        inner_fill.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomInnerFillColor(this._color_index);
        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;
        poison_fill.GetComponent<SpriteRenderer>().color =
            ColorPalette.RandomPoisonFillColor(this._color_index);
        Gradient gradient = new Gradient();
        gradient.SetKeys(new GradientColorKey[] {
                new GradientColorKey(ColorPalette.RandomPoisonFillColor(this._color_index), 0.0f),
                new GradientColorKey(Color.white, 1.0f)
            },
            new GradientAlphaKey[] {
                new GradientAlphaKey(0.0f, 0.0f),
                new GradientAlphaKey(1.0f, 1.0f)
            });
        this._dir_line.colorGradient = gradient;
    }

    void RandomPoisonFill(bool immediately)
    {
		return;

        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;        
        float poison_scale = 0.0f;
        float poison_scale_z = poison_fill.GetComponent<Transform>().localScale.z;
        if (immediately) {
            poison_scale = Random.Range(this._poison_scale_min,
                                        this._poison_scale_max);
        }
        else {            
            poison_scale = Mathf.Lerp(this._poison_scale_min,
                                      this._poison_scale_max,
                                      this._poison_scale_inc);
            this._poison_scale_inc += this._poison_scale_inc_delta * Time.fixedDeltaTime;
            if (this._poison_scale_inc >= this._poison_scale_max) {
                float scale_tmp = this._poison_scale_min;
                this._poison_scale_min = this._poison_scale_max;
                this._poison_scale_max = scale_tmp;
                this._poison_scale_inc = 0.0f;
            }
        }
        if (this._total_scale_ratio < this._total_scale_inpoisonable_ratio) {
            poison_scale = 0.0f;
        }
        poison_fill.GetComponent<Transform>().localScale =
            new Vector3(poison_scale, poison_scale, poison_scale_z);
    }
    
    void RandomScale(bool immediately)
    {
        if (immediately) {
            GameObject outer_ring = this.gameObject;
            float total_scale = Random.Range(this._total_scale_min,
                                             this._total_scale_max);
            float total_scale_z = outer_ring.GetComponent<Transform>().localScale.z;
            outer_ring.GetComponent<Transform>().localScale = new Vector3(total_scale, total_scale, total_scale_z);
        }
    }

	public void SetDead( bool bDead )
	{
		if (bDead) {
			_dir_line.gameObject.SetActive ( false );
		} else {
			_dir_line.gameObject.SetActive ( true );
		}
	}
	/*
    void OnKoreoEventTrackExplosionBeat(KoreographyEvent koreoEvent)
    {
        int value = koreoEvent.GetIntValue();
        GameObject inner_fill = this.transform.Find("OuterRing/InnerFill").gameObject;
        GameObject poison_fill = this.transform.Find("OuterRing/InnerFill/PoisonFill").gameObject;
        if (value > 0) {
            inner_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomInnerFillColor(this._color_index);
            poison_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomPoisonFillColor(this._color_index);            
        }
        else if (value == 0) {
            inner_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomPoisonFillColor(this._color_index);
            poison_fill.GetComponent<SpriteRenderer>().color =
                ColorPalette.RandomInnerFillColor(this._color_index);
        }
    } 
	*/
}
