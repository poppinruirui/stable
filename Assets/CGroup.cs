﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGroup : MonoBehaviour {

	public const int INVALID_GROUP_ID = 127;

	static Vector3 vecTempPos = new Vector3();
	static Vector3 vecTempPos2 = new Vector3();

	public Player m_Player = null;

	int m_nId = INVALID_GROUP_ID;
	Dictionary<int, Ball> m_dicBalls = new Dictionary<int, Ball>();
	Vector2 m_Direction = new Vector2();
	public Vector2 m_vecLastDirection = new Vector2();
	public Vector2 m_Speed = new Vector2 ();
	public Vector2 m_SpeedAdjust = new Vector2 ();
	public Vector3 m_LogicPos = new Vector3();
	public float m_fSize = 1f;

	float c_fCalculateAverageInfoInterval = 0.5f; // 每1000毫秒运算一次平均信息（Group的位置、大小就是用该组的球球的值的平均值）
	float m_fCalculateAverageInfoTimeCount = 0f;

	bool m_bDismissed = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	List<Ball> m_lstRongCuoBallsToRemove = new List<Ball>();
	void RongCuo_RemoveBalls ()
	{
		if (!m_Player.photonView.isMine) {
			return;
		}

		m_lstRongCuoBallsToRemove.Clear ();
		foreach (Transform child in this.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();
			m_lstRongCuoBallsToRemove.Add (ball); 
		}

		for (int i = 0; i < m_lstRongCuoBallsToRemove.Count; i++) {
			Ball ball = m_lstRongCuoBallsToRemove [i];
			//RemoveOneBall ( ball );
		}

	}

	void FixedUpdate()
	{
		if (GetDismissed ()) {
			return;
		}

		if (this.transform.childCount == 0) {
			return;
		}

		Move();
	}

	public void SetPlayer( Player player )
	{
		m_Player = player;
		this.transform.parent = player.transform;
	}

	public Player GetPlayer()
	{
		return m_Player;
	}


	// 吸纳一个球球进小组
	public void DrawOneBall( Ball ball )
	{
		if (m_Player == null) {
			Debug.LogError ( "从个的？m_Player == null" );
			return;
		}

		m_Player.GroupDrawOneBall ( GetID(), ball );
	}

	public void Local_DrawOneBall( Ball ball, bool bFromMainPlayer = false )
	{
		// 如果该球球目前隶属其它Group，则先移除
		CGroup group = ball.GetGroup ();
		if (group) {
			group.Local_RemoveOneBall ( ball );
		}

        ball.SetGroup ( this );
        if (bFromMainPlayer)
        {
            ball.GroupCount( m_nId );
        }

        ball.transform.parent = this.transform;
		OnMemberChanged ();
    }

	// 球球死亡或壳消失，应该会自动触发ExitCollide事件（这点只是猜测，稍候实测）
	public void Local_RemoveOneBall( Ball ball )
	{
		ball.SetGroup ( null );
		//m_dicBalls.Remove ( ball.GetIndex() );
		ball.transform.parent = ball._Player.transform;
		OnMemberChanged ();
		CheckIsDismiss ();
	}


	void CheckIsDismiss()
	{
		if (this.transform.childCount <= 0) {
			SetDismissed (true);
		} 
	}

	public void RemoveOneBall( Ball ball )
	{
		if ( m_Player == null) {
			Debug.LogError ( "从个出Bug了？m_Player == null" );
			return;
		}

		m_Player.RemoveOneBallFromGroup( (byte)GetID(), (byte)ball.GetIndex() );
	}

	// 合并另一个组
	List<Ball> m_lstMergeGroupTemp = new List<Ball>();
	public void DrawAllBallsFromAnotherGroup( CGroup group )
	{
		m_Player.DrawAllBallsFromAnotherGroup ( this.GetID(), group.GetID() );
	}

	public void Local_DrawAllBallsFromAnotherGroup( CGroup group, bool bFromMainPlayer = false )
	{
		m_lstMergeGroupTemp.Clear ();
		foreach ( Transform child in group.transform ) {
			Ball ball = child.GetComponent<Ball> ();
			m_lstMergeGroupTemp.Add( ball );
		}
		for (int i = 0; i < m_lstMergeGroupTemp.Count; i++) {
			Local_DrawOneBall(m_lstMergeGroupTemp[i], bFromMainPlayer);
		}
	}


	public int GetID()
	{
		return m_nId;
	}

	public void SetID( int nId )
	{
		m_nId = nId;
		this.gameObject.name = "group_" + m_nId;
	}

	public Dictionary<int, Ball> GetDicBalls()
	{
		return m_dicBalls;
	}

	public int GetBallsNum()
	{
		return this.transform.childCount/*m_dicBalls.Count*/;
	}

	public void SetPos( Vector3 pos )
	{
		this.transform.position = pos;
	}

	public Vector3 GetPos()
	{
		return this.transform.position;
	}

	public void UpdatePosition(Vector3 cursor_position, float ball_velocity)
	{
		if (GetDismissed ()) {
			return;
		}

		m_Direction = cursor_position - GetLogicPos();
		m_Direction.Normalize ();
		if (m_fSize <= 0f) {
			return;
		}

        float fSpeed = ( ball_velocity ) / m_fSize;
		m_Speed.x = m_Direction.x * fSpeed;
		m_Speed.y = m_Direction.y * fSpeed;
		float fDeltaX = m_Speed.x * Time.fixedDeltaTime;
		float fDeltaY = m_Speed.y * Time.fixedDeltaTime;

		// 检测是否超过地图边界
		bool bCanMoveX = true;
		bool bCanMoveY = true;
		foreach (Transform child in this.transform) { // poppin to youhua 说过无数次了不准高频执行这类操作。找时间把这个环节优化掉。
			Ball ball = child.gameObject.GetComponent<Ball>();
			MapEditor.s_Instance.CheckIfWillExceedWorldBorder (ball, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
			if ( ( !bCanMoveX ) && ( !bCanMoveY ) ) {
				break;
			}
		}
		// end 检测是否超过地图边界
		vecTempPos = GetPos();
		if (bCanMoveX) {
			vecTempPos.x += fDeltaX;
		}
		if (bCanMoveY) {
			vecTempPos.y += fDeltaY;
		}
		SetPos ( vecTempPos );

	}

	public Vector2 GetDirection()
	{
		return m_Direction;
	}

	public Vector2 GetSpeed()
	{
		return m_Speed;
	}
	/*
	void CalculateAverageInfo()
	{
		if (GetDismissed ()) {
			return;
		}
	
		m_fCalculateAverageInfoTimeCount += Time.deltaTime;
		if (m_fCalculateAverageInfoTimeCount < c_fCalculateAverageInfoInterval) {
			return;
		}
		m_fCalculateAverageInfoTimeCount = 0f;

		// 计算出这个组的中心点，即该组所有球球的position的平均值
		vecTempPos.x = vecTempPos.y = 0f;
		int nNum = 0;
		m_fSize = 0f;
		foreach (KeyValuePair<int, Ball> pair in m_dicBalls) { 
			Ball ball = pair.Value;
			if (ball.IsDead ()) {
				Debug.LogError ( "从个的，已经死了还在组里面" );
				continue;
			}

			if (ball.GetGroup () == null) {
				Debug.LogError ("从个的，这个球不再任何组里面");
				ball._srMouth.color = Color.green;
				continue;
			} else {
				ball._srMouth.color = Color.white;
			}

			if (  ball.GetGroup ().GetID () != m_nId) {
				Debug.LogError ( "从个的，这个球居然不在这个组里面" );
				ball._srMouth.color = Color.blue;
				continue;
			}else {
				ball._srMouth.color = Color.white;
			}

 		   if (Main.s_Instance.m_nShit == 2) {
				m_fSize += ball.GetSize2KaiGenHao ();
			} else if (Main.s_Instance.m_nShit == 3) {
				m_fSize += ball.GetSize3KaiGenHao ();
			}

			vecTempPos.x += ball.GetPos ().x;
			vecTempPos.y += ball.GetPos ().y;
		
			nNum++;
		} // end for
		if (nNum == 0) {
			return;
		}
		m_fSize /= nNum;
		vecTempPos.x /= nNum; 
		vecTempPos.y /= nNum;
		SetLogicPos ( vecTempPos );
	}
*/
	void CalculateAverageInfo()
	{
		if (GetDismissed ()) {
			return;
		}

		// 计算出这个组的中心点，即该组所有球球的position的平均值
		vecTempPos.x = vecTempPos.y = 0f;
		int nNum = 0;
		m_fSize = 0f;
		foreach( Transform child in this.transform ){
			Ball ball = child.GetComponent<Ball>(); // poppin to youhua 不能在高频操作中做GetComponent。
			if (ball.IsDead ()) {
				//Debug.LogError ( "从个的，已经死了还在组里面" );
				continue;
			}

			if (ball.GetGroup () == null) {
				Debug.LogError ("从个的，这个球不再任何组里面");
				ball._srMouth.color = Color.green;
				continue;
			} else {
				ball._srMouth.color = Color.white;
			}

			if (  ball.GetGroup ().GetID () != m_nId) {
				Debug.LogError ( "从个的，这个球居然不在这个组里面" );
				ball._srMouth.color = Color.blue;
				continue;
			}else {
				ball._srMouth.color = Color.white;
			}

			if (Main.s_Instance.m_nShit == 2) {
				m_fSize += ball.GetSize2KaiGenHao ();
			} else if (Main.s_Instance.m_nShit == 3) {
				m_fSize += ball.GetSize3KaiGenHao ();
			}

			vecTempPos.x += ball.GetPos ().x;
			vecTempPos.y += ball.GetPos ().y;

			nNum++;
		} // end for
		if (nNum == 0) {
			//Debug.LogError ( "从个的，nNum == 0" );
			return;
		}
		m_fSize /= nNum;
		vecTempPos.x /= nNum; 
		vecTempPos.y /= nNum;
		SetLogicPos ( vecTempPos );
	}

	public void SetLogicPos ( Vector3 pos )
	{
		m_LogicPos = pos;
	}

	public Vector3 GetLogicPos ()
	{
		return m_LogicPos;
	}

	void OnMemberChanged()
	{
		CalculateAverageInfo ();
	}

	public bool GetDismissed()
	{
		return m_bDismissed;
	}

	public void Local_Dismiss ()
	{
		m_bDismissed = true;
		if (m_bDismissed == true && this.transform.childCount > 0) {
			int shit = 123;
		}
		foreach (Transform child in this.transform) { 
			Ball ball = child.GetComponent<Ball> ();
			ball.SetGroup (null);
			ball.transform.parent = ball._Player.transform;
		}
	}
	/*
	public void Local_SetDismissed ( bool val )
	{
		m_bDismissed = val;
		if (m_bDismissed) {
			foreach (KeyValuePair<int, Ball> pair in m_dicBalls) { 
				Ball ball = pair.Value;
				ball.SetGroup (null);
				ball.transform.parent = ball._Player.transform;
			}
			m_dicBalls.Clear ();
		}
	}
	*/
	public void SetDismissed( bool val )
	{
		m_bDismissed = val;
	}

	public void BeginMove( Vector3 vecWorldCursorPos, float fSpeedWithoutSize )
	{
		m_Direction = vecWorldCursorPos - GetLogicPos();
		m_Direction.Normalize();

        if (m_fSize <= 0f) {
			//Debug.LogError ( "22222222" );
			return;
		}

		float fRealTimeSpeed = fSpeedWithoutSize / m_fSize;
		m_Speed.x = fRealTimeSpeed * m_Direction.x + m_SpeedAdjust.x;
		m_Speed.y = fRealTimeSpeed * m_Direction.y + m_SpeedAdjust.y;
	}

	void Move()
	{
		if (m_Player.photonView.isMine) {
			return;
		}

		if (!m_Player.IsMoving()) {
			return;
		}

		if (GetDismissed ()) {
			return;
		}

		vecTempPos = GetPos ();
		float fDeltaX = m_Speed.x * Time.fixedDeltaTime;
		float fDeltaY = m_Speed.y * Time.fixedDeltaTime;

		// 检测是否超过地图边界
		bool bCanMoveX = true;
		bool bCanMoveY = true;
		/*
		foreach (Transform child in this.transform) { // poppin to youhua 说过无数次了不准高频执行这类操作。找时间把这个环节优化掉。
			Ball ball = child.gameObject.GetComponent<Ball>();
			MapEditor.s_Instance.CheckIfWillExceedWorldBorder (ball, fDeltaX, fDeltaY, ref bCanMoveX, ref bCanMoveY);
			if ( ( !bCanMoveX ) && ( !bCanMoveY ) ) {
				break;
			}
		}
		*/
		if (bCanMoveX) {
			vecTempPos.x += fDeltaX;
		}
		if (bCanMoveY) {
			vecTempPos.y += fDeltaY;
		}
	
		SetPos (vecTempPos  );

	}


	public void SetAdjustInfo( float fPosX, float fPosY, int nDirect = 0 )
	{
		vecTempPos = GetPos ();

		float fDeltaX = fPosX - vecTempPos.x;
		float fDeltaY = fPosY - vecTempPos.y;

		if (nDirect == 0) {
			m_SpeedAdjust.x = fDeltaX;
			m_SpeedAdjust.y = fDeltaY;
		} else if (nDirect == 1) {
			vecTempPos.x = fPosX;
			vecTempPos.y = fPosY;
			SetPos ( vecTempPos );
		}
		
		m_vecLastDirection = m_Direction;
	}

	public int GetBallCount()
	{
		return /*m_dicBalls.Count*/this.transform.childCount ;
	}

	public void AddOneBall( Ball ball )
	{
		ball.SetGroup ( this );
		ball.transform.parent = this.transform;
		OnMemberChanged ();
	}

	public static float AdjustInGroupLocalSpeed( float fSpeed )
	{
		//fSpeed = fSpeed > 0 ? Mathf.Sqrt( fSpeed ) : ( -Mathf.Sqrt( -fSpeed ) );
		return fSpeed;
	}
}
