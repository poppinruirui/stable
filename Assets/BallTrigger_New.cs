﻿/*
 * 这个文件中的代码可以好好重构一次，太乱了。性能很低下
 * 
 * */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger_New : MonoBehaviour {

	public Ball _ball;
	public Bean _bean;
	public Thorn _thorn;
	public eTriggerType _type;

	public enum eTriggerType
	{
		ball,
		bean,
		thorn,
		soft_obstacle,
		hard_obstacle,
	};

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// 接触

	void OnTriggerEnter2D(Collider2D other)
	{
		if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game )
		{
			return;
		}

		if ( this._type != eTriggerType.ball ) {
			return;
		}
		if (this._ball.IsDead()) {
			return;
		}

		if (other.transform.gameObject.tag == "thorn") {
			Thorn thorn = other.transform.gameObject.GetComponent<Thorn> ();
			if (thorn) {
				//Debug.Log ( "吃刺：" + thorn.transform.position );
				this._ball.EatThorn (thorn);
			}
		} else if (other.transform.gameObject.tag == "bean") {
			Bean bean = other.transform.gameObject.GetComponent<Bean> ();
			if (bean) {
				this._ball.EatBean (bean);
			}
		} 
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
		{
			return;
		}

		if ( this._type != eTriggerType.ball ) {
			return;
		}
		if (this._ball.IsDead()) {
			return;
		}

		if (other.transform.gameObject.tag == "Mouth") {
			Ball ballOpponent = other.transform.parent.gameObject.GetComponent<Ball> ();
			if (ballOpponent) {
				//this._ball._Player.ProcessCollideEnterGroup ( this._ball, ballOpponent ); 
				this._ball.ProcessPk ( ballOpponent );
			}
		}
		else if (other.transform.gameObject.tag == "BeanSpray_Main") {
			Spray spray = other.transform.parent.gameObject.GetComponent<Spray> ();
			if (spray) {
				spray.GenerateMeatBean ( this._ball );
			}
			//this._ball._Player.DestroyBall ( this._ball.GetIndex() );
		}
	}




	void OnTriggerExit2D(Collider2D other)
	{
		if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
		{
			return;
		}

		/*
		if (other.transform.gameObject.tag == "Mouth") { 
			Ball ball = other.transform.parent.gameObject.GetComponent<Ball>();
			if ( this._ball && ball) {
				this._ball.RemoveFromCollideDic ( ball );
			}
		}
		*/

	}
}
