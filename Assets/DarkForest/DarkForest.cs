﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DarkForest : MonoBehaviour {

    public SpriteRenderer _NightRenderer;
    const int c_nMaskTotal = 15; // 最大支持15层混合，超过了shader就会报错。


    public Texture2D[] _TestMask = new Texture2D[c_nMaskTotal];

    List<Ball> m_lstBalls = new List<Ball>();

    Vector2 vecTempPos = new Vector2();
    public Vector2 vecTempScale = new Vector2();

    public static DarkForest s_Instance = null;

    bool m_bIsOn = true;

    float m_BallSizeSortCount = 0.0f;

    void Awake()
    {
        s_Instance = this;

        for (int i = 0; i < c_nMaskTotal; i++)
        {
            _NightRenderer.material.SetTexture("_Mask" + i, _TestMask[i]);
            vecTempPos.x = 0.5f;
            vecTempPos.y = 0.5f;
            vecTempScale.x = 10000.0f;
            vecTempScale.y = 10000.0f;
            _NightRenderer.material.SetTextureOffset("_Mask" + i, new Vector2((1.0f - vecTempScale.x) / 2f - vecTempPos.x * vecTempScale.x, (1.0f - vecTempScale.y) / 2.0f - vecTempPos.y * vecTempScale.y));
            _NightRenderer.material.SetTextureScale("_Mask" + i, new Vector2(vecTempScale.x, vecTempScale.y));

        }

		this.gameObject.SetActive ( false );
    }

    // Use this for initialization
    void Start () {
		
	}
    public float fX = 0.6f;
    public float fY = 0.6f;
    // Update is called once per frame
    void Update () {

        if ( AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game || (!m_bIsOn) )
        {
            return;
        }

        m_BallSizeSortCount += Time.fixedDeltaTime;
        if (m_BallSizeSortCount > 5.0f)
        {
            SortByBallSize();
            m_BallSizeSortCount = 0.0f;
        }

        int idx = 0;
        for ( int i = m_lstBalls.Count - 1; i >= 0; i-- )
        {
            Ball ball = m_lstBalls[i];
            bool bDestroyed = false;
            if ( ball == null )
            {
                m_lstBalls.RemoveAt( i );
                bDestroyed = true;
            }



            if (idx > c_nMaskTotal || bDestroyed || idx >= m_lstBalls.Count )
            {
                vecTempScale.x = 10000.0f;
                vecTempScale.y = 10000.0f;
            }
            else
            {
                vecTempPos.x = ball.transform.position.x / MapEditor.s_Instance.GetWorldWidth();
                vecTempPos.y = ball.transform.position.y / MapEditor.s_Instance.GetWorldHeight();

                // 算出来的SetTextureScale的scale取值范围是0 ~ 1，不能超过1，超过1之后会有诡异现象
                float fK = 0.5f / ball.GetSize();

                vecTempScale.x = fK * this.transform.localScale.x / 10.0f;
                if (vecTempScale.x > 0.9f)
                {
                    vecTempScale.x = 0.9f;
                }
                vecTempScale.y = fK * this.transform.localScale.y / 10.0f;
                if (vecTempScale.y > 0.9f)
                {
                    vecTempScale.y = 0.9f;
                }

            }
            float offsetX = (1.0f - vecTempScale.x) / 2f - vecTempPos.x * vecTempScale.x;
            float offsetY = (1.0f - vecTempScale.y) / 2.0f - vecTempPos.y * vecTempScale.y;
            _NightRenderer.material.SetTextureOffset("_Mask" + idx, new Vector2(offsetX, offsetY));
            _NightRenderer.material.SetTextureScale("_Mask" + idx, new Vector2(vecTempScale.x, vecTempScale.y));
            idx++;
        }

        /*
        for (int i = 0; i <= c_nMaskTotal - 1; i++)
        {
            _NightRenderer.material.SetTextureOffset("_Mask" + i, new Vector2((1f - aryTexScale[i].x) / 2f - aryTexOffset[i].x * aryTexScale[i].x, (1f - aryTexScale[i].y) / 2f - aryTexOffset[i].y * aryTexScale[i].y));
            _NightRenderer.material.SetTextureScale("_Mask" + i, new Vector2(aryTexScale[i].x, aryTexScale[i].y));
        }
        */
    }

    public void AddBall( Ball ball )
    {
        m_lstBalls.Add( ball );
        SortByBallSize();
    }


    public void ToggleDarkForestMode( bool isOn )
    {
        m_bIsOn = isOn;

        if (m_bIsOn)
        {
            this.gameObject.SetActive(true);
        }
        else
        {
            this.gameObject.SetActive( false );
        }
    }

    void SortByBallSize()
    {
        for ( int i = 0; i < m_lstBalls.Count - 1; i++ )
        {
            for ( int j = i + 1; j < m_lstBalls.Count; j++ )
            {
                Ball ball1 = m_lstBalls[i];
                Ball ball2 = m_lstBalls[j];
                if ( ball1 == null || ball2 == null )
                {
                    continue;
                }
                
                if ( ball1.GetSize() > ball2.GetSize() )
                {
                    m_lstBalls[i] = ball2;
                    m_lstBalls[j] = ball1;
                }
            }
        }
    }
}
