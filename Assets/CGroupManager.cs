﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System;

public class CGroupManager : MonoBehaviour {

	public Player m_Player = null;
	CGroup[] m_lstGroups = new CGroup[32];

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        SyncReArrangeGroupInfo();
	}

	public void SetPlayer( Player player )
	{
		m_Player = player;
	}

	public CGroup[] GetGroupList()
	{
		return m_lstGroups;
	}

	public CGroup NewGroup( int nId )
	{
		CGroup group = GameObject.Instantiate (Main.s_Instance.m_preGroup).GetComponent<CGroup> ();
		group.gameObject.name = "Group_" + nId;
		group.SetPlayer ( m_Player );
		group.SetID ( nId );
		m_lstGroups[nId] = group;
        group.SetDismissed(false);
        return group;
	}

	public CGroup GetGroupByIndex( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstGroups.Length) {
			return null;
		}
		return m_lstGroups [nIndex];
	}

	public int GetLiveGroupNum()
	{
		int num = 0;
		for (int i = 0; i < m_lstGroups.Length; i++) {
			CGroup group = m_lstGroups [i];
			if (group == null || group.GetDismissed ()) {
				continue;
			}
			num++;
		}
		return num;
	}

	public CGroup ReuseOneGroup()
	{
		CGroup group = null;
		int nIndex = 0;
		for (int i = 0; i < m_lstGroups.Length; i++) {
			nIndex = i;
			group = m_lstGroups [i];
			if (group == null) {
				break;
			}

			if (group && group.GetDismissed ()) { // available
				group.SetDismissed( false ); // 复用之
				return group; 
			}
		} // end for

		group = NewOneGroup ( nIndex );
		return group;
	}

	public CGroup NewOneGroup( int nIndex )
	{
		CGroup group = GameObject.Instantiate (Main.s_Instance.m_preGroup).GetComponent<CGroup> ();
		group.SetDismissed( false );
		group.SetPlayer ( m_Player );
		group.SetID ( nIndex );
		m_lstGroups[nIndex] = group;
		return group;
	}

	Dictionary<int, int> m_dicTemp = new Dictionary<int, int> ();
	Dictionary<int, CGroup> m_dicTempLiveGroups = new Dictionary<int, CGroup>();
	float c_fReArrangeInterval = 0.3f;
	float m_fReArrangeCount = 0f;
	byte[] _bytesReArrangeGroupInfo = new byte[700];
	void ReArrangeGroups ()
	{
		return;

		if (!m_Player.photonView.isMine) { // Mainplayer负责本Player的Group组织，其余客户端只是被动遵照执行
			return;
		}

		m_fReArrangeCount += Time.deltaTime;
		if (m_fReArrangeCount < c_fReArrangeInterval) {
			return;
		}
		m_fReArrangeCount = 0f;

		int nIndex = 0;
		m_dicTemp.Clear ();
		m_dicTempLiveGroups.Clear ();
		CGroup group = null;
		List<Ball> lst = m_Player.GetBallList ();
		for (int i = 0; i < m_lstGroups.Length; i++) { // 新的一轮组织，原Group全部解散
			group = m_lstGroups [i];
			if (group == null) {
				//Debug.LogError ( "有Bug！！group == null" );
				continue;
			}
			group.SetDismissed ( true );
		}

		for (int i = 0; i < lst.Count; i++) {
			Ball ball = lst [i];
			Dictionary<int, Ball> dic = ball.GetCollideBallsDic ();
			if ( ball.IsDead () || dic.Count <= 0 ) { // 死亡或壳消失会自动触发碰撞体的Exit事件，这点不用操心
				ball.SetGroup( null ); // 不在任何Group里面
				continue;
			}

			int nGroupIndex = 0;
			if (m_dicTemp.TryGetValue (ball.GetIndex (), out nGroupIndex)) { // 已经在预分组里面了
				group = GetGroupByIndex( nGroupIndex );
				if ( group == null || group.GetDismissed ()) {
					Debug.LogError ( "有Bug!!group == null || group.GetDismissed () " );
				}
			} else {// 没有在预分组里面了，新成立一个分组
				group = ReuseOneGroup();
				nGroupIndex = group.GetID ();
				if ( group == null || group.GetDismissed ()) {
					Debug.LogError ( "有Bug!!group == null || group.GetDismissed () " );
				}
			}
			m_dicTempLiveGroups [nGroupIndex] = group;
			group.AddOneBall ( ball );
			foreach (KeyValuePair<int, Ball> pair in dic) {
				group.AddOneBall( pair.Value );     // 加入分组  
				m_dicTemp [pair.Key] = nGroupIndex; // 记录入预分组
			}
		}

		// 发网络协议，把最新的分组情况通知所有其它客户端
		int nPointer = 0;
		int nLiveGroupNum = m_dicTempLiveGroups.Count;

		BitConverter.GetBytes (nLiveGroupNum).CopyTo (_bytesReArrangeGroupInfo, nPointer);
		nPointer += sizeof (byte);

		foreach( KeyValuePair<int, CGroup> pair in m_dicTempLiveGroups ) // 遍历GRoup
		{
			group = pair.Value;
			if (group == null) {
				Debug.LogError ( "从个的，有Bug！！group == null"  );
				continue;
			}
			if (group.GetDismissed ()) {
				Debug.LogError ( "从个的，有Bug！！group.GetDismissed ()"  );
				continue;
			}
			int nLiveBallNUm = group.transform.childCount;

			BitConverter.GetBytes (group.GetID()).CopyTo (_bytesReArrangeGroupInfo, nPointer);
			nPointer += sizeof (byte);
			BitConverter.GetBytes (nLiveBallNUm).CopyTo (_bytesReArrangeGroupInfo, nPointer);
			nPointer += sizeof (byte);

			float fGroupPosX = group.GetPos().x;
			BitConverter.GetBytes (fGroupPosX).CopyTo (_bytesReArrangeGroupInfo, nPointer);
			nPointer += sizeof (float);
			float fGroupPosY = group.GetPos ().y;
			BitConverter.GetBytes (fGroupPosY).CopyTo (_bytesReArrangeGroupInfo, nPointer);
			nPointer += sizeof (float);

			foreach (Transform child in group.transform) { // poppin to youhua 说过无数次了，不要在高频操作中使用GetComponent,应该做一个缓存
				Ball ball = child.gameObject.GetComponent<Ball>();
				BitConverter.GetBytes (ball.GetIndex()).CopyTo (_bytesReArrangeGroupInfo, nPointer);
				nPointer += sizeof (byte);

				float fPosX = ball.GetLocalPosition ().x;
				BitConverter.GetBytes (fPosX).CopyTo (_bytesReArrangeGroupInfo, nPointer);
				nPointer += sizeof (float);
				float fPosY = ball.GetLocalPosition ().y;
				BitConverter.GetBytes (fPosY).CopyTo (_bytesReArrangeGroupInfo, nPointer);
				nPointer += sizeof (float);
			}
		}
		m_Player.SyncReArrangeGroupInfo ( _bytesReArrangeGroupInfo );
	}

    public void SyncReArrangeGroupInfo()
    {
        return;

        StringManager.BeginPushData(_bytesReArrangeGroupInfo);
        StringManager.SetCurPointerPos(sizeof(int));

        m_fReArrangeCount += Time.deltaTime;
        if (m_fReArrangeCount < c_fReArrangeInterval)
        {
            return;
        }
        m_fReArrangeCount = 0f;

        int nPointer = 0;
        nPointer += sizeof(int);
        CGroup group = null;
        int nLiveGroupNum = 0;
        for (int i = 0; i < m_lstGroups.Length; i++)
        {
            group = m_lstGroups[i];
            if (group == null)
            {
                continue;
            }
            if (group.GetDismissed())
            {
                continue;
            }
            nLiveGroupNum++;
            int nLiveBallNUm = group.transform.childCount;

            StringManager.PushData_Byte((byte)(group.GetID()));
            StringManager.PushData_Byte((byte)nLiveBallNUm);

            float fGroupPosX = group.GetPos().x;
            StringManager.PushData_Float(fGroupPosX);
            float fGroupPosY = group.GetPos().y;
            StringManager.PushData_Float(fGroupPosY);

            foreach (Transform child in group.transform)
            { // poppin to youhua 说过无数次了，不要在高频操作中使用GetComponent,应该做一个缓存
                Ball ball = child.gameObject.GetComponent<Ball>();
                StringManager.PushData_Byte((byte)(ball.GetIndex()));

                float fPosX = ball.GetLocalPosition().x;
                StringManager.PushData_Float(fPosX);
                float fPosY = ball.GetLocalPosition().y;
                StringManager.PushData_Float(fPosY);
            } // end for ball-list
        } // end for group-list

        StringManager.SetCurPointerPos(0);
        StringManager.PushData_Int(nLiveGroupNum);
       m_Player.SyncReArrangeGroupInfo(_bytesReArrangeGroupInfo);
    }

    public void AnalyzeReArrangeGroupInfo( byte[] bytes ) // 相当于Group系统的一次全量同步
	{
        return;

        StringManager.BeginPopData(bytes);
        int nLiveGroupNum = StringManager.PopData_Int();

        List<Ball> lst = m_Player.GetBallList ();

		CGroup group = null;
		for (int i = 0; i < nLiveGroupNum; i++) {
            int nGroupIndex = StringManager.PopData_Byte();
			int nLiveBallNUm = StringManager.PopData_Byte();

            float fGroupPosX = StringManager.PopData_Float();
			float fGroupPosY =  StringManager.PopData_Float();

            group = GetGroupByIndex ( nGroupIndex );
			if (group == null) {
				group = NewOneGroup ( nGroupIndex );
			}
			group.SetDismissed ( false );
			group.SetAdjustInfo (fGroupPosX, fGroupPosY, 1);

            for (int j = 0; j < nLiveBallNUm; j++) {
                int nBallIndex = StringManager.PopData_Byte();
				Ball ball = m_Player.GetBallByIndex ( nBallIndex );
				if (ball == null || ball.IsDead ()) {
					Debug.LogError ( "从个的，有Bug!! ball == null || ball.IsDead ()" );
				}

                group.AddOneBall( ball );

                float fPosX = StringManager.PopData_Float();
				float fPosY = StringManager.PopData_Float();
				ball.SetAdjustInfo_InGroup ( fPosX, fPosY );
			} // end for ball-list
		} // end for group-list

        // 清理一下，把没有球的Group关闭
      
	}

}
