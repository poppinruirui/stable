﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {
	Animator m_Animator;

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		m_Animator = this.gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		AnimatorStateInfo info = m_Animator.GetCurrentAnimatorStateInfo (0);
		if (info.normalizedTime > ( info.length - 0.1f )) {
			Destroy (this.gameObject);
		}

	}
}
