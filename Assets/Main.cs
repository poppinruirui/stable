﻿/*
 *  只要有流水，奖金每月发，所谓年终奖不是真汉子。


*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

/*
 * 关于豆子的基本规则
 * 1、一个竞技场的总面积是有限的（有边界）。面积可按策划需求配置。
 * 2、豆子单位面积的分布数量有一个系数，该系数可按策划需求配置。
 * 3、豆子是要网络同步的，同竞技场每个客户端面对的豆子都完全一致（位置、产出、消耗 等）
 * 4、玩家视角不限，只要在游戏逻辑允许的范围内，视角可以大到同时看到全竞技场所有的豆子，故必须有优化策略（后期再考虑）
 * 5、每个豆子被消耗掉后，并非立即又刷出来，而是间隔一定时间后再刷出来，这个时间间隔可按策划需求配置。
 */
public class Main :  Photon.PunBehaviour 
{
	public Sprite[] m_aryBallSprite; 

	// Self
	public static Main s_Instance;

	public PlayerAction _playeraction;

	  // prefab “预设件”。  游戏中的各种对象都是用预设件来实例化出来的
    public GameObject m_preGridLine;
    public GameObject m_preBallLine;   
	public GameObject m_preBall;  
	public GameObject m_preBallTest;  
	public GameObject m_preElectronicBall;  
	public GameObject m_prePlayer;
	public GameObject m_preThorn; //  刺
	public GameObject m_preRhythm; // 节奏环
	public GameObject m_preBean;   // 豆子
	public GameObject m_preEffectPreExplode; // 预爆炸
	public GameObject m_preEffectExplode;    //  爆炸 
	public GameObject m_preBlackHole; // 黑洞
	public GameObject m_preSpitBallTarget; // 吐球的目标指示
	public GameObject m_preSpore; // 孢子
	public GameObject m_preNail;  // 钉子
	public GameObject m_preVoice; // 语音聊天控件
	public GameObject m_preGroup; // 分组

    // UI
	public GameObject _uiDead;
    Text m_txtDebugInfo;
	Slider m_sliderStickSize;
	Slider m_sliderAccelerate;
    Button m_btnSpit;
	Scrollbar m_sbSpitSpore;
	Scrollbar m_sbSpitBall;
	Scrollbar m_sbUnfold;
    public GameObject m_uiGamePanel;
    public GameObject m_uiMapEditorPanel;

    public Image m_imgSpitBall_Fill;
    public Image m_imgSpitSpore_Fill;
    public Image m_imgUnfold_Fill;
    public Image m_imgOneBtnSplit_Fill;

    // public data

	public int m_nTestStatus = 0; // 测试状态
    public int m_nShit = 3;
	public int m_nPlatform = 0; // 0 - PC  1 - Mobile Phone
	public const float BALL_MIN_SIZE = 1.0f; // spore size

	public float m_fBallBaseSpeed;
	public float m_fBallMoveToCenterBaseSpeed = 40f;
   
	/// <summary>
	/// new new new 
	/// </summary>
	public float m_fAdjustMoveProtectInterval = 0.5f;
	public float m_fMaxMoveDistancePerFrame = 5f;
	public int m_nObservor = 1;
	public float m_fShellAdjustShit = 3f;
	public float m_fBaseSpeedInGroupLocal = 2f;
	public float m_fSyncMoveCountInterval = 0.3f;
	public float m_nCheatMode = 0; // 是否可以作弊
	public float m_fBornProtectTime = 5f; // 重生保护时间
	public float m_fMinRunTime = 0.02f;
	public float m_fGridLineGap = 5;
	public float m_fTimeBeforeUiDead = 3f;
	public float m_fBeanNumPerRange = 0.01f;  // 单位面积豆子产生系数
	public float m_fThornNumPerRange = 0.005f; // 单位面积刺产生系数
	public float m_fBeanSize = 0.5f;
	public float m_fThornSize = 0.5f;

	public float m_fShellShrinkTotalTime;
	public float m_fExplodeShellShrinkTotalTime;
	public float m_fSplitShellShrinkTotalTime;
	public float m_fThornContributeToBallSize = 0.1f; // 刺对球总体积的贡献值

	public float m_fAutoAttenuate = 0.001f; // 球体的自动衰减系数（如果体内含刺，那么刺的体积也跟着衰减）。注意这个衰减是按比例，而不是绝对数值
	public float m_fAutoAttenuateTimeInterval = 1.0f;
	public float m_fThornAttenuate = 0.001f;

	public float m_fArenaWidth = 100.0f;
	public float m_fArenaHeight = 100.0f;
	public float m_fGenerateNewBeanTimeInterval = 10.0f; // 生成新豆子的时间间隔（单位：秒）
	public float m_fGenerateNewThornTimeInterval = 10.0f; // 生成新刺的时间间隔（单位：秒）

	public float m_fSpitSporeTimeInterval = 0.3f;        // 吐孢子时间间隔
	public float m_fSpitBallTimeInterval = 0.5f;         // 吐球时间间隔


	public float m_fMainTriggerUnfoldTime;
	public float m_fMainTriggerPreUnfoldTime;     
	public float m_fUnfoldTimeInterval = 2.0f;           // 膨胀的时间间隔

	public float m_fUnfoldSale = 2.0f;                   // 猥琐膨胀的体积是原始体积的多少倍
	public float m_fUnfoldCircleSale = 0.8f;                   // 猥琐膨胀的体积是原始体积的多少倍

	public float m_fForceSpitTotalTime = 1.0f;
	public float m_fSpitRunTime = 1.0f;                  // 吐孢子的运行时间(秒)
	public float m_fSpitBallRunTime = 1.0f;                  // 吐球的运行时间(秒)
	public float m_fSpitSporeRunDistance = 30.0f;         // 吐孢子的运行距离(描述绝对距离)
	public float m_fSpitSporeInitSpeed = 5.0f;           // 吐孢子的初速度
	public float m_fSpitSporeAccelerate = 0.0f;                 // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 
	public float m_fSpittedObjrunRunTime = 1.0f;
	public float m_fSpittedObjrunRunDistance = 5.0f;        


	public float m_fSpitBallRunDistanceMultiple= 3.0f;         // 分球的运行距离（描述母球半径的倍数, 而非直接的距离数值）

	public float m_fBornMinDiameter = 0.0f;              // 初生最小直径
	public float m_fMinDiameterToEatThorn = 0.0f;              // 吃刺最小直径

	public float m_fExplodeRunDistanceMultiple = 0.0f;   // 爆球时子球运行的距离（母球半径的倍数）
	public float m_fExplodeStayTime = 1f;   // 爆球逗留时间
	public float m_fExplodePercent = 0.8f;   // 母球分出百分之多少的体积来爆
	public float m_fExpectExplodeNum = 8.0f;             // 每次爆刺期望爆出的球球数
	public float m_fExplodeRunTime = 0.5f;             // 爆球运行时间

	public float m_fMaxBallNumPerPlayer = 0.0f;          // 一个玩家最多的球球数


	public float m_fSprayBeanMaxInitSpeed = 4.0f;
	public float m_fSprayBeanMinInitSpeed = 1.5f;
	public float m_fSprayBeanAccelerate = 8.0f;
    public float m_fSprayBeanLiveTime = 40.0f;

    public float m_fCrucifix = 0.0f;
	public int m_nSplitNum = 32;
	public float m_fSplitRunTime = 2f;	
	public float m_fSplitMaxDistance = 40f;
	public float m_fSplitTotalTime = 1f;
    public float m_fSplitTimeInterval = 2f;

	public float  m_fTriggerRadius = 1.33f;
    // poppin temp
    public GameObject the_spray;
    public GameObject m_goSprays;   

    /// <summary>
    /// 钉子
    public float m_fSpitNailTimeInterval = 0.5f;        // 吐钉子时间间隔
	public float m_fNailLiveTime = 20.0f;  			    // 钉子存活时间

	public LineRenderer m_lineWorldBorderTop;
	public LineRenderer m_lineWorldBorderBottom;
	public LineRenderer m_lineWorldBorderLeft;
	public LineRenderer m_lineWorldBorderRight;

	public GameObject m_goBorderLeft;
	public GameObject m_goBorderRight;
	public GameObject m_goBorderTop;
	public GameObject m_goBorderBottom;


    Vector3 vecTempPos = new Vector3();

	/// <summary>
	/// // public GameObjects
	/// </summary>
	/// 
	// container
	public GameObject m_goThornsSpit;
	public GameObject m_goThorns;
	public GameObject m_goSpores;
	public GameObject m_goBeans;
	public GameObject m_goSpitBallTargets;                    
	public GameObject m_goNails;
	public GameObject m_goBallsBeNailed;
	public GameObject m_goDestroyedBalls;

	public GameObject m_goBalls;

    public GameObject m_goJoyStick;

    public int s_nBallTotalNum = 0;
    public const int c_nStartUserLayerId = 9;

    public Player m_MainPlayer;
	public GameObject m_goMainPlayer;
	public GameObject _mainplayer
	{
		get { return m_goMainPlayer; }
		set { m_goMainPlayer = value; }
	}
	PhotonView m_photonViewMainPlayer;

	/// <summary>
	/// / UI
	/// </summary>
	public Text m_textDebugInfo;

	/// <summary>
	/// / Instantiate Shit
	/// </summary>
	public enum eInstantiateType
	{
		Player,
		Ball,
		Bean,
		Spore,
	};
	public object [] s_Params = new object[256];
    
	public bool m_bInitingAllBalls = true;
	public bool IsInitingAllBalls()
	{
		return m_bInitingAllBalls;
	}

	public static List<PhotonView> m_lstDelayInit = new List<PhotonView> ();
    // Use this for initialization
	void Awake()
    {
		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) { // 这儿有个BUg，暂时没找到源头，所以只有做一个容错
			return;
		}

	    s_Instance = this;

        ReadGamePlayDataConfig();

		if ( AccountManager.m_eSceneMode == AccountManager.eSceneMode.Game && !AccountManager.s_bObserve )
        {
            //InitArena();
            InitMainPlayer();
            m_uiGamePanel.SetActive( true );
			/*
			for (int i = 0; i < m_lstDelayInit.Count; i++) {
				PhotonView pv = m_lstDelayInit [i];
				GameObject go = GameObject.Instantiate ( m_prePlayer );
				Player player = go.GetComponent<Player>();
				PhotonView pv_new = go.GetComponent<PhotonView>();
				int nViewId = pv.viewID;
				int nOwnerId = pv.ownerId;
				pv_new.ownerId = nOwnerId;
				pv_new.viewID = nViewId;
				player.Init ();

			}
			*/		
        }
        else
        {
            m_goJoyStick.SetActive( false );
            m_uiGamePanel.SetActive(false);
        }

        if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.MapEditor)
        {
            MapEditor.s_Instance.Init();
            m_uiMapEditorPanel.SetActive( true );

        }
        else
        {
            m_uiMapEditorPanel.SetActive(false);
        }

        //MapEditor.s_Instance.LoadMap();
    }
    
	Dictionary<string, float> m_dicGamePlayDataConfig = new Dictionary<string, float> ();
	string [][]Array;  
	void ReadGamePlayDataConfig()
	{
        return; // 这块功能转移到统一的地图编辑器了

		//按行读取为字符串数组
		string filename = Application.streamingAssetsPath + "/GamePlayData.txt";

		string[] lines = System.IO.File.ReadAllLines( filename  );
		List<string> lst = new List<string> ();
		foreach (string line in lines)
		{
			lst.Add ( line );
		}
		Array = new string [lst.Count][];  //创建二维数组  
		for (int i = 0; i < lst.Count; i++) {
			Array[i] = lst[i].Split ('\t');  
			float val = 0.0f;
			float.TryParse ( Array[i][2], out val );
			m_dicGamePlayDataConfig[Array[i][1]] = val;
		}
	
		m_fBeanNumPerRange = m_dicGamePlayDataConfig["fBeanNumPerRange"];
		m_fThornNumPerRange = m_dicGamePlayDataConfig ["fThornNumPerRange"];
		m_fArenaWidth = m_dicGamePlayDataConfig ["fArenaWidth"];
		m_fArenaHeight = m_dicGamePlayDataConfig ["fArenaHeight"];
		m_fGenerateNewBeanTimeInterval = m_dicGamePlayDataConfig ["fGenerateNewBeanTimeInterval"];
		m_fGenerateNewThornTimeInterval = m_dicGamePlayDataConfig ["fGenerateNewThornTimeInterval"];
		m_fBeanSize = m_dicGamePlayDataConfig ["fBeanSize"];
		m_fThornSize = m_dicGamePlayDataConfig ["fThornSize"];
		m_fThornContributeToBallSize = m_dicGamePlayDataConfig ["fThornContributeToBallSize"];
		m_fBallBaseSpeed = m_dicGamePlayDataConfig ["fBallBaseSpeed"];
		m_fSpitSporeRunDistance = m_dicGamePlayDataConfig ["fSpitSporeRunDistance"];
		m_fSpitRunTime = m_dicGamePlayDataConfig ["fSpitRunTime"];
		m_fSpitBallRunDistanceMultiple = m_dicGamePlayDataConfig ["fSpitBallRunDistanceMultiple"];
		m_fMainTriggerPreUnfoldTime = m_dicGamePlayDataConfig ["fMainTriggerPreUnfoldTime"];
		m_fMainTriggerUnfoldTime = m_dicGamePlayDataConfig ["fMainTriggerUnfoldTime"];
		m_fUnfoldTimeInterval = m_dicGamePlayDataConfig ["fUnfoldTimeInterval"];
		m_fUnfoldSale = m_dicGamePlayDataConfig ["fUnfoldSale"];
		m_fUnfoldCircleSale = m_fUnfoldSale * 0.8f / 2.0f;
		m_fAutoAttenuate = m_dicGamePlayDataConfig ["fAutoAttenuate"];
		m_fAutoAttenuateTimeInterval = m_dicGamePlayDataConfig ["fAutoAttenuateTimeInterval"];
		m_fForceSpitTotalTime = m_dicGamePlayDataConfig ["fForceSpitTotalTime"];
		m_fShellShrinkTotalTime = m_dicGamePlayDataConfig ["fShellShrinkTotalTime"];
		if (m_fShellShrinkTotalTime < 0.1f) {
			m_fShellShrinkTotalTime = 0.1f;
		}
		m_fSpitBallTimeInterval = m_dicGamePlayDataConfig ["fSpitBallTimeInterval"];
		m_fSpitSporeTimeInterval = m_dicGamePlayDataConfig ["fSpitSporeTimeInterval"];
		m_fBornMinDiameter = m_dicGamePlayDataConfig ["fBornMinDiameter"];
		m_fMinDiameterToEatThorn = m_dicGamePlayDataConfig ["fMinDiameterToEatThorn"];
		m_fExplodeRunDistanceMultiple = m_dicGamePlayDataConfig ["fExplodeRunDistanceMultiple"];
		m_fMaxBallNumPerPlayer = m_dicGamePlayDataConfig ["fMaxBallNumPerPlayer"];
		m_fSpitNailTimeInterval = m_dicGamePlayDataConfig ["fSpitNailTimeInterval"];
		m_fNailLiveTime = m_dicGamePlayDataConfig ["fNailLiveTime"];
		m_fSpittedObjrunRunDistance	 = m_dicGamePlayDataConfig ["fSpittedObjrunRunDistance"];
        m_fCrucifix = m_dicGamePlayDataConfig["fCrucifix"];
		m_nSplitNum = (int)m_dicGamePlayDataConfig["nSplitNum"];
		m_fSplitRunTime = m_dicGamePlayDataConfig["fSplitRunTime"];
		m_fSplitMaxDistance = m_dicGamePlayDataConfig["fSplitMaxDistance"];

        m_fSpitSporeAccelerate = CyberTreeMath.GetA(m_fSpitSporeRunDistance,  m_fSpitRunTime);
		m_fSpitSporeInitSpeed = CyberTreeMath.GetV0 (m_fSpitSporeRunDistance,  m_fSpitRunTime);
	}

	public void Born()
	{
		if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game) {
			return;
		}

		if (AccountManager.s_bObserve) {
			return;
		}

		m_MainPlayer.SetPlayerName ( MapEditor.GetMainPlayerName() );

		// 直接初始化本玩家的全部球出来（配置的上限是多少个就是多少个）
		Ball ball = null;
		for (int i = 0; i < m_fMaxBallNumPerPlayer; i++) {
			ball = GameObject.Instantiate ( m_preBall ).GetComponent<Ball>();
			m_MainPlayer.AddOneBall ( ball );
		}

		m_MainPlayer.DoReborn ();
	}

	void InitMainPlayer ()
	{
		if (Main.s_Instance == null) {
			return;
		}

		s_Params [0] = eInstantiateType.Player;
		s_Params [1] = 111;
		m_goMainPlayer = PhotonInstantiate ( m_prePlayer, Vector3.zero, s_Params );// GameObject.Instantiate( m_prePlayer );
		m_MainPlayer = m_goMainPlayer.GetComponent<Player>();
		m_goMainPlayer = m_MainPlayer.gameObject;
		m_photonViewMainPlayer = m_goMainPlayer.GetComponent<PhotonView> ();

		m_goMainPlayer.name = "player_" + m_photonViewMainPlayer.ownerId;
		m_goMainPlayer.transform.parent = m_goBalls.transform;


	}


    public int GetMainPlayerPhotonId()
    {
        return m_MainPlayer.photonView.ownerId;
    }

	public GameObject PhotonInstantiate( GameObject prefab, Vector3 vecPos, object[] aryParams = null )
	{
		return PhotonNetwork.Instantiate( prefab.name, vecPos, Quaternion.identity, 0, aryParams );
	}

	public void PhotonDestroy( GameObject obj )
	{
		PhotonNetwork.Destroy ( obj );
	}

	int woca = 1;
    void ColdDownLoop()
    {
        // SpitBall ColdDown
        if (!m_bForceSpit)
		{
            if (m_fSpitBallCurColdDownTime > 0.0f)
            {
                m_fSpitBallCurColdDownTime -= Time.deltaTime;
                if (m_fSpitBallTimeInterval > 0)
                {
                    m_imgSpitBall_Fill.fillAmount = ( m_fSpitBallTimeInterval - m_fSpitBallCurColdDownTime ) / m_fSpitBallTimeInterval;
                }
                else
                {
                    m_imgSpitBall_Fill.fillAmount = 1f;
                }
            }
        }

        // SpitSpore ColdDown
        if (m_fSpitSporeCurColdDownTime > 0.0f)
        {
            m_fSpitSporeCurColdDownTime -= Time.deltaTime;
            if (m_fSpitSporeTimeInterval > 0)
            {
                m_imgSpitSpore_Fill.fillAmount = ( m_fSpitSporeTimeInterval - m_fSpitSporeCurColdDownTime ) / m_fSpitSporeTimeInterval;
            }
            else
            {
                m_imgSpitSpore_Fill.fillAmount = 1f;
            }
        }

        // unfold colddown
        if (m_fUnfoldCurColdDownTime > 0.0f)
        {
            m_fUnfoldCurColdDownTime -= Time.deltaTime;
            if (m_fUnfoldTimeInterval > 0)
            {
                m_imgUnfold_Fill.fillAmount = ( m_fUnfoldTimeInterval - m_fUnfoldCurColdDownTime ) / m_fUnfoldTimeInterval;
            }
            else
            {
                m_imgUnfold_Fill.fillAmount = 1f;
            }
        }

        // OneBtnSplit ColdDown
        if (m_fSplitTimeCount < m_fSplitTimeInterval)
        {
            if (m_fSplitTimeInterval > 0)
            {
                m_imgOneBtnSplit_Fill.fillAmount = m_fSplitTimeCount / m_fSplitTimeInterval;
            }
            else
            {
                m_imgOneBtnSplit_Fill.fillAmount = 1f;
            }
			m_fSplitTimeCount += Time.deltaTime;
        }
        else
        {
            m_imgOneBtnSplit_Fill.fillAmount = 1f;
        }
            
    }

	int m_nDeadStatus = 0; // 0 - 没有展示Uidead的状态  1 - 正在展示UiDead的状态
	float m_fDeadProgressTimeCount = 0f;

	void ProcessDead()
	{
		if ( ( m_MainPlayer == null ) || (!m_MainPlayer.IsRebornCompleted()) || ( !m_MainPlayer.IsDead ()) ) {
			return;
		}

		if (m_nDeadStatus == 0) { // 等待UI_Dead界面出现阶段
			m_fDeadProgressTimeCount += Time.deltaTime;
			if (m_fDeadProgressTimeCount >= m_fTimeBeforeUiDead) {
				m_nDeadStatus = 1;
				_uiDead.SetActive (true);
				m_fDeadProgressTimeCount = 0f;
			}
		} else if (m_nDeadStatus == 1) { // UI_Dead界面出现阶段

		}



	}

    void GameUpdate()
    {
		if (AccountManager.s_bObserve) {
			return;
		}

        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
        {
            return;
        }

		if (!MapEditor.s_Instance.IsMapInitCompleted ()) { // 地图初始化完成之前，不要开始执行游戏逻辑
			return;
		}

		SpitTouching();
		PreOneBtnSplitting ();
				//InitingBalls ();
        InitArenaBeans();
        InitArenaThorns();
        Bean.GenerateNewBeanLoop();
        Thorn.GenerateNewThornLoop();
		Thorn.ColorThornReborn ();

        ColdDownLoop();
		CameraSizeProtectLoop ();

        ForceSpitting();

		ProcessDead ();

        ProcessKeyboard();
		ProcessMouse ();
		ProcessTouch ();

        //AutoAttenuate();
		if (m_goMainPlayer)
        {
            //m_textDebugInfo.text = "主角球球数: " + m_goMainPlayer.transform.childCount.ToString () + "\n场景豆子和场景刺总数:" + (m_nBeanTotalNumDueToRange + m_nThornTotalNumDueToRange) .ToString();
            //m_textDebugInfo.text = PhotonNetwork.time.ToString();
            //m_textDebugInfo.text = PhotonNetwork.isMasterClient.ToString();
            //m_textDebugInfo.text = the_spray.transform.childCount.ToString();
        }

     }

	void Update () 
	{
		if (AccountManager.m_eSceneMode == AccountManager.eSceneMode.None) {
			return;
		}

        GameUpdate();

    }

	// 判断当前是否点击在了UI上
	public bool IsPointerOverUI()
	{
		PointerEventData eventData = new PointerEventData(UnityEngine.EventSystems.EventSystem.current);
		eventData.pressPosition = Input.mousePosition;
		eventData.position = Input.mousePosition;

		List<RaycastResult> list = new List<RaycastResult>();
		UnityEngine.EventSystems.EventSystem.current.RaycastAll(eventData, list);

		return list.Count > 0;

	}

	void ProcessTouch ()
	{
		if (m_nPlatform != 1) {
			return;
		}

		if (IsPointerOverUI ()) {
			return;
		}

		if (Input.touchCount > 0) {
			if (!m_MainPlayer.IsMoving ()) {
				m_MainPlayer.SetMoving ( true );
			}
		}


	}

	void ProcessMouse ()
	{
		if (m_nPlatform != 0) {
			return;
		}

		if (IsPointerOverUI ()) {
			return;
		}

		if (Input.GetMouseButtonDown (1)) {
			if (m_MainPlayer) {
				m_MainPlayer.SetMoving ( false );
			}
		}

		if (Input.GetMouseButtonDown (0)) {
			if (m_MainPlayer) {
				m_MainPlayer.SetMoving ( true );
			}
		} 
	}

	void ProcessKeyboard()
	{
		if ( Input.GetKeyDown (KeyCode.A) )
		{
			Test_AddBallsSize();
		}

		if ( Input.GetKeyDown (KeyCode.S) )
		{
			MergeAll();
		}

		if ( Input.GetKeyDown (KeyCode.D) )
		{
			Test_KingExplode ();
		}

		if (Input.GetKey (KeyCode.Q)) {
			if (!m_MainPlayer.IsSpittingSpore ()) {
				m_MainPlayer.BeginSpitSpore ();
			}
		} else {
			if (m_MainPlayer.IsSpittingSpore ()) {
				m_MainPlayer.EndSpitSpore ();
			}
		}


		if (Input.GetKey(KeyCode.Escape))
		{
			ExitGame ();
		} 
	

		if (!UI_Spit.s_bUsingUi) {
			if (Input.GetKey (KeyCode.W)) {
				BeginSpit ();
			} else {
				if (m_bForceSpit) {
					EndSpit ();
				}
			}
		}

		if (Input.GetKey(KeyCode.E))
        {
            if (MapEditor.GetEatMode() == MapEditor.eEatMode.yaqiu) {
				Unfold ();
			}
		} 

		if (Input.GetKey(KeyCode.R)) // PC版一键分球
		{
            if (m_fSplitTimeCount > m_fSplitTimeInterval ) {
				OneBtnSplitBall ();
				m_fSplitTimeCount = 0f;
			}

		} 
	}

	public void OnClick_OneBtnSplitBall()
	{
		/*
		if (m_fSplitTimeCount > m_fSplitTimeInterval ) {
			OneBtnSplitBall ();
			m_fSplitTimeCount = 0f;
		}  
		*/
	}

	float m_fSplitTimeCount = 1024f;

	float m_fArenaRange = 0.0f;
	float m_nBeanTotalNumDueToRange = 0;
	float m_nCurBeanTotalNum = 0;
	float m_nCurGeneraedByMeBeanTotalNum = 0;
    int m_nCurGeneraedByMeThornTotalNum = 0;
	float m_nThornTotalNumDueToRange = 0;
	float m_nCurThornTotalNum = 0;
	public void UpdateWorldSize( float fWorldSizeX, float fWorldSizeY )
	{
        /*
		m_fArenaRange = m_fArenaWidth * m_fArenaHeight;
		m_nBeanTotalNumDueToRange = (int)( m_fBeanNumPerRange * m_fArenaRange );
		m_nThornTotalNumDueToRange =  (int)( m_fThornNumPerRange * m_fArenaRange );
		m_fWorldLeft = -m_fArenaWidth / 2.0f;
		m_fWorldRight = m_fArenaWidth / 2.0f; 
		m_fWorldBottom = -m_fArenaHeight / 2.0f;
		m_fWorldTop = m_fArenaHeight / 2.0f;
        */
		m_fArenaRange = fWorldSizeX * fWorldSizeY;
        m_nBeanTotalNumDueToRange = (int)( m_fBeanNumPerRange * m_fArenaRange );
        m_nThornTotalNumDueToRange =  (int)( m_fThornNumPerRange * m_fArenaRange );
		m_fWorldLeft = -fWorldSizeX / 2.0f;
		m_fWorldRight = fWorldSizeX / 2.0f; 
		m_fWorldBottom = -fWorldSizeY / 2.0f;
		m_fWorldTop = fWorldSizeY / 2.0f;
	}

	// 为竞技场生成豆子。注意一定要分帧处理，不然会卡死。


	// 平衡场景豆子的总量
	float m_fGenerateBeanTime = 0.0f;
	float m_fGenerateThornTime = 0.0f;
	const float c_GenerateTotalTime = 10.0f;
	public void InitArenaBeans()
	{
       if (m_nCurGeneraedByMeBeanTotalNum >= m_nBeanTotalNumDueToRange   ) {
			return;
		}

	    GenerateBean ();
        m_nCurGeneraedByMeBeanTotalNum++;
	}

	// 平衡刺的个数
	public void InitArenaThorns()
	{
        if (m_nCurGeneraedByMeThornTotalNum >= m_nThornTotalNumDueToRange)
        {
            return;
        }

		GenerateThorn ();
        m_nCurGeneraedByMeThornTotalNum++;
	}




	/*
	public void CalculateCurTotalBeanNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		m_nCurBeanTotalNum = (int)(fRange * m_fBeanNumPerRange);
	}



	public void CalculateCurTotalThornNum()
	{
		vecWorldMin = Camera.main.ScreenToWorldPoint ( vecScreenMin );
		vecWorldMax = Camera.main.ScreenToWorldPoint ( vecScreenMax );
		float fWorldWidth = vecWorldMax.x - vecWorldMin.x;
		float fWorldHeight = vecWorldMax.y - vecWorldMin.y;
		float fRange = fWorldWidth * fWorldHeight;
		//Debug.Log ( "fRange=" + fRange );
		m_nCurThornTotalNum = (int)(fRange * m_fThornNumPerRange);
	}
*/
	float m_fWorldLeft = 0.0f, m_fWorldRight = 0.0f, m_fWorldBottom = 0.0f, m_fWorldTop = 0.0f;
	public Vector3 RandomPosWithinWorld()
	{
		vecTempPos.x = (float)UnityEngine.Random.Range ( m_fWorldLeft, m_fWorldRight );
		vecTempPos.y = (float)UnityEngine.Random.Range ( m_fWorldBottom, m_fWorldTop );
		vecTempPos.z = 0.0f;
		return vecTempPos;
	}

	Vector3 vecScreenMin = new Vector3 ( 0.0f, 0.0f, 0.0f );
	Vector3 vecScreenMax = new Vector3 ( Screen.width, Screen.height, 0.0f );
	Vector3 vecWorldMin = new Vector3();
	Vector3 vecWorldMax = new Vector3();
	public Vector3 RandomPosWithinScreen( )
	{
		vecTempPos.x = (float)UnityEngine.Random.Range (vecWorldMin.x, vecWorldMax.x);
		vecTempPos.y = (float)UnityEngine.Random.Range (vecWorldMin.y, vecWorldMax.y);
		vecTempPos.z = 0.0f;
		return vecTempPos;
	}

	public void GenerateBean()
	{
		//GameObject goBean = PhotonInstantiate( m_preBean, new Vector3 ( (float)UnityEngine.Random.Range(-200, 200), (float)UnityEngine.Random.Range(-200, 200) , 10.0f ) ) ;
		Vector3 pos = RandomPosWithinWorld();
		Bean bean = ResourceManager.ReuseBean();
		if (bean == null) {
			return;
		}
		bean.transform.transform.parent = m_goBeans.transform;
		bean.transform.localPosition = pos;

    }

	List<Thorn> m_lstArenaThorn = new List<Thorn>();
	public void GenerateThorn()
	{
		//GameObject goThorn = PhotonInstantiate(m_preThorn, new Vector3 ( posX,  posY, 10.0f )  );
		vecTempPos = MapEditor.s_Instance.GetFakeRandomThornPos( (int)m_nCurGeneraedByMeThornTotalNum );
		vecTempPos.z = 0;
		GameObject goThorn = GameObject.Instantiate( m_preThorn );
		Thorn thorn = goThorn.GetComponent<Thorn> ();
		thorn.SetDead ( false );
		goThorn.transform.parent = m_goThorns.transform;
		goThorn.transform.localPosition = vecTempPos;
		thorn.SetIndex ( m_lstArenaThorn.Count );
		m_lstArenaThorn.Add ( thorn );
	}

	public void DestroyThorn( int nThornIndex )
	{
		if (nThornIndex >= m_lstArenaThorn.Count) {
			return;
		}
		Thorn thorn = m_lstArenaThorn [nThornIndex];
		thorn.SetDead ( true );

		Thorn.AddToRebornList ( thorn );
	}

    void FixedUpdate()
    {


    }

	public float m_fForceSpitPercent = 0.0f;
	bool m_bForceSpit = false;
	float m_fSpitBallCurColdDownTime = 0.0f;
	public void BeginSpit ()
	{
        if (m_bForceSpit)
        {
            return;
        }

		int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		if (m_fSpitBallCurColdDownTime > 0.0f) { // ColdDown not completed
			return;
		}
		m_fSpitBallCurColdDownTime = m_fSpitBallTimeInterval;

		m_bForceSpit = true;
		m_fForceSpitPercent = 0.0f;

		m_MainPlayer.RefreshLiveBalls ();
		//ball.ClearAvailableChildSize ();

		m_fForceSpitCurTime = 0.0f;
	}

	void DoSpit()
	{

	}

	// 处于“视野保护状态”那一瞬间，镜头只能拉远不能拉近
	float m_fCurCamSize = 0f;
	float m_fCamSizeProtectTime = 0;
	public void BeginCameraSizeProtect(  float fCamSizeProtectTime )
	{
		m_fCurCamSize = Camera.main.orthographicSize;
		m_fCamSizeProtectTime = fCamSizeProtectTime;
	}

	void CameraSizeProtectLoop()
	{
		if (m_fCamSizeProtectTime <= 0f) {
			return;
		}
		m_fCamSizeProtectTime -= Time.deltaTime;
	}

	public bool IsCameraSizeProtecting( )
	{
		return m_fCamSizeProtectTime > 0f;
	}

	public void CancelSpit ()
	{
		m_bForceSpit = false;
		m_MainPlayer.CancelSplit ();
	}

	public void EndSpit ()
	{
		if ( !m_bForceSpit )
		{
			return;	
		}

		int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			CancelSpit ();
			return;
		}

		m_bForceSpit = false;


		if (m_MainPlayer) {
			m_MainPlayer.DoSpit ( m_nPlatform ); // right here
		}

	/*

		foreach (Transform child in m_goMainPlayer.transform.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();

			if (ball.IsEjecting ()) {
				continue;
			}

			float fMotherSize = ball.GetSize ();
			float fMotherLeftSize = 0.0f;
			float fChildSize = ball.GetAvailableChildSize( ref fMotherLeftSize );
			if (fChildSize < BALL_MIN_SIZE || fMotherLeftSize < BALL_MIN_SIZE || fMotherLeftSize < fChildSize ) {
				continue;
			}

			// 如果母球身上有刺，则按比例被子球带走
			//float fMotherThornSize = ball.GetThornSize ();
			//float fChildThornSize = ;
			//SetThornSize ( Mathf.Sqrt( fCurThornSize * fCurThornSize + fAddedThornSize * fAddedThornSize ) );
			//ball.SetThornSize ( ball.GetThornSize() * fMotherLeftSize / fMotherSize );

			ball.SetSize ( fMotherLeftSize );
            Main.s_Instance.s_Params[0] = eInstantiateType.Ball;
            Main.s_Instance.s_Params[1] = 222;
			Ball new_ball = Ball.NewOneBall( ball.photonView.ownerId );//PhotonInstantiate (m_preBall, Vector3.zero, Main.s_Instance.s_Params);
			//Ball new_ball = goNewBall.GetComponent<Ball> ();
			new_ball.SetSize ( fChildSize );
			new_ball.transform.parent = m_goMainPlayer.transform;
			ball.CalculateNewBallBornPosAndRunDire ( new_ball, ball._dirx, ball._diry );
			ball.RelateTarget ( new_ball );
			ball.BeginShell ();
			new_ball.SetThornSize ( ball.GetThornSize() * fChildSize / fMotherSize );
			new_ball.BeginShell ();
			new_ball.BeginEject ( ball.m_fSpitBallInitSpeed, ball.m_fSpitBallAccelerate );


		} // end foreach

		*/




	}

	public int GetMainPlayerCurBallCount()
	{
		if ( m_MainPlayer == null) {
			return 0;
		}

		return m_MainPlayer.GetCurMainPlayerBallCount();//m_goMainPlayer.transform.childCount;
	}

	float m_fForceSpitCurTime = 0.0f;

	public bool IsForceSpitting()
	{
		return m_bForceSpit;
	}

	// 在这里计算瞄准器的形态
	void ForceSpitting ()
	{
		int nAvailableNum = m_MainPlayer.GetCurAvailableBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		if (!m_bForceSpit) {
			return;
		}

		m_fForceSpitCurTime += Time.deltaTime;  
		m_fForceSpitPercent = m_fForceSpitCurTime / m_fForceSpitTotalTime; 
		if (m_fForceSpitPercent > 0.5f) {
			m_fForceSpitPercent = 0.5f;
		}
        
		m_MainPlayer.Spitting ( m_fForceSpitPercent );
		/*
		int nCount = 0;
		foreach (Transform child in m_goMainPlayer.transform.transform) {
			Ball ball = child.gameObject.GetComponent<Ball> ();
			if (!ball.CheckIfCanForceSpit ()) {
				continue;
			}
			ball.CalculateChildSize ( m_fForceSpitPercent );
			ball.ShowTarget ();
			nCount++;
			if (nCount >= nAvailableNum) {
				break;
			}
		} // end foreach
		*/
	}

	// 猥琐地扩张
	float m_fUnfoldCurColdDownTime = 0.0f;
	public void Unfold()
	{
		if (m_fUnfoldCurColdDownTime > 0.0f) { // ColdDown not completed
			return;
		}
		/*
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}

			if (!ball.CheckIfCanUnfold ()) {
				continue;
			}

			ball.BeginPreUnfold ();

		} // end foreach		

		*/
		m_MainPlayer.BeginPreUnfold ();
		m_fUnfoldCurColdDownTime += ( m_fMainTriggerPreUnfoldTime + m_fMainTriggerUnfoldTime + m_fUnfoldTimeInterval );

	}

	float m_fSpitSporeCurColdDownTime = 0.0f;
	float m_fSpitNailCurColdDownTime = 0.0f; // 吐钉子的ColdDown时间


	// 吐孢子
	/*
	public void SpitSpore()
	{
		if (m_fSpitSporeCurColdDownTime > 0.0f) { // ColdDown还没结束
			return;
		}

		m_fSpitSporeCurColdDownTime = m_fSpitSporeTimeInterval;

		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			//Ball ballSpore =  ball.SpitSpore();
			ball.SpitBean();
		} // end foreach		
	}
	*/


	/*
	// 自动衰减
	float m_fAttenuateTimeCount = 0.0f;
	public void AutoAttenuate()
	{
		// poppin test for optimizing
		return;

        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }
            ball.AutoAttenuateThorn();
         }

        m_fAttenuateTimeCount += Time.deltaTime;
		if (m_fAttenuateTimeCount < Main.s_Instance.m_fAutoAttenuateTimeInterval) {
			return;
		}
		m_fAttenuateTimeCount = 0.0f;
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}
			ball.AutoAttenuate ( Main.s_Instance.m_fAutoAttenuate );
		}// end foreach		
	}
	*/


	public void Test_AddBallsSize()
	{
		if (m_nCheatMode == 0) {
			return;
		}

		m_MainPlayer.Test_AddAllBallSize (0.5f);
		/*
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			if (ball.IsDead ()) {
				continue;
			}

			ball.SetSize ( ball.GetSize() + 1.0f );

		}// end foreach	
		*/
	}

	List<Ball> lstTemp = new List<Ball> ();
	public void Test_KingExplode()
	{		if (m_nCheatMode == 0) {
			return;
		}
		
		m_MainPlayer.OneBtnExplode ();
	}

    public void Test_AddThornSize()
    {
        foreach (Transform child in m_goMainPlayer.transform)
        {
            GameObject go = child.gameObject;
            Ball ball = go.GetComponent<Ball>();
            if (ball == null)
            {
                continue;
            }

            ball.SetThornSize(ball.GetThornSize() + 0.1f);

        }// end foreach	
    }

	public void Test_AddThornOfBallSize() 
	{
		foreach (Transform child in m_goMainPlayer.transform) {
			GameObject go = child.gameObject;
			Ball ball = go.GetComponent<Ball> ();
			if (ball == null) {
				continue;
			}	

			if ( ball.GetSize () < (2 * Main.BALL_MIN_SIZE ) ) {
				continue;
			}

			ball.SetThornSize ( ball.GetThornSize() + Main.s_Instance.m_fThornSize );

			// 吃刺也会增加球本身的总体积
			ball.SetSize( ball.GetSize() + Main.s_Instance.m_fThornContributeToBallSize );


		}// end foreach	
	}
		
	public void ExitGame()
    {
		AccountManager.m_eSceneMode = AccountManager.eSceneMode.None;
//		SceneManager.UnloadSceneAsync ( "Scene" );
		PhotonNetwork.Disconnect();
	//	SceneManager.LoadScene( "SelectCaster" );
	}

	public void MergeAll()
	{
		if (m_nCheatMode == 0) {
			return;
		}

		Main.s_Instance.BeginCameraSizeProtect ( 0.5f );
		m_MainPlayer.MergeAll ();
	}

/*
	public void SyncMoveInfo( Vector3 vecWorldCursorPos )
	{
		if (m_MainPlayer) {
			m_MainPlayer.SyncMoveInfo ( vecWorldCursorPos );
		}
	}
*/
	List<Player> m_lstPlayers = new List<Player>();
	public void AddOnePlayer( Player player )
	{
		m_lstPlayers.Add ( player );
	}

	public void RemoveOnePlayer( Player player )
	{

	}

	// 一键分球
	public void OneBtnSplitBall()
	{
		BeginCameraSizeProtect (  Main.s_Instance.m_fSplitRunTime );
		if (m_nPlatform == 0) { // 这个流程只针对PC版。 手机版走另外的流程			
			m_MainPlayer.OneBtnSplitBall ( m_nPlatform, _playeraction.GetWorldCursorPos ());
		} else {
			m_MainPlayer.OneBtnSplitBall ( m_nPlatform, m_fCurOneBtnSplitDis );
		}
	}

		// One Btn Split
	bool m_bPreOneBtnSplit = false;
	float m_fPreOneBtnSplitTimeCount = 0f;
	float m_fCurOneBtnSplitDis = 0f;
	int m_nSplitTouchFingerId = 0;
	Vector3 m_vecSplitTouchStartPos = new Vector3();
	Vector3 m_vecCurSplitTouchPos = new Vector3();
	Vector2 m_vecSplitDirction = new Vector2();

	public void BeginRSplit()
	{
		if (m_fSplitTimeCount < m_fSplitTimeInterval) {
			return; // ColdDown还没结束
		}
		m_fPreOneBtnSplitTimeCount = 0;
		m_bPreOneBtnSplit = true;
	}

	public void BeginPreOneBtnSplit( int nFingerId, Vector3 vecStartPos )
	{
		m_bPreOneBtnSplit = true;
		m_fPreOneBtnSplitTimeCount = 0f;
		m_nSplitTouchFingerId = nFingerId;
		m_vecSplitTouchStartPos = vecStartPos;
		m_vecCurSplitTouchPos = vecStartPos;
		m_fSplitTimeCount = 0f;
	}

	public void EndPreOneBtnSplit()
	{
		m_MainPlayer.BeginClearSpitTargetCount ( m_fSplitRunTime );
		m_bPreOneBtnSplit = false;
		OneBtnSplitBall ();
	}

	void PreOneBtnSplitting()
	{
		if (!m_bPreOneBtnSplit) {
			return;
		}

		m_fPreOneBtnSplitTimeCount += Time.deltaTime;
		float fPercent = m_fPreOneBtnSplitTimeCount / m_fSplitTotalTime;
		if (fPercent > 1f) {
			fPercent = 1f;
		}
		m_fCurOneBtnSplitDis = m_fSplitMaxDistance * fPercent;

		if (m_nSplitTouchFingerId != 0 && Input.touchCount < 2 ) {
			m_nSplitTouchFingerId = 0;
		}  

		if (m_nPlatform == 1) {
			Touch touch = Input.GetTouch (m_nSplitTouchFingerId);
			m_vecCurSplitTouchPos.x += touch.deltaPosition.x;
			m_vecCurSplitTouchPos.y += touch.deltaPosition.y;
		} else {
			m_vecCurSplitTouchPos = Input.mousePosition;
		}

		m_vecSplitDirction = m_vecCurSplitTouchPos - m_vecSplitTouchStartPos;
		m_vecSplitDirction.Normalize ();
		_playeraction.m_vec2MoveInfo = m_vecSplitDirction; 

		m_MainPlayer.PreOneBtnSplitting ( m_fCurOneBtnSplitDis );
	}

	public Vector3 GetSplitDirction()
	{
		return m_vecSplitDirction;
	}

	public void StartDaTaoSha()
	{
		if (m_MainPlayer) {
			m_MainPlayer.StartDaTaoSha ();
		}
	}

	public Dictionary<int, TattooFood> m_dicTattooFood = new Dictionary<int, TattooFood>();
	int m_nTattooFoodGUID = 0;
	public int GenerateTattooFoodGUID()
	{
		return m_nTattooFoodGUID++;
	}

	public void AddOnetattooFood ( TattooFood food )
	{
		m_dicTattooFood [food.GetGUID()] = food;
	}

	public void RemoveOnetattoo( int nGUID )
	{
		TattooFood food = null;
		m_dicTattooFood.TryGetValue (nGUID, out food);
		if (food) {
			ResourceManager.s_Instance.RemoveTattooFood ( food );
		}
	}

	public Sprite GetSpriteByPhotonOwnerId( int nOwnerId )
	{
		int nSpriteId = nOwnerId - 1;
		if (nSpriteId < 0 )
		{
			nSpriteId = 0;
		}
		if (nSpriteId >= Main.s_Instance.m_aryBallSprite.Length) {
			nSpriteId = nSpriteId % Main.s_Instance.m_aryBallSprite.Length;
		}
		return m_aryBallSprite [nSpriteId];
	}

	public int GetSpriteIdByPhotonOwnerId( int nOwnerId )
	{
		int nSpriteId = nOwnerId - 1;
		if (nSpriteId < 0 )
		{
			nSpriteId = 0;
		}
		if (nSpriteId >= Main.s_Instance.m_aryBallSprite.Length) {
			nSpriteId = nSpriteId % Main.s_Instance.m_aryBallSprite.Length;
		}
		return nSpriteId;
	}

	public Sprite GetSpriteBySpriteId( int nSpriteId )
	{
		if (nSpriteId < 0 || nSpriteId >= Main.s_Instance.m_aryBallSprite.Length )
		{
			nSpriteId = 0;
		}
		return m_aryBallSprite [nSpriteId];
	}

	public int GetSpriteArrayLength()
	{
		return m_aryBallSprite.Length;
	}

	int m_nInitingBallsCount = 0;
	float m_fInitingBallsTime = 0f;
	/*
	void InitingBalls()
	{
		if (!m_bInitingAllBalls) {
			return;
		}

		if (m_MainPlayer == null || (!m_MainPlayer.photonView.isMine)) {
			return;
		}

		vecTempPos.x = -10000f;
		vecTempPos.y = -10000f;
		GameObject goBall = PhotonInstantiate(m_preBall, vecTempPos, Main.s_Instance.s_Params) ;
		goBall.transform.parent = m_MainPlayer.transform;
		goBall.transform.position = vecTempPos;
		Ball ball = goBall.GetComponent<Ball> ();
		m_MainPlayer.AddOneBallToRecycled ( ball );
		ball.SyncFullInfo ();
		if ( m_MainPlayer.GetCurTotalBallCountIncludeRecycled() >= m_fMaxBallNumPerPlayer) {
			EndInitingBalls ();
		}
	}
	*/
	void EndInitingBalls()
	{
		m_MainPlayer.EndInitAllBalls ();
		//m_MainPlayer.RecycleAllBalls ();
		m_bInitingAllBalls = false;
	}

	Vector3 m_vecStartPos = new Vector3();
	int m_nFingerId = 0;
	bool m_bSpitTouching = false;
	Vector3 m_vecCurPos = new Vector3();
	Vector2 m_vecSpitDirction = new Vector2();
	public void BeginSpitTouch( int nfingerId, Vector3 vecStartPos )
	{
		m_nFingerId = nfingerId;
		m_vecStartPos = vecStartPos;
		m_vecCurPos = vecStartPos;
		m_bSpitTouching = true;
	}

	public void EndSpitTouch()
	{
		m_bSpitTouching = false;
		m_MainPlayer.BeginClearSpitTargetCount ( m_fSpitRunTime );
	}



	void SpitTouching()
	{
		if (!m_bSpitTouching) {
			return;
		}

		if (m_nFingerId != 0 && Input.touchCount < 2 ) {
			m_nFingerId = 0;
		}  

		Touch touch = Input.GetTouch ( m_nFingerId );
		m_vecCurPos.x += touch.deltaPosition.x;
		m_vecCurPos.y += touch.deltaPosition.y;
		m_vecSpitDirction = m_vecCurPos - m_vecStartPos;
		m_vecSpitDirction.Normalize ();
		_playeraction.m_vec2MoveInfo = m_vecSpitDirction;
	}

	public int GetSpitFingerID()
	{
		return m_nFingerId;
	}

	public Vector2 GetSpitDirection()
	{
		return m_vecSpitDirction;
	}

	public void SetSpitDirection( float dirx, float diry )
	{
		m_vecSpitDirction.x = dirx;
		m_vecSpitDirction.y = diry;
	}

	public void OnClickBtn_Reborn()
	{
		m_MainPlayer.DoReborn ();
		_uiDead.SetActive ( false );
		m_nDeadStatus = 0;
		_playeraction.Reset ();
		m_MainPlayer.Reset ();
	}

	public void OnClickBtn_ReturnToHomePage()
	{
		PhotonNetwork.Disconnect ();
		SceneManager.LoadScene ( "SelectCaster" );
	}

	public void ChangeSkin()
	{
		m_MainPlayer.ChangeSkin ();
	}
} // end Main
