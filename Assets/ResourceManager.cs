﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

	public static ResourceManager s_Instance = null;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public GameObject PrefabInstantiate( string szPrefabName )
	{
		return GameObject.Instantiate((GameObject)Resources.Load(szPrefabName));
	}

	public void RemoveTattooFood( TattooFood food )
	{
		GameObject.Destroy ( food.gameObject );
	}

	public static void DestroyPolygon( Polygon polygon )
	{
		if (polygon != null) {
			GameObject.Destroy (polygon.gameObject);
		}
	}

	static List<SpitBallTarget> m_lstRecycledTarget = new List<SpitBallTarget>();
	public static SpitBallTarget ReuseTarget()
	{
		SpitBallTarget target = null;
		if (m_lstRecycledTarget.Count == 0) {
			target = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			return target;
		}
		target = m_lstRecycledTarget [0];
		target.gameObject.SetActive (true );
		m_lstRecycledTarget.RemoveAt (0);
		return target;
	}

	public static void RecycleTarget( SpitBallTarget target )
	{
		target.gameObject.SetActive (false );
		m_lstRecycledTarget.Add (target);
	}

	static List<Bean> m_lstRecycledBean = new List<Bean>();
	public static Bean ReuseBean()
	{
		return (GameObject.Instantiate (Main.s_Instance.m_preBean)).GetComponent<Bean>();

		Bean bean = null;
		if (m_lstRecycledBean.Count == 0) {
			bean = (GameObject.Instantiate (Main.s_Instance.m_preBean)).GetComponent<Bean>();
			return bean;
		}
		bean = m_lstRecycledBean [0];
		if (bean == null) {
			return null;
		}
		bean.gameObject.SetActive (true );
		bean.SetDead ( false );
		m_lstRecycledBean.RemoveAt (0);
		return bean;
	}

	public static void RecycleBean( Bean bean )
	{
		// 为确保无Bug,暂不优化
		bean.SetDead ( true );
		bean.gameObject.SetActive (false );
		GameObject.Destroy( bean.gameObject );
		return;

		bean.gameObject.SetActive (false );
		bean.SetDead ( true );
		m_lstRecycledBean.Add (bean);
	}
}
